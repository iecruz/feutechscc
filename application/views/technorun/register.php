<div id="paymentDetailModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">

    <div class="modal-content gothic-m">
        <div class="modal-header">
            <h4 class="modal-title">Mode of Payment Details</h4>
        </div>
        <div class="modal-body">
            <div id="bpiDetail" class="d-none">
                <p>
                    Fill out a deposit slip (available at any BPI branch nationwide) with the following information:<br><br>
                    1. Account Number - 1581-0012-46<br>
                    2. Account Name - East Asia Educational Foundation Inc.<br>
                    3. Planholder's Name - indicate your name<br>
                    4. Enter Amount<br>
                    5. Only <strong>cash</strong> payment will be accepted
                </p>
            </div>
            <div id="cashierDetail" class="d-none">
                <p>
                    FEU Institute of Technology (Cashier's Office)<br><br>
                    Address: P. Paredes Street, Sampaloc, Manila 1015<br>
                    Floor: 15th Floor<br>
                    Room: F-1508
                </p>
                <iframe frameborder="0" style="border:0; width: 100%; height: 30em;" 
                src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJWbJOsfjJlzMRjgaarMoSTa0&key=AIzaSyA0n-yj_uxS0tEuiwZLrDUjhfvctwwFDxw" 
                allowfullscreen></iframe>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

    </div>
</div>

<div id="registerSection" class="container-fluid py-md-5 bg-white parallax">
    <div class="row align-items-center">
        <div class="col-12 col-md-6 px-0 px-md-3">
            <div class="card bg-white border-0 rounded-0">
                <?= form_open(base_url('technorun/register'), ['id' => 'registerForm']) ?>
                <div class="card-header border-0">
                    <h2 class="text-center">Registration Form</h2>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="firstNameField">First Name</label>
                        <input type="text" name="first_name" id="firstNameField" class="form-control" required>
                        <small class="form-text text-danger"><?= form_error('first_name') ?></small>
                    </div>

                    <div class="form-group">
                        <label for="lastNameField">Last Name</label>
                        <input type="text" name="last_name" id="lastNameField" class="form-control" required>
                        <small class="form-text text-danger"><?= form_error('last_name') ?></small>
                    </div>

                    <div class="form-group">
                        <label for="emailAddressField">Email Address</label>
                        <input type="text" name="email_address" id="emailAddressField" class="form-control" required>
                        <small class="form-text text-danger"><?= form_error('email_address') ?></small>
                    </div>

                    <div class="form-group">
                        <label for="contactNumberField">Contact Number</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">+63</div>
                            </div>
                            <input type="text" name="contact_number" id="contactNumberField" class="form-control" maxlength="10" placeholder="9991234567" required>
                        </div>
                        <small class="form-text text-danger"><?= form_error('contact_number') ?></small>
                    </div>

                    <div class="form-group">
                        <label for="schoolCompanyField">School/Company</label>
                        <input type="text" name="school_company" id="schoolCompanyField" class="form-control">
                        <small class="form-text text-danger"><?= form_error('school_company') ?></small>
                    </div>

                    <div class="form-group">
                        <label for="typeField" class="mr-2">Category</label>
                        <select name="type" id="typeField" class="custom-select mr-2">
                            <option value="3k" <?= $category == '3k' ? 'selected' : '' ?>>3k</option>
                            <option value="5k" <?= $category == '5k' ? 'selected' : '' ?>>5k</option>
                            <option value="10k" <?= $category == '10k' ? 'selected' : '' ?>>10k</option>
                        </select>
                    </div>

                    <div id="singletSizeField" class="form-group">
                        <label for="sizeField" class="mr-2">Singlet Size</label>
                        <select name="size" id="sizeField" class="custom-select mr-2">
                            <option value="XS">Extra Small</option>
                            <option value="S">Small</option>
                            <option value="M" selected>Medium</option>
                            <option value="L">Large</option>
                            <option value="XL">Extra Large</option>
                            <option value="2Xl">2&times; Extra Large</option>
                            <option value="3Xl">3&times; Extra Large</option>
                        </select>
                        <a id="sizeChartDownload" href="#" download="size_chart.jpg" class="form-text text-capitalize" style="display: none;">Download Size Chart</a>
                    </div>
                    
                    <div class="form-group">
                        <label for="paymentField" class="mr-2">Mode of Payment</label>
                        <select name="mode_of_payment" id="paymentField" class="custom-select mr-2">
                            <option value="direct" selected>Direct Payment</option>
                            <option value="cashier">FEU Tech Cashier</option>
                            <option value="bpi">Through BPI</option>
                        </select>
                        <a href="#paymentDetailModal" data-toggle="modal" id="paymentDetail" class="form-text text-capitalize" style="display: none;">Show details</a>
                    </div>

                    <div class="d-block my-2">
                        <small id="formError" class="text-danger"></small>
                    </div>

                </div>

                <div class="card-footer border-0">
                    <button type="submit" class="btn btn-dark btn-block">Submit</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>

        <div class="col-12 col-md-6 px-0 px-md-3">
            <img src="<?= base_url('assets/res/technorun/banner/banner_'.$category.'.jpg') ?>" class="d-none d-md-block category-img mx-auto img-fluid w-100" alt="logo">
            <img src="<?= base_url('assets/res/technorun/banner/banner_'.$category.'.jpg') ?>" class="d-md-none d-block category-img mx-auto img-fluid w-100" alt="logo">
        </div>
    </div>
</div>
<div id="mainSection" class="container-fluid full-page h-100 py-5 parallax">
    <div class="row">
        <div class="col-12">
            <img src="<?= base_url('assets/res/technorun/logo/logo.png') ?>" class="d-none d-md-block mx-auto img-fluid pt-3" alt="logo">
            <img src="<?= base_url('assets/res/technorun/logo/logo.png') ?>" class="d-block d-md-none img-fluid w-100 py-5" alt="logo">
        </div>

        <div class="w-100"></div>

        <div class="col-12">
            <h1 class="d-none d-md-block text-center font-weight-light font-italic text-light text-shadow impact">MARCH 25, 2018 (SUNDAY)</h1>
            <h4 class="d-md-none d-block text-center font-weight-light font-italic text-light text-shadow impact">MARCH 25, 2018 (SUNDAY)</h>

            <h2 class="d-none d-md-block text-center font-weight-light font-italic text-light text-shadow impact">CCP COMPLEX, PASAY CITY</h2>
            <h5 class="d-md-none d-block text-center font-weight-light font-italic text-light text-shadow impact">CCP COMPLEX, PASAY CITY</h5>
        </div>

        <div class="w-100"></div>

        <!-- <div class="col-12 py-3 py-md-5 text-center">
            <a href="#catSection" class="d-none d-md-inline-block btn-solid btn btn-outline-dark btn-lg mt-3 font-weight-normal impact" style="font-size: 2.3rem; border-width: .4rem;">REGISTER</a>
            <a href="#catSection" class="d-block d-md-none btn btn-outline-light w-50 mt-4 mx-auto font-weight-normal impact" style="border-width: .2rem;">REGISTER</a>

            <a href="#" class="d-none d-md-inline-block btn-solid btn btn-outline-dark btn-lg mt-3 bebas" style="border-width: .25rem;">LEARN MORE</a>
            <a href="#" class="d-block d-md-none btn btn-outline-light w-50 mt-1 mx-auto bebas" style="border-width: .2rem;">LEARN MORE</a>
        </div> -->
    </div>
</div>

<div id="videoSection" class="section container-fluid py-5 bg-black">
    <div class="container text-center">
        <h1 class="d-none d-md-block display-1 text-white text-shadow font-italic bebas" style="letter-spacing:0.2rem;">TEASER VIDEO</h1>
        <h1 class="d-md-none d-block text-white text-shadow font-weight-bold font-italic bebas" style="letter-spacing:0.2rem;">TEASER VIDEO</h1>
        <div class="video-container">
            <iframe src="https://www.youtube.com/embed/QQ2aRBIFihk?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </div>
    </div>
</div>

<div id="aboutSection" class="container-fluid d-none d-md-block py-5 bg-white parallax">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-6">
                <img src="<?= base_url('assets/res/technorun/logo/logo-2.png') ?>" class="mx-auto img-fluid w-100" alt="logo">
            </div>
            <div class="col-6 text-center">
                <h6 class="desc text-justify text-dark py-5 gotham-r" style="line-height: 1.5rem; letter-spacing: 0.1rem;">
                    The environment supports the life of each and every living thing on earth. We rely on the environment for life. But it is because of the human activities which destroy all of its component, the plants, trees, and animals, water resources, air, the land, and all living creatures are now threatened by destruction.<br><br>
                    Still, there is HOPE. It is important to educate people on the importance of environmental protection. Conservation of the environment aims at keeping it safe and healthy. It aims at the reduction of overusing the natural resources. It is the taking care of all the components that make up the environment.<br><br>
                    That is why, Student Leaders of Far Eastern University Institute of Technology will be at front to educate the community about protecting our EARTH.<br><br>
                    The TECHNORUN aims to make everyone aware how important to protect our environment and that youth of today  must be more assertive on doing this kind of noble cause. To make this Fun Run a memorable one, it gives new highlights by welcoming the competitors with much bolder, and fierce competition among different schools that will participate in the Cheerdance Competition.<br><br>
                    The little things people do will do a long way in environmental protection.  The environment is very important for the existence of life. It supports living things and protecting it should be our first priority. Without a safer environment, we run the danger of extinction. It is good to understand that it is the environment that supports us and not vice versa.<br><br>
                    It’s the RIGHT TIME to RUN to SAVE the Environment.<br><br>
                    If <strong>FEU</strong> run, you pro<strong>TECH</strong> the envi<strong>RUN</strong>ment!<br><br>
                    TECHNORUN 2018!
                </h6>
            </div>
        </div>
    </div>
</div>

<div id="aboutSectionSm" class="container-fluid d-block d-md-none py-3 bg-light">
    <div class="row">
        <div class="col-12">
            <img src="<?= base_url('assets/res/technorun/logo/logo-1.png') ?>" class="img-fluid d-block mx-auto w-100" alt="logo">
        </div>
        <div class="col-12 text-center">
            <p class="text-justify text-dark mx-3 my-3 gotham-r" style="line-height: 1.2rem;">
                The environment supports the life of each and every living thing on earth. We rely on the environment for life. But it is because of the human activities which destroy all of its component, the plants, trees, and animals, water resources, air, the land, and all living creatures are now threatened by destruction.<br><br>
                Still, there is HOPE. It is important to educate people on the importance of environmental protection. Conservation of the environment aims at keeping it safe and healthy. It aims at the reduction of overusing the natural resources. It is the taking care of all the components that make up the environment.<br><br>
                That is why, Student Leaders of Far Eastern University Institute of Technology will be at front to educate the community about protecting our EARTH.<br><br>
                The TECHNORUN aims to make everyone aware how important to protect our environment and that youth of today  must be more assertive on doing this kind of noble cause. To make this Fun Run a memorable one, it gives new highlights by welcoming the competitors with much bolder, and fierce competition among different schools that will participate in the Cheerdance Competition.<br><br>
                The little things people do will do a long way in environmental protection.  The environment is very important for the existence of life. It supports living things and protecting it should be our first priority. Without a safer environment, we run the danger of extinction. It is good to understand that it is the environment that supports us and not vice versa.<br><br>
                It’s the RIGHT TIME to RUN to SAVE the Environment.<br><br>
                If <strong>FEU</strong> run, you pro<strong>TECH</strong> the envi<strong>RUN</strong>ment!<br><br>
                TECHNORUN 2018!
            </p>
        </div>
    </div>
</div>

<div id="catSection" class="container-fluid px-4 py-5">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-12">
                <h1 class="d-none d-md-block display-1 text-center text-white text-shadow font-italic bebas" style="letter-spacing:0.2rem;">ANNOUNCEMENT</h1>
                <h1 class="d-md-none d-block text-center text-white text-shadow font-weight-bold font-italic bebas" style="letter-spacing:0.2rem;">ANNOUNCEMENT</h1>
            </div>
            
            <div class="col-12 col-md-8">
                <div class="card card-body bg-warning impact">
                    <h4>Online registration is now <span class="font-weight-bold">closed</span>.</h4>
                    <hr>
                    <h4>We are still accepting walk-ins for 3K category on the site.</h4>
                    <hr>
                    <h4>For 5k and 10k category, participants will be accommodated on the site in a first come, first serve basis.</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="detailsSection" class="container-fluid bg-white parallax">
    <div class="container">
        <div class="row">
            <div class="col-12 text-left py-5">
                <h3 class="gotham-b font-weight-bold">MARCH 25, 2018<br>4 AM - 9 AM</h3>
                <h4 class="gotham-b">CULTURAL CENTER OF THE PHILIPPINES COMPLEX<br>ROXAS BLVD., PASAY CITY</h4>
                <h5 class="mt-3 gotham-r">Participants should be<br><span class="gotham-m">7 years old and above</span></h5>
            </div>
        </div>
    </div>
</div>

<div id="organizerSection" class="section container-fluid bg-black py-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="d-none d-md-block display-1 text-center text-white text-shadow font-italic bebas" style="letter-spacing:0.2rem;">ORGANIZERS</h1>
                <h1 class="d-md-none d-block text-center text-white text-shadow font-weight-bold font-italic bebas" style="letter-spacing:0.2rem;">ORGANIZERS</h1>
            </div>
            <div class="col-12 py-3">
                <div class="row justify-content-center">
                    <div class="col-4 col-md-2">
                        <img src="<?= base_url('assets/res/scc/logo/scc.png') ?>" class="d-none d-md-block mx-auto img-fluid w-75" alt="logo">
                        <img src="<?= base_url('assets/res/scc/logo/scc.png') ?>" class="d-md-none d-block mx-auto img-fluid w-100" alt="logo">
                    </div>
                    <div class="col-4 col-md-2">
                        <img src="<?= base_url('assets/res/scc/logo/feu-tech.png') ?>" class="d-none d-md-block mx-auto img-fluid w-75" alt="logo">
                        <img src="<?= base_url('assets/res/scc/logo/feu-tech.png') ?>" class="d-md-none d-block mx-auto img-fluid w-100" alt="logo">
                    </div>
                    <div class="col-4 col-md-2">
                        <img src="<?= base_url('assets/res/scc/logo/rac.png') ?>" class="d-none d-md-block mx-auto img-fluid w-75" alt="logo">
                        <img src="<?= base_url('assets/res/scc/logo/rac.png') ?>" class="d-md-none d-block mx-auto img-fluid w-100" alt="logo">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="sponsorSection" class="section container-fluid bg-success py-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="d-none d-md-block display-1 text-white text-center text-shadow font-italic bebas" style="letter-spacing:0.2rem;">SPONSORS</h1>
                <h1 class="d-md-none d-block text-white text-center text-shadow font-weight-bold font-italic bebas" style="letter-spacing:0.2rem;">SPONSORS</h1>
            </div>
            <div class="col-12 py-3">
                <div class="row justify-content-center">
                    <div class="col-4 col-md-3">
                        <img src="<?= base_url('assets/res/technorun/sponsors/pocari.png') ?>" class="img-fluid" alt="logo">
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="partnershipSection" class="section container-fluid bg-white py-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="d-none d-md-block display-1 text-center text-shadow font-italic bebas" style="letter-spacing:0.2rem;">IN PARTNERSHIP WITH</h1>
                <h1 class="d-md-none d-block text-center text-shadow font-weight-bold font-italic bebas" style="letter-spacing:0.2rem;">IN PARTNERSHIP WITH</h1>
            </div>
            <div class="col-12 py-3">
                <div class="row justify-content-center">
                    <div class="col-4 col-md-6">
                        <img src="<?= base_url('assets/res/technorun/sponsors/climate-reality.png') ?>" class="img-fluid" alt="logo">
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="announcementModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content bg-warning">
            <div class="modal-header">
                <h2 class="modal-title font-weight-bold mb-0 bebas">ANNOUNCEMENT</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body impact">
            <h4>Online registration is now <span class="font-weight-bold">closed</span>.</h4>
                    <hr>
                    <h4>We are still accepting walk-ins for 3K category on the site.</h4>
                    <hr>
                    <h4>For 5k and 10k category, participants will be accommodated on the site in a first come, first serve basis.</h4>
            </div>
        </div>
    </div>
</div>
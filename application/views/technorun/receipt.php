<div id="receiptSection" class="section container-fluid py-5">
<div class="container">

    <div class="row">
        <div class="col-12">
            <img src="<?= base_url('assets/res/technorun/logo/logo.png') ?>" class="d-none d-md-block mx-auto img-fluid w-75" alt="logo">
            <img src="<?= base_url('assets/res/technorun/logo/logo.png') ?>" class="d-block d-md-none img-fluid w-100" alt="logo">
        </div>

        <div class="col-12">
            <h1 class="d-none d-md-block text-center font-weight-light font-italic text-light text-shadow impact">MARCH 25, 2018 (SUNDAY)</h1>
            <h4 class="d-md-none d-block text-center font-weight-light font-italic text-light text-shadow impact">MARCH 25, 2018 (SUNDAY)</h>

            <h2 class="d-none d-md-block text-center font-weight-light font-italic text-light text-shadow impact">CCP COMPLEX, PASAY CITY</h2>
            <h5 class="d-md-none d-block text-center font-weight-light font-italic text-light text-shadow impact">CCP COMPLEX, PASAY CITY</h5>
        </div>

        <div class="col-12 pt-4">
            <div class="card">
                <div class="step-title d-none d-md-block card-header h4">Step 1: Fill out the details</div>
                <div class="step-title d-md-none d-block card-header">Step 1: Fill out the details</div>
                <div class="card-body" style="overflow-x: auto;">

                    <?= form_open('', ['id' => 'searchForm']) ?>
                        <div class="row form-group">
                            <div class="form-group col-12">
                                <label for="firstNameSearchField">First Name</label>
                                <input type="text" class="form-control" name="first_name" id="firstNameSearchField" required>
                            </div>
                            <div class="form-group col-12">
                                <label for="lastNameSearchField">Last Name</label>
                                <input type="text" class="form-control" name="last_name" id="lastNameSearchField" required>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Next <i class="fa fa-chevron-right"></i></button>
                    <?= form_close() ?>

                    <table id="resultTable" class="table" style="display: none;">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">School/Company</th>
                                <th scope="col">Singlet Size</th>
                                <th scope="col">Type</th>
                                <th scope="col">Select</th>
                            </tr>
                        </thead>
                        <tbody id="resultTableList">
                        </tbody>
                    </table>

                    <?= form_open_multipart('', ['id' => 'receiptForm', 'style' => 'display: none;']) ?>
                        <div class="row form-group">
                            <div class="form-group col-12">
                                <label for="numberField">Registration Number</label>
                                <input type="text" class="form-control" name="id" id="numberField" readonly>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="firstNameField">First Name</label>
                                <input type="text" class="form-control" name="first_name" id="firstNameField" readonly>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="lastNameField">Last Name</label>
                                <input type="text" class="form-control" name="last_name" id="lastNameField" readonly>
                            </div>
                        </div>
                        
                        <div class="custom-file mb-3">
                            <input type="file" class="custom-file-input" name="receipt" id="receiptField">
                            <label for="receiptField" class="custom-file-label">Upload Receipt</label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-upload"></i> Submit</button>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
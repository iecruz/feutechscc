<div class="container-fluid candidate-page full-page">
    <div class="container py-4">
        <div class="row justify-content-center mb-4">
            <div class="col-4 col-md-2"><img src="<?= base_url('assets/res/scc/logo/feu-tech.png') ?>" class="img-fluid" alt=""></div>
            <div class="col-4 col-md-2"><img src="<?= base_url('assets/res/scc/logo/scc.png') ?>" class="img-fluid" alt=""></div>
        </div>

        <div class="d-none d-md-block">
            <h5 class="text-center text-uppercase font-weight-bold mb-0" style="letter-spacing: 0.1rem;">Student Coordinating Council</h5>
            <hr class="w-75 my-0">
            <h1 class="text-center font-weight-bold" style="letter-spacing: 0.2rem;">ELECTION 2018</h1>
        </div>

        <div class="d-block d-md-none">
            <h6 class="text-center text-uppercase font-weight-bold mb-0" style="letter-spacing: 0.1rem;">Student Coordinating Council</h6>
            <hr class="my-0">
            <h3 class="text-center font-weight-bold" style="letter-spacing: 0.2rem;">ELECTION 2018</h3>
        </div>
    </div>

    <div class="container py-4">
        <div class="row">
            <div class="col-12">
                </div>
            <div class="col-12 col-md-4">
                <img src="/assets/res/election/candidates/portrait/<?= $name ?>.JPG" class="img-fluid" alt="">
            </div>
            <div class="col-12 col-md-8">
                <h1 class="display-4 text-uppercase font-weight-bold mb-0"><?= $name ?></h1>
                <h3 class="text-uppercase font-weight-bold mb-4"><?= $course ?></h3>

                <?php foreach($descriptions as $description): ?>
                    <h5 class="text-uppercase font-weight-bold mb-0"><?= $description['title'] ?></h5>
                    <h6 class="text-uppercase mb-0"><?= $description['subtitle'] ?? '' ?></h6>
                    <h6 class="text-uppercase mb-0"><?= $description['school'] ?? '' ?></h6>
                    <h6 class="text-uppercase"><?= $description['year'] ?? '' ?></h6>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
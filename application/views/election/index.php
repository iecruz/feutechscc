<div class="container-fluid front-page">
    <div class="container py-4">
        <div class="row justify-content-center mb-4">
            <div class="col-4 col-md-2"><img src="<?= base_url('assets/res/scc/logo/feu-tech.png') ?>" class="img-fluid" alt=""></div>
            <div class="col-4 col-md-2"><img src="<?= base_url('assets/res/scc/logo/scc.png') ?>" class="img-fluid" alt=""></div>
        </div>

        <div class="d-none d-md-block">
            <h5 class="text-center text-uppercase font-weight-bold mb-0" style="letter-spacing: 0.1rem;">Student Coordinating Council</h5>
            <hr class="w-75 my-0">
            <h1 class="text-center font-weight-bold" style="letter-spacing: 0.2rem;">ELECTION 2018</h1>
        </div>

        <div class="d-block d-md-none">
            <h6 class="text-center text-uppercase font-weight-bold mb-0" style="letter-spacing: 0.1rem;">Student Coordinating Council</h6>
            <hr class="my-0">
            <h3 class="text-center font-weight-bold" style="letter-spacing: 0.2rem;">ELECTION 2018</h3>
        </div>
    </div>

    <div class="container py-4">
        <div id="electionCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#electionCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#electionCarousel" data-slide-to="1"></li>
                <li data-target="#electionCarousel" data-slide-to="2"></li>
                <li data-target="#electionCarousel" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="<?= base_url('assets/res/election/front-page/1.jpg') ?>" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?= base_url('assets/res/election/front-page/2.jpg') ?>" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?= base_url('assets/res/election/front-page/3.jpg') ?>" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?= base_url('assets/res/election/front-page/4.jpg') ?>" alt="Fourth slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#electionCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#electionCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<div class="container-fluid bg-success election-page text-white parallax">
    <div class="container py-4">
        <h1 class="d-none d-md-block text-center font-weight-bold" style="letter-spacing: 0.2rem;">ELECTION CODE</h1>
        <h3 class="d-md-none d-block text-center font-weight-bold" style="letter-spacing: 0.2rem;">ELECTION CODE</h3>
        <hr class="border-secondary w-75">

        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <ol class="d-none d-md-block">
                    <li class="h5">All candidates will run for presidency</li>
                    <li class="h5"><span class="font-weight-bold">Meeting de Avance</span> (May 22, 2018)</li>
                    <li class="h5">Every voter can vote <span class="font-weight-bold">2 candidates</span></li>
                    <li class="h5"><span class="font-weight-bold">Top 2 candidates</span> will be qualified to be presidential candidates</li>
                    <li class="h5">After the general election, there will be an <span class="font-weight-bold">internal election</span> for presidency</li>
                    <li class="h5">It will be followed by the internal election for other positions</li>
                </ol>
                <ol class="d-md-none d-block">
                    <li>All candidates will run for presidency</li>
                    <li><span class="font-weight-bold">Meeting de Avance</span> (May 22, 2018)</li>
                    <li>Every voter can vote <span class="font-weight-bold">2 candidates</span></li>
                    <li><span class="font-weight-bold">Top 2 candidates</span> will be qualified to be presidential candidates</li>
                    <li>After the general election, there will be an <span class="font-weight-bold">internal election</span> for presidency</li>
                    <li>It will be followed by the internal election for other positions</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="container py-4">
        <h1 class="d-none d-md-block text-center font-weight-bold" style="letter-spacing: 0.2rem;">ELECTION CALENDAR</h1>
        <h3 class="d-md-none d-block text-center font-weight-bold" style="letter-spacing: 0.2rem;">ELECTION CALENDAR</h3>
        <hr class="border-secondary w-75">

        <div class="d-none d-md-block">
            <h2 class="text-center text-uppercase font-weight-bold mb-0">Campaign Period</h2>
            <h5 class="text-center">May 3 - 23, 2018</h5>

            <h2 class="text-center text-uppercase font-weight-bold mb-0">Miting de Avance</h2>
            <h5 class="text-center">May 22, 2018 | Student Plaza | 1:00 PM - 3:00 PM</h5>

            <h2 class="text-center text-uppercase font-weight-bold mb-0">Voting Period</h2>
            <h5 class="text-center">May 24 - 26, 2018 | Student Plaza | 9:00 AM - 9:00 PM</h5>
        </div>

        <div class="d-md-none d-block">
            <h4 class="text-center text-uppercase font-weight-bold mb-0">Campaign Period</h4>
            <h6 class="text-center">May 3 - 23, 2018</h6>

            <h4 class="text-center text-uppercase font-weight-bold mb-0">Miting de Avance</h4>
            <h6 class="text-center">May 22, 2018 | Student Plaza | 1:00 PM - 3:00 PM</h6>

            <h4 class="text-center text-uppercase font-weight-bold mb-0">Voting Period</h4>
            <h6 class="text-center">May 24 - 26, 2018 | Student Plaza | 9:00 AM - 9:00 PM</h6>
        </div>
    </div>
</div>

<div class="container-fluid front-page">
    <div class="container py-4">
        <div id="candidateCarousel" class="carousel slide my-4" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#candidateCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#candidateCarousel" data-slide-to="1"></li>
                <li data-target="#candidateCarousel" data-slide-to="2"></li>
                <li data-target="#candidateCarousel" data-slide-to="3"></li>
                <li data-target="#candidateCarousel" data-slide-to="4"></li>
                <li data-target="#candidateCarousel" data-slide-to="5"></li>
            </ol>
            <div class="carousel-inner">
                <?php foreach($candidates as $index => $candidate): ?>
                <div class="carousel-item <?= $index == 0 ? 'active' : '' ?>">
                    <img class="d-block w-100" src="/assets/res/election/candidates/rectangle/<?= $candidate['name'] ?>.png" alt="">
                </div>
                <?php endforeach ?>
            </div>
            <a class="carousel-control-prev" href="#candidateCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#candidateCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<div class="container-fluid candidate-page">
    <div class="container py-4">
        <h1 class="d-none d-md-block text-center font-weight-bold" style="letter-spacing: 0.2rem;">CANDIDATES</h1>
        <h3 class="d-md-none d-block text-center font-weight-bold" style="letter-spacing: 0.2rem;">CANDIDATES</h3>
        <hr class="w-75">
    </div>
    <div class="row align-items-center">
        <div class="w-100"></div>
        <?php foreach($candidates as $index => $candidate): ?>
        <?php if($index / 2 % 2 == 0): ?>
        <div class="d-none d-md-block col-12 col-md-3 px-0">
            <img class="img-fluid" src="/assets/res/election/candidates/thumbnail/<?= $candidate['name'] ?>.jpg" alt="">
        </div>
        <?php endif ?>
        <div class="d-md-none d-block col-12 px-0">
            <img class="img-fluid" src="/assets/res/election/candidates/thumbnail/<?= $candidate['name'] ?>.jpg" alt="">
        </div>
        <div class="col-12 col-md-3 py-4 py-md-0">
            <h4 class="text-center font-weight-bold text-uppercase" style="letter-spacing: 0.1rem;"><?= $candidate['name'] ?></h4>
            <h6 class="text-center font-weight-bold text-uppercase"><?= $candidate['course'] ?></h6>
            <a href="<?= base_url("election/profile/{$candidate['link']}") ?>" class="btn btn-dark btn-block font-weight-bold text-uppercase w-50 mx-auto">View Profile</a>
        </div>
        <?php if($index / 2 % 2 == 1): ?>
        <div class="d-none d-md-block col-12 col-md-3 px-0">
            <img class="img-fluid" src="/assets/res/election/candidates/thumbnail/<?= $candidate['name'] ?>.jpg" alt="">
        </div>
        <?php endif ?>
        <?php endforeach ?>
    </div>
</div>

<div class="container-fluid bg-warning py-4">
    <div class="d-none d-md-block">
        <h1 class="display-4 text-center text-black font-weight-bold">#SERVE #LEAD #EXCEL</h1>
        <h2 class="text-center text-black text-uppercase">FEU Tech Student Leaders</h2>
    </div>
    <div class="d-md-none d-block">
        <h3 class="text-center text-black font-weight-bold">#SERVE #LEAD #EXCEL</h3>
        <h5 class="text-center text-black text-uppercase">FEU Tech Student Leaders</h5>
    </div>
</div>
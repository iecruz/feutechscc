<!DOCTYPE html>
<html lang="en">
<head>
    <meta property="og:title" content="Student Coordinating Council" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?= base_url() ?>" />
    <meta property="og:image" content="<?= base_url('assets/res/scc/logo/scc.png') ?>" />

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" href="<?= base_url('favicon.png') ?>" type="image/x-icon">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

    <?php foreach ($cdn_css as $css_instance): ?>
    <link rel="stylesheet" href="<?= $css_instance ?>"/>
    <?php endforeach; ?>

    <?php foreach ($css as $css_instance): ?>
    <link rel="stylesheet" href="<?= base_url($css_instance) ?>"/>
    <?php endforeach; ?>

    <?php foreach ($font as $font_instance): ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=<?= str_replace(' ', '+', $font_instance) ?>:400,700">
    <?php endforeach; ?>

    <script defer src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>

    <?php foreach($cdn_js as $js_instance): ?>
    <script defer src="<?= $js_instance ?>"></script>
    <?php endforeach; ?>

    <?php foreach($js as $js_instance): ?>
    <script defer src="<?= base_url($js_instance) ?>"></script>
    <?php endforeach; ?>

    <title><?= $page_title; ?></title>

    <!-- Want to help us and be part of the SCC Developer Committee? 
    Contact us on our Facebook Page: FEU TECH Student Coordinating Council
    https://www.facebook.com/feutechscc/ -->
</head>

<body class="bg-light">

<div id="fb-root"></div>

<div id="load" class="h-100 w-100 position-fixed bg-light" <?php if(!$require_load): ?> style="display:none;" <?php endif; ?>>
    <div class="d-flex align-items-center w-100 h-100">
        <img src="<?= base_url('assets/res/scc/icon/load.png') ?>" class="img-fluid d-block mx-auto align-self-center" style="width: 150px;">
    </div>
</div>

<header class="navbar navbar-expand-md sticky-top navbar-dark" style="background-color: #0a0a0a;">
    <div class="container">
        <a class="navbar-brand" href="#">
            <img src="<?= base_url('assets/res/scc/logo/scc.png') ?>" width="30" height="30" class="d-inline-block align-top" alt=""> SCC
        </a>

        <!-- <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>">News</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Events</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="<?= base_url('technofest/home') ?>">TechnoFest</a>
                        <a class="dropdown-item" href="<?= base_url('technorun/home') ?>">TechnoRun</a>
                        <a class="dropdown-item" href="<?= base_url('ambassadors/home') ?>">iTam Ambassadors</a>
                        <a class="dropdown-item" href="#">iTam Night</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>">Concerns</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>">About Us</a>
                </li>
            </ul>
        </div> -->
    </div>  
</header>

<main id="mainContent" data-spy="scroll" data-offset="0" class="bg-light">
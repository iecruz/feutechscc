</main>

<?php if ($require_footer): ?>
<footer id="footer" class="container-fluid text-muted bg-black py-3 py-md-4">
    <div class="container">
        <div class="row justify-content-between justify-content-around">
            <div class="col-12 col-md-4 my-3 my-md-0">
                <div class="row">
                    <div class="col-12">
                        <h4 class="text-center">Contact Us</h4>
                        <hr class="border border-dark">
                    </div>

                    <div class="col-2 text-center"><i class="fa fa-phone"></i></div>
                    <div class="col-10">
                        00 63 2 2818888 loc. 128<br>
                    </div>

                    <div class="col-2 text-center"><i class="fa fa-mobile"></i></div>
                    <div class="col-10">
                        00 63 905 6809034<br>
                        00 63 927 7556612<br>
                        00 63 925 7148094
                    </div>

                    <div class="col-2 text-center"><i class="fa fa-envelope"></i></div>
                    <div class="col-10">
                        feutechscc@gmail.com
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4 my-3 my-md-0">
                <div class="row">
                    <div class="col-12">
                        <h4 class="text-center">Follow Us</h4>
                        <hr class="border border-dark">
                    </div>

                    <div class="col-12">
                        <a href="https://www.facebook.com/feutechscc"><i class="fab fa-facebook-square mx-1"></i> Facebook</a><br>
                        <a href="#"><i class="fab fa-twitter-square mx-1"></i> Twitter</a><br>
                        <a href="#"><i class="fab fa-instagram mx-1"></i> Instagram</a>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <p class="text-center small my-0">&copy; Copyright <?= date('Y') ?> Student Coordinating Council. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</footer>
<?php endif; ?>
</body>
</html>
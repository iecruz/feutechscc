<div class="container h-100 d-flex align-items-center py-5">
    <div class="card w-100">
        <form action="<?= base_url('admin/login') ?>" method="post">
        <div class="card-header">
            <h1>Admin Login</h1>
        </div>
        <div class="card-body">
            <div class="form-group">
                <label for="usernameField">Username</label>
                <input type="text" name="username" id="usernameField" class="form-control">
            </div>
            <div class="form-group">
                <label for="passwordField">Password</label>
                <input type="password" name="password" id="passwordField" class="form-control">
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-block btn-lg"><i class="fa fa-sign-in-alt"></i> Login</button>
        </div>
        </form>
    </div>
</div>
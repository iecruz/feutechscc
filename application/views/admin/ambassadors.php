<div class="container py-3">
    <div class="card">
        <div class="card-header"><h4>Vote Chart</h4></div>
        <div class="card-body">
            <table class="table table-striped table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Program Code</th>
                        <th scope="col">Male</th>
                        <th scope="col">Female</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($vote_counts as $program_code => $vote_count): ?>
                    <tr>
                        <th scope="row"><?= $program_code ?></th>
                        <td><?= $vote_count['M'] ?></td>
                        <td><?= $vote_count['F'] ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <h4>Total Voters <span class="text-muted"><?= $total_count ?></span></h4>
        </div>
    </div>
</div>
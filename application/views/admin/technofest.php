<div class="modal fade" id="answerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Essay Answers</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="font-weight-bold">1. Albert Einstein once stated, it has become appaillingly obvious that our technology has exceeded our humanity. What is the effect of technology to the people in the aspect of human values?</p>
                <p id="answer1"></p>
                <p class="font-weight-bold">2. With the theme "Fortifying Human Values and Technology in Achieving Sustainable Innovations", how can we innovate technology while maintaining high regard to human values?</p>
                <p id="answer2"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid py-3">
    <div class="card card-body bg-success mb-2">
        <h1 class="text-center text-uppercase text-warning font-weight-bold mb-0" style="text-shadow: 0px 3px 4px #000; letter-spacing: 0.1rem;">TechnoFest Database</h1>
    </div>
    <div class="card mb-2">
        <div class="card-header bg-success" style="overflow: auto;">
            <span class="h5 font-weight-bold mb-0 text-warning text-uppercase" style="text-shadow: 0px 3px 3px #000;">TechnoFest Registration Summary</span>
            <a href="#technoFestSummary" data-toggle="collapse" class="btn btn-dark btn-sm float-right"><i class="fa fa-eye"></i></a>
        </div>
        <div id="technoFestSummary" class="collapse show">
            <div class="card-body">
                <div class="row">
                    <?php foreach($locale as $l): ?>
                    <div class="col-4">
                        <h5 class="font-weight-bold"><?= ucwords(str_replace('_', ' ', $l)) ?></h5>
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th scope="col">Career</th>
                                        <th scope="col">With Accommodation</th>
                                        <th scope="col">Without Accommodation</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($career_type as $ct): ?>
                                    <tr>
                                        <th scope="row"><?= ucwords($ct) ?></th>
                                        <?php foreach($accommodation as $a): ?>
                                        <td><?= $count[$ct][$a][$l] ?></td>
                                        <?php endforeach; ?>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-2">
        <div class="card-header bg-success" style="overflow: auto;">
            <span class="h5 font-weight-bold mb-0 text-warning text-uppercase" style="text-shadow: 0px 3px 3px #000;">TechnoFest Participants</span>
            <a href="#technoFestRecords" data-toggle="collapse" class="btn btn-dark btn-sm float-right"><i class="fa fa-eye"></i></a>
        </div>
        <div id="technoFestRecords" class="collapse">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="technofestParticipants" class="table table-sm w-100" cellspacing="0"></table>
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-2">
        <div class="card-header bg-success" style="overflow: auto;">
            <span class="h5 font-weight-bold text-warning text-uppercase" style="text-shadow: 0px 3px 3px #000;">TechnoFest Competitors</span>
            <a href="#technoFestCompetitors" data-toggle="collapse" class="btn btn-dark btn-sm float-right"><i class="fa fa-eye"></i></a>
        </div>
        <div id="technoFestCompetitors" class="collapse">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="technofestCompetitors" class="table table-sm w-100" cellspacing="0"></table>
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-2">
        <div class="card-header bg-success" style="overflow: auto;">
            <span class="h5 font-weight-bold text-warning text-uppercase" style="text-shadow: 0px 3px 3px #000; let">TechnoFest Scholarship Applicants</span>
            <a href="#technoFestApplicants" data-toggle="collapse" class="btn btn-dark btn-sm float-right"><i class="fa fa-eye"></i></a>
        </div>
        <div id="technoFestApplicants" class="collapse">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="technofestApplicants" class="table table-sm w-100" cellspacing="0"></table>
                </div>
            </div>
        </div>
    </div>
</div>
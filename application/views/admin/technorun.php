<div class="container-fluid py-3">
    <div class="card my-2">
        <div class="card-header" style="overflow: auto;">
            <span class="h5 font-weight-bold">TechnoRun Summary</span>
            <a href="#technoRunSummary" data-toggle="collapse" class="btn btn-dark btn-sm float-right"><i class="fa fa-eye"></i></a>
        </div>
        <div id="technoRunSummary" class="collapse show">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <h5>Total Participants</h5>
                        <table class="table table-sm">
                            <tbody>
                                <?php foreach($categories as $category => $price): ?>
                                <tr>
                                    <th scope="row"><?= strtoupper($category) ?> Internal</th>
                                    <td><?= $count[$category.'_internal'] ?></td>
                                </tr>
                                <tr>
                                    <th scope="row"><?= strtoupper($category) ?> External</th>
                                    <td><?= $count[$category.'_external'] ?></td>
                                </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <th scope="row">Total</th>
                                    <td><?= $count_total['all'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-12 col-md-4">
                        <h5>Paid Runners</h5>
                        <table class="table table-sm">
                            <tbody>
                                <?php foreach($categories as $category => $price): ?>
                                <tr>
                                    <th scope="row"><?= strtoupper($category) ?></th>
                                    <td><?= $pay['paid_'.$category] ?></td>
                                    <td>&#8369; <?= number_format((float)((int)$pay['paid_'.$category] * $price), 2, '.', '') ?></td>
                                </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <th scope="row">Total</th>
                                    <td><?= $count_total['paid'] ?></td>
                                    <td>&#8369; <?= number_format($pay_total['paid'], 2, '.', '') ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-12 col-md-4">
                        <h5>Unpaid Runners</h5>
                        <table class="table table-sm">
                            <tbody>
                                <?php foreach($categories as $category => $price): ?>
                                <tr>
                                    <th scope="row"><?= strtoupper($category) ?></th>
                                    <td><?= $pay['unpaid_'.$category] ?></td>
                                    <td>&#8369; <?= number_format((float)((int)$pay['unpaid_'.$category] * $price), 2, '.', '') ?></td>
                                </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <th scope="row">Total</th>
                                    <td><?= $count_total['unpaid'] ?></td>
                                    <td>&#8369; <?= number_format($pay_total['unpaid'], 2, '.', '') ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-12 col-md-4">
                        <h5>Singlet 5K</h5>
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">Size</th>
                                    <th scope="col">Paid</th>
                                    <th scope="col">Unpaid</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($sizes as $size_key => $size_value): ?>
                                <tr>
                                    <th scope="row"><?= $size_key ?></th>
                                    <td><?= $singlet['paid_'.$size_value.'_5k'] ?></td>
                                    <td><?= $singlet['unpaid_'.$size_value.'_5k'] ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-12 col-md-4">
                        <h5>Singlet 10K</h5>
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">Size</th>
                                    <th scope="col">Paid</th>
                                    <th scope="col">Unpaid</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($sizes as $size_key => $size_value): ?>
                                <tr>
                                    <th scope="row"><?= $size_key ?></th>
                                    <td><?= $singlet['paid_'.$size_value.'_10k'] ?></td>
                                    <td><?= $singlet['unpaid_'.$size_value.'_10k'] ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="card my-2">
        <div class="card-header" style="overflow: auto;">
            <span class="h5 font-weight-bold">TechnoRun Participants</span>
            <a href="#technoRunRecords" data-toggle="collapse" class="btn btn-dark btn-sm float-right"><i class="fa fa-eye"></i></a>
        </div>
        <div id="technoRunRecords" class="collapse">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="technorunParticipants" class="table table-sm w-100" cellspacing="0"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="frame-wrapper">
    
    <div class="frame clearfix" data-order="1">
        <div class="container text-center">
            <h1 class="title">Student Coordinating Council</h1>
            <p class="lead">SERVE. LEAD. EXCEL.</p>
        </div>
    </div>

    <div class="frame clearfix" data-order="2">
        <div class="container text-center">
            <h2 id="main-title" class="title">Vision</h2>
            <div class="row">
                <div class="col-md-1">
                    <i class="fa fa-quote-left fa-2x"></i>
                </div>
                <div class="col-md-10">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
                        minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit 
                        in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
                        deserunt mollit anim id est laborum.
                    </p>
                </div>
                <div class="col-md-1">
                    <i class="fa fa-quote-right fa-2x"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="frame clearfix" data-order="3">
        <div class="container text-center">
            <h2 class="title">Social</h2>
            <div class="col-md-4">
                <h3>Follow Us</h3>
                <ul class="list-unstyled">
                    <li><a href="https://www.facebook.com/feutechscc/"><i class="fa fa-facebook-official" aria-hidden="true"></i>&nbsp;feutechscc</a></li>
                    <li><a href="https://www.twitter.com/feutechscc/"><i class="fa fa-twitter" aria-hidden="true"></i>&nbsp;@feutechscc</a></li>
                    <li><a href="https://www.instagram.com/feutechscc/"><i class="fa fa-instagram" aria-hidden="true"></i>&nbsp;@feutechscc</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <div class="fb-page" data-href="https://www.facebook.com/feutechscc" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/feutechscc" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/feutechscc">FEU TECH Student Coordinating Council</a></blockquote></div>
            </div>
            <div class="col-md-4">
                <!-- <a class="twitter-timeline" data-width="480" data-height="640" href="https://twitter.com/FEUTechSCC?ref_src=twsrc%5Etfw">Tweets by FEUTechSCC</a> -->
            </div>
        </div>
    </div>

    <div class="frame clearfix" data-order="4">
        <div class="container text-center">
            <h2 class="title">Events</h2>
            <div id="eventCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#eventCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#eventCarousel" data-slide-to="1"></li>
                    <li data-target="#eventCarousel" data-slide-to="2"></li>
                    <li data-target="#eventCarousel" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="<?= base_url('assets/res/img/itam-ambassadors.jpg'); ?>" alt="Event">
                        <div class="carousel-caption">
                            <h3>iTam Ambassador</h3>
                        </div>
                    </div>
                
                    <div class="item">
                        <img src="<?= base_url('assets/res/img/itam-night.jpg'); ?>" alt="Event">
                        <div class="carousel-caption">
                            <h3>Fun Run</h3>
                        </div>
                    </div>
                
                    <div class="item">
                        <img src="<?= base_url('assets/res/img/technofest.jpg'); ?>" alt="Event">
                        <div class="carousel-caption">
                            <h3>TechnoFest</h3>
                        </div>
                    </div>

                    <div class="item">
                        <img src="<?= base_url('assets/res/img/itam-night.jpg') ?>" alt="Event">
                        <div class="carousel-caption">
                            <h3>iTam Night</h3>
                        </div>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#eventCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#eventCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

    <div class="frame clearfix" data-order="5">
        <div class="container text-center">
            <h2 class="title">Officers</h2>
            <div class="row">
                <div class="person col-md-2">
                    <img src="<?= base_url('assets/res/img/office2.jpg'); ?>" class="img-circle" alt="Officer"/>
                    <h3>Lorem Ipsum</h3>
                    <p class="text-center">President</p>
                </div>
                <div class="person col-md-2">
                    <img src="<?= base_url('assets/res/img/office2.jpg'); ?>" class="img-circle" alt="Officer"/>
                    <h3>Lorem Ipsum</h3>
                    <p class="text-center">Vice President</p>
                </div>
                <div class="person col-md-2">
                    <img src="<?= base_url('assets/res/img/office2.jpg'); ?>" class="img-circle" alt="Officer"/>
                    <h3>Lorem Ipsum</h3>
                    <p class="text-center">Secretary</p>
                </div>
                <div class="person col-md-2">
                    <img src="<?= base_url('assets/res/img/office2.jpg'); ?>" class="img-circle" alt="Officer"/>
                    <h3>Lorem Ipsum</h3>
                    <p class="text-center">Treasurer</p>
                </div>
                <div class="person col-md-2">
                    <img src="<?= base_url('assets/res/img/office2.jpg'); ?>" class="img-circle" alt="Officer"/>
                    <h3>Lorem Ipsum</h3>
                    <p class="text-center">Auditor</p>
                </div>
                <div class="person col-md-2">
                    <img src="<?= base_url('assets/res/img/office2.jpg'); ?>" class="img-circle" alt="Officer"/>
                    <h3>Lorem Ipsum</h3>
                    <p class="text-center">Public Relations Officer</p>
                </div>
                <div class="person col-md-2">
                    <img src="<?= base_url('assets/res/img/office2.jpg'); ?>" class="img-circle" alt="Officer"/>
                    <h3>Lorem Ipsum</h3>
                    <p class="text-center">Computer Engineering Representative</p>
                </div>
                <div class="person col-md-2">
                    <img src="<?= base_url('assets/res/img/office2.jpg'); ?>" class="img-circle" alt="Officer"/>
                    <h3>Lorem Ipsum</h3>
                    <p class="text-center">Computer Science Representative</p>
                </div>
                <div class="person col-md-2">
                    <img src="<?= base_url('assets/res/img/office2.jpg'); ?>" class="img-circle" alt="Officer"/>
                    <h3>Lorem Ipsum</h3>
                    <p class="text-center">Electrical Engineering Representative</p>
                </div>
            </div>
        </div>
    </div>

    <div class="frame clearfix" data-order="6">
        <div id="googleMap" style="height:100%;width:100%;"></div>
        <script>
            function myMap() {
                var myCenter = new google.maps.LatLng(14.6041612, 120.9886066);
                var mapProp = {center:myCenter, zoom:18, scrollwheel:false, draggable:false, mapTypeId:google.maps.MapTypeId.ROADMAP};
                var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
                var marker = new google.maps.Marker({position:myCenter});
                marker.setMap(map);
            }
        </script>
    </div>

    <div class="overlay">
        <a class="prev"><i class="fa fa-chevron-left fa-3x" aria-hidden="true"></i></a>
        <a class="next"><i class="fa fa-chevron-right fa-3x" aria-hidden="true"></i></a>
    </div>

</div>
</div>
</div>
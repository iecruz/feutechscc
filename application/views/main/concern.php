<div class="container-fluid bg-info py-5">
    <div class="container">
        <form enctype="multipart/fom-data" action="" method="post">
        <div class="card  bg-light">
            <div class="card-header">
                <h1 class="text-center font-weight-bold">Concern Area</h1>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="nameField">Your name*</label>
                    <input type="text" name="name" id="nameField" class="form-control">
                </div>
                <div class="form-group">
                    <label for="typeField" class="mr-2">Category</label>
                    <select name="type" id="typeField" class="custom-select mr-2">
                        <option value="academic">Academic Policies</option>
                        <option value="events">Events</option>
                        <option value="faculty">Faculty</option>
                        <option value="facilities">Facilites</option>
                        <option value="personnel">Personnel</option>
                        <option value="other">Other</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="commentField">Comment</label>
                    <textarea name="comment" id="commentField" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <div id="receiptFormGroup" class="form-group">
                    <label for="imageField" class="mr-2">Upload Image</label>
                    <label class="custom-file">
                        <input type="file" id="imageField" name="image" class="custom-file-input">
                        <span class="custom-file-control"></span>
                    </label>
                </div>
                <span class="card-text text-muted mr-2">* - optional</span>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        </form>
    </div>
</div>
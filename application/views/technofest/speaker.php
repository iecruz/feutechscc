<div id="speakerMainSection" class="section container-fluid py-5 parallax px-0">
    <h1 class="d-none d-md-block display-4 text-center bold" style="letter-spacing: 2px; color:#036c44;">SPEAKERS</h1>
    <h1 class="d-md-none d-block text-center bold" style="letter-spacing: 2px; color:#036c44;">SPEAKERS</h1>
    <div id="rectangle" class="mb-5"style="width: 80px; height:10px; margin:auto; padding-top:-5%; background-color:#036c44"></div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-5 order-1 order-md-2">
                <img src="<?= base_url('assets/res/technofest/speakers/outline/ludwig-federigan.png') ?>" class="img-fluid my-3 my-md-0 d-block w-75 mx-auto">
            </div>
            <div class="col-12 col-md-7 order-2 order-md-1">
                <div class="card card-body bg-warning my-3">
                    <h1 class="d-none d-md-block bold" style="letter-spacing: 2px; color:#036c44;">LUDWIG FEREDIGAN</h1>
                    <h4 class="d-md-none d-block bold" style="letter-spacing: 2px; color:#036c44;">LUDWIG FEREDIGAN</h4>
                    <h6 class="font-italic bold" style="letter-spacing: 2px; color:#036c44;">Climate Reality Leader of The Climate Reality Project, Philippines</h6>
                    <div id="rectangle" class="mb-3" style="width: 80px; height:10px; padding-top:-5%; background-color:#036c44"></div>

                    <ul>
                        <li>Graduate of Bachelor of Science in Engineering major in Electronics & Communications (BSECE) at the Don Bosco Technical College</li>
                        <li>Graduate of Masters in Business Administration at the University of San Carlos of Cebu City</li>
                        <li>Strategic Business Economic Program graduate at the University of Asia & the Pacific</li>
                        <li>Finished diploma in Supply Chain Management under the Business & Training Linkage Program of the PUM Entrepreneurs for Entrepreneurs at the Hague Netherlands</li>
                        <li>Recipient of a number of fellowships and scholarships on executive development programs on climate change, sustainable consumption and production, green energy and climate finance, and leadership</li>
                        <li>Expert Reviewers on the first order draft of the Intergovernmental Panel on Climate Change (IPCC) Special Report on Global Warming of 1.5ºC (SR15)</li>
                        <li>Miguel R. Magalang Individual Climate Leadership Memorial Awardee given by The Climate Reality Project Philippines</li>
                        <li>Dangal ng Kalikasan Awardee given by the Mister Earth Philippines</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-5 order-1 order-md-1">
                <img src="<?= base_url('assets/res/technofest/speakers/outline/manuel-belino.png') ?>" class="img-fluid my-3 my-md-0 d-block w-75 mx-auto">
            </div>
            <div class="col-12 col-md-7 order-2 order-md-2">
                <div class="card card-body bg-warning my-3">
                    <h1 class="d-none d-md-block bold" style="letter-spacing: 2px; color:#036c44;">DR. MANUEL BELINO</h1>
                    <h4 class="d-md-none d-block bold" style="letter-spacing: 2px; color:#036c44;">DR. MANUEL BELINO</h4>
                    <h6 class="font-italic bold" style="letter-spacing: 2px; color:#036c44;">Senior Director for Engineering of FEU Institute of Technology, Philippines</h6>
                    <div id="rectangle" class="mb-3" style="width: 80px; height:10px; padding-top:-5%; background-color:#036c44"></div>

                    <ul>
                        <li>Bachelor’s degree in Mechanical Engineering, Mapua Institute of Technology, Philippines</li>
                        <li>Masters degree in Engineering Education, De La Salle University. Philippines</li>
                        <li>Master’s degree of Theological Studies, Havard University, USA</li>
                        <li>Doctorate degree in Religious and Values Education, De La Salle University</li>
                        <li>Chair of the Commission on Higher Education (CHED) Technical Committee in Mechanical Engineering</li>
                        <li>Published in local and international journals in the fields of engineeting ethics, engineering education and safety engineering</li>
                        <li>Featured in the Metrobank 50th anniversary publication Ten Outstanding Filipino Teachers</li>
                        <li>President, Philippine Institute of Mechanical Engineering Educators</li>
                        <li>Senior Director for Engineering,Far Eastern University Institute of Technology</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="container-fluid bg-white"> -->
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-5 order-1 order-md-2">
                    <img src="<?= base_url('assets/res/technofest/speakers/outline/arlene-romasanta.png') ?>" class="img-fluid my-3 my-md-0">
                </div>
                <div class="col-12 col-md-7 order-2 order-md-1">
                    <div class="card card-body bg-warning my-3">
                        <h1 class="d-none d-md-block bold" style="letter-spacing: 2px; color:#036c44;">ARLENE ROMASANTA</h1>
                        <h4 class="d-md-none d-block bold" style="letter-spacing: 2px; color:#036c44;">ARLENE ROMASANTA</h4>
                        <h6 class="font-italic bold" style="letter-spacing: 2px; color:#036c44;">OIC, Systems and Infrastructure Development Service (SID) of Department of Information and Communication Technology, Philippines</h6>
                        <div id="rectangle" class="mb-3" style="width: 80px; height:10px; padding-top:-5%; background-color:#036c44"></div>

                        <ul>
                            <li>Bachelor of Science in Computer Data Processing Management, Polytechnic University of the Philippines</li>
                            <li>Master’s degree in Productivity and Quality Management with Leadership Award, Development Academy of the Philippines</li>
                            <li>Took international course, “Formulating and Managing GIS Projects” at the International Institute for Geo-Information Science and Earth Observation, Enschede, The Netherlands</li>
                            <li>Co-Project Lead in the International Collaborative Research on Disaster Response Model Using Vehicle Communications that was funded by Asia Pacific Telecommunity</li>
                            <li>OIC Director of the Systems and Infrastructure Development Service, Department of Information and Communications Technology (DICT)</li>
                            <li>Division Chief/Information Technology Officer 3, Strategic R&D Services Division (SRDSD) Systems and Infrastructure Development Service (SIDS), DICT</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <!-- </div> -->
        
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-5 order-1 order-md-1">
                <img src="<?= base_url('assets/res/technofest/speakers/outline/yacine-petitprez.png') ?>" class="img-fluid my-3 my-md-0">
            </div>
            <div class="col-12 col-md-7 order-2 order-md-2">
                <div class="card card-body bg-warning my-3">
                    <h1 class="d-none d-md-block bold" style="letter-spacing: 2px; color:#036c44;">YACINE PETITPREZ</h1>
                    <h4 class="d-md-none d-block bold" style="letter-spacing: 2px; color:#036c44;">YACINE PETITPREZ</h4>
                    <h6 class="font-italic bold" style="letter-spacing: 2px; color:#036c44;">IT Manager of Ingedata International</h6>
                    <div id="rectangle" class="mb-3" style="width: 80px; height:10px; padding-top:-5%; background-color:#036c44"></div>

                    <ul>
                        <li>IT Manager, Ingedata Philippines</li>
                        <li>Founder and Chief Operating Officer of the Digital Bakery LTD., Philippines</li>
                        <li>IT Consultant of Iscale, Philippines an Web application factory, focused on delivering custom made applications for international customers</li>
                        <li>Trainer of the programming language, Ruby on Rails in France in year 2013</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
        
    <!-- <div class="container-fluid bg-white"> -->
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-5 order-1 order-md-2">
                    <img src="<?= base_url('assets/res/technofest/speakers/outline/rindu-alriavindra.png') ?>" class="img-fluid my-3 my-md-0 d-block w-75 mx-auto">
                </div>
                <div class="col-12 col-md-7 order-2 order-md-1">
                    <div class="card card-body bg-warning my-3">
                        <h1 class="d-none d-md-block bold" style="letter-spacing: 2px; color:#036c44;">RINDU ALRIAVINDRAFUNNY</h1>
                        <h4 class="d-md-none d-block bold" style="letter-spacing: 2px; color:#036c44;">RINDU ALRIAVINDRAFUNNY</h4>
                        <h6 class="font-italic bold" style="letter-spacing: 2px; color:#036c44;">Lecturer of Institute Technology Adisutjipto, Indonesia</h6>
                        <div id="rectangle" class="mb-3" style="width: 80px; height:10px; padding-top:-5%; background-color:#036c44"></div>

                        <ul>
                            <li>Masters degree on Mathematics Education in State University of Surabaya</li>
                            <li>Masters degree on Science Education Communication and Technology (SECT), Utrecht University, Utrecht Belanda</li>
                            <li>Instructor of Elementary, Middle School, and Senior High School in the different schools in Indonesia</li>
                            <li>Mathematics Lecturer of the Aeronatica Engineering Department and the Head of Student Affairs of Institute of Technology Adisutjpto, Yogyakarta, Indonesia</li>
                            <li>Master Course in Science Education Communication and Technology (SECT), Utrecht University, Utrecht Belanda</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <!-- </div> -->
        
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-5 order-1 order-md-1">
                <img src="<?= base_url('assets/res/technofest/speakers/outline/michael-choy.png') ?>" class="img-fluid my-3 my-md-0">
            </div>
            <div class="col-12 col-md-7 order-2 order-md-2">
                <div class="card card-body bg-warning my-3">
                    <h1 class="d-none d-md-block bold" style="letter-spacing: 2px; color:#036c44;">DR. MICHAEL CHOY SENG KIM</h1>
                    <h4 class="d-md-none d-block bold" style="letter-spacing: 2px; color:#036c44;">DR. MICHAEL CHOY SENG KIM</h4>
                    <h6 class="font-italic bold" style="letter-spacing: 2px; color:#036c44;">Founder of Dioworks Learning, Singapore</h6>
                    <div id="rectangle" class="mb-3" style="width: 80px; height:10px; padding-top:-5%; background-color:#036c44"></div>

                    <ul>
                        <li>Senior educator and researcher in the area of facilitation, learning design and development</li>
                        <li>Trainer of more than 5000 adult educators and teachers across China, Indonesia, Malaysia and Singapore in the area of learning styles, design skills and pedagogy</li>
                        <li>Former Assistant Director and Head, Design and Development Unit, Institute for Adult Learning, WDA</li>
                        <li>Manager of curriculum and MOOC (Massive Online Open Courseware) development</li>
                        <li>Founder Dioworks Learning, his own e-learning company, designing MOOCs</li>
                        <li>Facilitator of enterprises to adopt e-learning as part of the workplace learning initiative</li>
                        <li>Published numerous articles in academic journals and conference publications</li>
                        <li>Writer in Educational Media International and Cogent Education discussed the impact of pedagogical beliefs of teachers on technology adoption in the classroom</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="container-fluid bg-white"> -->
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-5 order-1 order-md-2">
                    <img src="<?= base_url('assets/res/technofest/speakers/outline/niel-lopez.png') ?>" class="img-fluid my-3 my-md-0">
                </div>
                <div class="col-12 col-md-7 order-2 order-md-1">
                    <div class="card card-body bg-warning my-3">
                        <h1 class="d-none d-md-block bold" style="letter-spacing: 2px; color:#036c44;">NIEL STEPHEN LOPEZ</h1>
                        <h4 class="d-md-none d-block bold" style="letter-spacing: 2px; color:#036c44;">NIEL STEPHEN LOPEZ</h4>
                        <h6 class="font-italic bold" style="letter-spacing: 2px; color:#036c44;">Assistant Professor, Vice Chair and External Affairs Coordinator, Mechanical Engineering of De La Salle University, Philippines</h6>
                        <div id="rectangle" class="mb-3" style="width: 80px; height:10px; padding-top:-5%; background-color:#036c44"></div>

                        <ul>
                            <li>Bachelors degree in Mechanical Engineering, Magna Cum Laude, De La Salle University</li>
                            <li>Master’s degree in Mechanical Engineering</li>
                            <li>Awardee of Gold Medal for Outstanding Master’s Thesis. His Master’s Thesis is entitled “Energy modeling, fabrication and evaluation of a solar dryer to lower the production cost of biofuel from micoralgae”</li>
                            <li>Candidate for Doctor of Philosophy in Mechanical Engineering in the De La Salle University</li>
                            <li>Visiting PhD Researcher in the different countries</li>
                            <li>Assistant Professor and Vice Chair and External Affairs Coordinator of Mechanical Engineering Department of the De La Salle University, Philippines</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <!-- </div> -->
        
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-5 order-1 order-md-1">
                <img src="<?= base_url('assets/res/technofest/speakers/outline/alberto-amorsolo.png') ?>" class="img-fluid my-3 my-md-0 d-block w-75 mx-auto">
            </div>
            <div class="col-12 col-md-7 order-2 order-md-2">
                <div class="card card-body bg-warning my-3">
                    <h1 class="d-none d-md-block bold" style="letter-spacing: 2px; color:#036c44;">DR. ALBERTO AMORSOLO</h1>
                    <h4 class="d-md-none d-block bold" style="letter-spacing: 2px; color:#036c44;">DR. ALBERTO AMORSOLO</h4>
                    <h6 class="font-italic bold" style="letter-spacing: 2px; color:#036c44;">Professor of the College of Engineering of the University of the Philppines, Philippines</h6>
                    <div id="rectangle" class="mb-3" style="width: 80px; height:10px; padding-top:-5%; background-color:#036c44"></div>

                    <ul>
                        <li>Former Member of Board of Directors and Founding Member, Institute of Materials Engineers of the Philippines</li>
                        <li>Bachelor of Science in Metallurgical Engineering, Cum Laude, University of the Philippines</li>
                        <li>Certified in Mineral Processing and Metallurgy awarded by the Japan International Cooperation Agency and SENKEN in Tohuku University, Sendai, Japan</li>
                        <li>Acquired a Diploma in Extractive Metallurgy that was awarded by SUNKEN in the Tohuku University, Sendai, Japan</li>
                        <li>Masters degree, University of the Philippines</li>
                        <li>Masters degree in Material Science, University of Rochester, New York, USA</li>
                        <li>Doctorate degree in Material Science, University of Rochester, New York, USA</li>
                        <li>Professor, College of Engineering University of the Philppines</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="container-fluid bg-white"> -->
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-5 order-1 order-md-2">
                    <img src="<?= base_url('assets/res/technofest/speakers/outline/rosemary-seva.png') ?>" class="img-fluid my-3 my-md-0">
                </div>
                <div class="col-12 col-md-7 order-2 order-md-1">
                    <div class="card card-body bg-warning my-3">
                        <h1 class="d-none d-md-block bold" style="letter-spacing: 2px; color:#036c44;">DR. ROSEMARY VESA</h1>
                        <h4 class="d-md-none d-block bold" style="letter-spacing: 2px; color:#036c44;">DR. ROSEMARY VESA</h4>
                        <h6 class="font-italic bold" style="letter-spacing: 2px; color:#036c44;">President of the Human Factors and Ergonomics Society of the Philippines, Philippines</h6>
                        <div id="rectangle" class="mb-3" style="width: 80px; height:10px; padding-top:-5%; background-color:#036c44"></div>

                        <ul>
                            <li>Dean of the Gokongwei College of Engineering - De La Salle University</li>
                            <li>Top 250 scholars and scientists in the Philippines from Webometrics in 2015 and 2016</li>
                            <li>Current President of the Human Factors and Ergonomics Society of the Philippines (HFESP) and the past President of the Southeast Asian Network of Ergonomics Societies (SEANES)</li>
                            <li>Doctorate degree, Nanyang Technological University (Singapore)</li>
                            <li>Master's degree in Ergonomics, University of New South Wales (Australia)</li>
                            <li>Master's degree in Industrial Engineering, De La Salle University</li>
                            <li>Ausaid scholar and a recipient of Distinguished Educator Award from the Industrial Engineering and Operations Management (IEOM) Society</li>
                            <li>First Vice President of the Philippine Association of Engineering Schools (PAES) from 2012-2015</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-5 order-1 order-md-1">
                <img src="<?= base_url('assets/res/technofest/speakers/outline/analiza-teh.png') ?>" class="img-fluid my-3 my-md-0 d-block w-75 mx-auto">
            </div>
            <div class="col-12 col-md-7 order-2 order-md-2">
                <div class="card card-body bg-warning my-3">
                    <h1 class="d-none d-md-block bold" style="letter-spacing: 2px; color:#036c44;">ATTY. ANALIZA REBUELTA-TEH</h1>
                    <h4 class="d-md-none d-block bold" style="letter-spacing: 2px; color:#036c44;">ATTY. ANALIZA REBUELTA-TEH</h4>
                    <h6 class="font-italic bold" style="letter-spacing: 2px; color:#036c44;">Interim Undersecretary for Mining of Department of Environment and Natural Resources (DENR), Philippines</h6>
                    <div id="rectangle" class="mb-3" style="width: 80px; height:10px; padding-top:-5%; background-color:#036c44"></div>

                    <ul>
                        <li>Interim Undersecretary For Mining</li>
                        <li>Undersecretary, Climate Change Service Department of Environment and Natural Resources (DENR)</li>
                        <li>Permanent Alternate, Mining Industry Coordinating Council (MICC)</li>
                        <li>Permanent Alternate, Philippine Mining Development Corporation  (PMDC)</li>
                        <li>Permanent Alternate, Mines Adjudication Board (MAB)</li>
                        <li>Focal Point Of The National Designated Authority, Green Climate Fund</li>
                        <li>Alternate Representative To The Undersecretary For Legal Affairs, National Power Corporation (NPC)</li>
                        <li>DENR Representative, National Task Force for the West Philippine Sea</li>
                        <li>CHAIRPERSON, National Gender and Development Focal Point System Department of Environment and Natural Resources (DENR)</li>
                        <li>Permanent Representative, National Advisory Council National Youth Commission (NYC)</li>
                        <li>National Focal Point, East Asian Seas Partnership Council Partnerships in Environmental Management for the Seas of East Asia (PEMSEA)</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="container-fluid bg-white"> -->
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-5 order-1 order-md-2">
                    <img src="<?= base_url('assets/res/technofest/speakers/outline/virgilio-leyba.png') ?>" class="img-fluid my-3 my-md-0">
                </div>
                <div class="col-12 col-md-7 order-2 order-md-1">
                    <div class="card card-body bg-warning my-3">
                        <h1 class="d-none d-md-block bold" style="letter-spacing: 2px; color:#036c44;">VIRGILIO LEYBA</h1>
                        <h4 class="d-md-none d-block bold" style="letter-spacing: 2px; color:#036c44;">VIRGILIO LEYBA</h4>
                        <h6 class="font-italic bold" style="letter-spacing: 2px; color:#036c44;">Information and Technology Services Department Manager of National Power Corporation, Philippines</h6>
                        <div id="rectangle" class="mb-3" style="width: 80px; height:10px; padding-top:-5%; background-color:#036c44"></div>

                        <ul>
                            <li>Graduate of Bachelor of Science in Computer Engineering at the Mapua University and was awarded of Pres. Oscar B. Mapua Silver Scholarship Medal</li>
                            <li>Graduate of Master in Business Administration at the National College of Business and Arts</li>
                            <li>Cisco Certified Network Associate</li>
                            <li>Department Manager, National Power Corporation, Information and Technology Services Department</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <!-- </div> -->
</div>
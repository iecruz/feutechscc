<div id="registerSection" class="section container-fluid py-1 py-md-5 parallax">
    <div class="container text-center bold my-5">
        <h6 class="d-none d-md-block" style="letter-spacing: 0.1rem;">TECHNOFEST 2018 | RESEARCH PRESENTATION</h6>
        <h6 class="d-md-none d-block small" style="letter-spacing: 0.1rem;">TECHNOFEST 2018 | RESEARCH PRESENTATION</h6>
        <h1 class="d-none d-md-block display-2">CONVENTION SCHOLARSHIP</h1>
        <h2 class="d-md-none d-block">CONVENTION SCHOLARSHIP</h2>
        <h6 class="d-none d-md-block" style="letter-spacing: 0.1rem;">APRIL 26, 2018 &bull; FAR EASTERN UNIVERSITY - INSTITUTE OF TECHNOLOGY </h6>
        <h6 class="d-md-none d-block small" style="letter-spacing: 0.1rem;">APRIL 26, 2018 &bull; FAR EASTERN UNIVERSITY - INSTITUTE OF TECHNOLOGY </h6>
    </div>

    <div class="container card card-body mb-2">
        <h5 class="card-title font-weight-bold text-uppercase text-center" style="letter-spacing: 0.1rem;">Convention Scholarship</h5>
        <p class="card-text">One of our aims is to ensure that accepted delegates will be able to attend our conferences. We wish to honor delegates who have shown exceptional merit and financial aid. With that in mind, we offer scholarships to accepted delegates. Details are as follows:</p>
        <br>
        <p class="card-text">Approximately <span class="font-weight-bold">10 full scholarships</span> are available.</p>
        <br>
        <p class="card-text">For scholarship applications one must first complete the basic information sheet below. Afterwards, scholars must submit papers for presentation related to the conference theme, <span class="font-weight-bold">“Fortifying Human Values and Technology in Achieving Sustainable Innovations.”</span> Deadline of submission of papers will be <span class="font-weight-bold">until April 17,2018 at 11:59 P.M.</span>  Any applications received after April 17, 2018 will not be considered.</p>
        <br>
        <p class="card-text mb-0">Inclusion:</p>
        <ul>
            <li>Convention Fee</li>
            <li>Food for 3 days and 2 nights</li>
        </ul>
    </div>

    <div class="container card card-body mb-2">
        <div id="registerCollapse" class="collapse show">
        <form onsubmit="search_participant(event)">
        <h5 class="text-uppercase font-weight-bold" style="letter-spacing: 0.1rem;">Apply for Scholarship</h5>
        <div class="row">
            <div class="col-12 col-md-9 form-group">
                <label for="emailAddressField">Email Address</label>
                <input type="email" class="form-control" id="emailAddressField" name="email_address" required>
            </div>
            <div class="col-12 col-md-3 align-self-end">
                <button type="submit" class="btn btn-success btn-block text-uppercase font-weight-bold mb-md-3">Enter</button>
            </div>
        </div>
        </form>
        <div id="redirectCollapse" class="collapse">
            <hr>
            <p class="card-text text-center">You are not yet registered as a <span class="font-weight-bold">presenter</span> for TechnoFest 2018. Kindly register first by clicking the button below.</p>
            <div class="row">
                <div class="col-6">
                    <?= form_open('technofest/register') ?>
                    <?= form_hidden('locale', 'local') ?>
                    <?= form_hidden('accommodation', 'live_out') ?>
                    <button type="submit" class="d-none d-md-block btn bold btn-lg text-white btn-dark w-75 mx-auto">LOCAL</button>
                    <button type="submit" class="d-md-none d-block btn bold text-white btn-dark w-75 mx-auto">LOCAL</button>
                    <?= form_close() ?>
                </div>
                <div class="col-6">
                    <?= form_open('technofest/register') ?>
                    <?= form_hidden('locale', 'international') ?>
                    <?= form_hidden('accommodation', 'live_in') ?>
                    <button type="submit" class="d-none d-md-block btn bold btn-lg text-white btn-dark w-75 mx-auto">INTERNATIONAL</button>
                    <button type="submit" class="d-md-none d-block btn bold text-white btn-dark w-75 mx-auto">INTERNATIONAL</button>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
        </div>

        <div id="paperCollapse" class="collapse">
        <h5 class="text-uppercase font-weight-bold" style="letter-spacing: 0.1rem;">Background Information</h5>
        <p class="card-text">Name: <span id="registerName"></span></p>
        <p class="card-text">School: <span id="registerCompany"></span></p>
        <p class="card-text">Country: <span id="registerCountry"></span></p>
        <p class="card-text">Career: <span id="registerCareer"></span></p>
        <hr>

        <form onsubmit="submit_application(event)">
        <input type="hidden" id="participantField" name="participant_id">
        <h5 class="text-uppercase font-weight-bold" style="letter-spacing: 0.1rem;">Paper</h5>
        <div class="row">
            <div class="col-12 form-group">
                <p class="card-text">Kindly upload your paper in PDF, DOC, or DOCX file type.</p>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="paper_pdf">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
            </div>
        </div>

        <hr>

        <h5 class="text-uppercase font-weight-bold" style="letter-spacing: 0.1rem;">Essay Questions</h5>
        <div class="row">
            <div class="col-12 form-group">
                <p class="card-text">1. Albert Einstein once stated, it has become appaillingly obvious that our technology has exceeded our humanity. What is the effect of technology to the people in the aspect of human values?</p>
                <textarea class="form-control" name="answer_1" style="resize:none;" required></textarea>
            </div>
            <div class="col-12 form-group">
                <p class="card-text">2. With the theme "Fortifying Human Values and Technology in Achieving Sustainable Innovations", how can we innovate technology while maintaining high regard to human values?</p>
                <textarea class="form-control" name="answer_2" style="resize:none;" required></textarea>
            </div>
        </div>
        <button type="submit" class="btn btn-success btn-block btn-lg w-50 mx-auto text-uppercase font-weight-bold mt-3">Submit</button>
        </form>

        </div>
    </div>
</div>
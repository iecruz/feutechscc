<div class="container-fluid">
    <div class="row justify-content-center align-items-stretch">

        <div class="col-12 col-md-6 parallax" style="background-image: url(<?= base_url('assets/res/technofest/bg/dark.jpg') ?>);">
            <div class="row justify-content-center align-content-center h-100 p-3 p-md-5">
                <div class="col-12">
                    <img src="<?= base_url('assets/res/technofest/logo/logo-white.png') ?>" class="d-none d-md-block mx-auto img-fluid py-4" alt="logo">
                    <img src="<?= base_url('assets/res/technofest/logo/logo-white.png') ?>" class="d-block d-md-none img-fluid w-100" alt="logo">
                </div>
                
                <div class="w-100"></div>

                <div class="col-12">
                    <h1 class="d-none d-md-block text-white text-center bold" style="letter-spacing: 2px;">REGISTRATION FORM</h1>
                    <h4 class="d-md-none d-block text-white text-center my-3 bold" style="letter-spacing: 2px;">REGISTRATION FORM</h4>
                    <div id="rectangle" style="width: 80px; height:10px; background-color:white; margin:auto; padding-top:-5%;"></div>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6" style="background-image: url(<?= base_url('assets/res/technofest/bg/register.jpg') ?>); background-size: cover;">
            <?= form_open(base_url('technofest/register/'.$locale), ['id' => 'registerForm']) ?>
            <?= form_hidden('locale', $locale) ?>
            <div class="row py-5 px-4">
                <div class="form-group col-12 col-md-6">
                    <label for="firstNameField" class="h6">FIRST NAME</label>
                    <input type="text" name="first_name" class="form-control" id="firstNameField" value="<?= set_value('first_name') ?>">
                    <small class="form-text text-danger"><?= form_error('first_name') ?></small>
                </div>

                <div class="form-group col-12 col-md-6">
                    <label for="lastNameField" class="h6">LAST NAME</label>
                    <input type="text" name="last_name" class="form-control" id="lastNameField" value="<?= set_value('last_name') ?>">
                    <small class="form-text text-danger"><?= form_error('last_name') ?></small>
                </div>

                <div class="form-group col-12 col-md-6">
                    <label for="emailField" class="h6">EMAIL ADDRESS</label>
                    <input type="email" name="email_address" class="form-control" id="emailField" value="<?= set_value('email_address') ?>">
                    <small class="form-text text-danger"><?= form_error('email_address') ?></small>
                </div>

                <div class="form-group col-12 col-md-6">
                    <label for="emailField" class="h6">CONTACT NUMBER</label>
                    <input type="tel" name="contact_number" class="form-control" id="contactNumberField" value="<?= set_value('contact_number') ?>">
                    <small class="form-text text-danger"><?= form_error('contact_number') ?></small>
                </div>
                
                <?php if($locale == 'international'): ?>
                <div class="form-group col-12">
                    <label for="countryField" class="h6">COUNTRY</label>
                    <input type="text" name="country" class="form-control" id="countryField" value="<?= set_value('country') ?>">
                    <small class="form-text text-danger"><?= form_error('country') ?></small>
                </div>
                <?php endif; ?>

                <div class="form-group col-12">
                    <label for="schoolField" class="h6">COMPANY / SCHOOL</label>
                    <input type="text" name="school" class="form-control" id="schoolField" value="<?= set_value('school') ?>">
                    <small class="form-text text-danger"><?= form_error('school') ?></small>
                </div>

                <div class="form-group col-12 col-md-6">
                    <label for="careerField" class="h6">CAREER</label>
                    <select name="career" class="custom-select" id="careerField">
                        <option value="student">Student</option>
                        <option value="professional">Professional</option>
                    </select>
                </div>

                <?php if($locale == 'local'): ?>
                <div class="form-group col-12 col-md-6">
                    <label for="accommodationField" class="h6">ACCOMMODATION</label>
                    <select name="accommodation" class="custom-select" id="accommodationField">
                        <option value="live_in" <?= $accommodation == 'live_in' ? 'selected' : '' ?>>Live In</option>
                        <option value="live_out" <?= $accommodation == 'live_out' ? 'selected' : '' ?>>Live Out</option>
                    </select>
                </div>
                <?php endif; ?>

                <div class="form-group col-12 col-md-6">
                    <label for="foodField" class="h6">FOOD PREFERENCE</label>
                    <select name="food_preference" class="custom-select" id="foodField">
                        <option value="regular">Regular</option>
                        <option value="vegetarian">Vegetarian</option>
                        <option value="halal">Halal</option>
                    </select>
                </div>

                <div class="form-group col-12">
                    <div id="competitionCollapse" class="collapse">
                        <label for="competitionField" class="h6">COMPETITION</label>
                        <select name="competition" class="custom-select" id="competitionField">
                            <?php foreach($competitions as $competition): ?>
                            <option value="<?= $competition['id'] ?>"><?= $competition['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="col-12 my-1">
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="attendeeCheckField" name="attendee" value="true" checked>
                        <label class="custom-control-label h6" for="attendeeCheckField">ATTENDEE</label>
                    </div>

                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="presenterCheckField" name="presenter" value="true">
                        <label class="custom-control-label h6" for="presenterCheckField">PRESENTER</label>
                    </div>

                    <div class="custom-control custom-checkbox custom-control-inline">
                        <button type="button" class="d-none" data-target="#competitionCollapse" data-toggle="collapse"></button>
                        <input type="checkbox" class="custom-control-input" id="competitorCheckField" name="competitor" value="true">
                        <label class="custom-control-label h6" for="competitorCheckField">COMPETITOR</label>
                    </div>
                </div>

                <div class="col-12 my-1">
                    <p id="formError" class="text-danger font-weight-bold"></p>
                </div>

                <div class="col-12 my-3">
                    <button type="submit" form="registerForm" class="btn btn-dark btn-lg btn-block bold" style="font-size: 1.75rem;">REGISTER</button>
                </div>
            </div>
            <?= form_close() ?>
        </div>

    </div>
</div>
<div id="competitionMainSection" class="section container-fluid px-md-5 py-5">
    <h3 class="d-none d-md-block text-white bold" style="letter-spacing: 15px;">TECHNOFEST 2018's</h3>
    <h6 class="d-md-none d-block text-white bold" style="letter-spacing: 1.5px;">TECHNOFEST 2018's</h6>
    <h1 class="d-none d-md-block display-3 text-success bold">ACADEMIC COMPETITIONS</h1>
    <h1 class="d-md-none d-block text-success bold">ACADEMIC COMPETITIONS</h1>
    <a href="<?= base_url('technofest/register') ?>" class="d-none d-md-inline-block btn btn-outline-success btn-lg bold" style="border-width: 0.2rem; font-size: 1.5rem;">REGISTER NOW!</a>
    <a href="<?= base_url('technofest/register') ?>" class="d-md-none d-inline-block btn btn-outline-success btn-lg bold" style="border-width: 0.2rem;">REGISTER NOW!</a>
</div>

<div id="competitionOverviewSection" class="section container-fluid">
    <?php $ctr = 0; foreach($competitions as $competition): ?>
    <div class="row align-items-center py-2 py-md-4 px-0 <?= $ctr % 2 == 0 ? 'bg-white' : 'bg-success text-white' ?>">
        <div class="col-12 col-md-6 order-md-<?= $ctr % 2 == 0 ? '1' : '2' ?> text-justify py-2 py-md-0">
            <img src="<?= base_url('assets/res/technofest/competitions/'.$competition['id'].'.png') ?>" class="img-fluid" alt="competition">
        </div>

        <div class="col-12 col-md-6 order-md-<?= $ctr % 2 == 0 ? '2' : '1' ?> px-3 px-md-5">
            <p class="d-none d-md-block text-justify" style="letter-spacing: 0.1rem;"><?= $competition['description'] ?></p>
            <p class="d-md-none d-block text-justify"><?= $competition['description'] ?></p>

            <h4 class="d-none d-md-block text-warning text-uppercase font-weight-bold mb-4">
                Registration Fee: 
                <span class="font-weight-bold h3"><?= $competition['price'] > 0.00 ? '$ '.$competition['price'] : 'FREE' ?><span>
                <?php if($competition['price'] != 0.00): ?>
                <small> per <span class="font-weight-bold"><?= $competition['unit'] ?></span></small>
                <?php endif; ?>
            </h4>

            <h6 class="d-md-none d-block text-warning text-uppercase font-weight-bold mb-4">
                Registration Fee: 
                <span class="font-weight-bold h5"><?= $competition['price'] > 0 ? '$ '.$competition['price'] : 'FREE' ?><span>
                <?php if($competition['price'] != 0.00): ?>
                <small> per <span class="font-weight-bold"><?= $competition['unit'] ?></span></small>
                <?php endif; ?>
            </h6>

            <a href="<?= base_url("assets/res/technofest/downloads/competition_guidelines/{$competition['id']}.pdf") ?>" class="d-block d-md-inline-block btn btn-warning btn-lg text-uppercase mb-4" download="<?= $competition['name'] ?> Mechanics.pdf">Download Mechanics</a>
        </div>
    </div>
    <?php ++$ctr; endforeach; ?>
</div>

<div id="competitionRegisterSection" class="section container-fluid py-5 bt">
    <div class="container">
        <a href="<?= base_url('technofest/register') ?>" class="btn btn-success btn-lg btn-block bold" style="border-width: 0.2rem; font-size: 1.5rem;">REGISTER NOW!</a>
    </div>
    <!-- <a href="<?= base_url('technofest/register') ?>" class="d-none d-md-inline-block btn btn-outline-success btn-lg bold" style="border-width: 0.2rem; font-size: 1.5rem;">REGISTER NOW!</a> -->
    <!-- <a href="<?= base_url('technofest/register') ?>" class="d-md-none d-inline-block btn btn-outline-success btn-lg bold" style="border-width: 0.2rem;">REGISTER NOW!</a> -->
</div>
<div id="otherAcademicDisciplinesModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content text-white bg-success">
        <div class="modal-body p-md-5">
            <h6 class="text-white text-center bold mb-0">TOPICS UNDER</h6>
            <h1 class="text-warning text-center bold mb-0">OTHER ACADEMIC DISCIPLINES</h1>
            <h6 class="text-white text-uppercase text-center bold mb-4">(Studies with emphasis on Human Values and Technology)</h6>
            <div class="row">
                <div class="col-12 col-12 offset-md-3">
                    <ul>
                        <li>Social Sciences and Humanities</li>
                        <li>Environment</li>
                        <li>Business and Economics</li>
                        <li>Communication</li>
                        <li>Social Development</li>
                        <li>Medicine </li>
                    </ul>
                </div>
            </div>
            <button type="button" class="btn btn-warning d-block mx-auto" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>

<div id="engineeringModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content text-white bg-success">
        <div class="modal-body p-md-5">
            <h6 class="text-white text-center bold mb-0">TOPICS UNDER</h6>
            <h1 class="text-warning text-center bold mb-4">ENGINEERING</h1>
            <div class="row">
                <div class="col-12 col-md-6">
                    <ul class="mb-0">
                        <li>Earthquake Engineering</li>
                        <li>Environmental Engineering </li>
                        <li>Fluid Dynamics</li>
                        <li>Geotechnical Engineering</li>
                        <li>Structural Engineering</li>
                        <li>Natural Resources Engineering</li>
                        <li>Nanotechnology</li>
                        <li>Power Systems</li>
                        <li>Power Electronics</li>
                        <li>Networking</li>
                        <li>Acoustics</li>
                        <li>Microwave and RF</li>
                        <li>Computational Imaging</li>
                        <li>Signal Processing</li>
                        <li>Traffic</li>
                        <li>Pavements</li>
                    </ul>
                </div>
                <div class="col-12 col-md-6">
                    <ul class="mb-0">
                        <li>Transportation</li>
                        <li>Biomedical Engineering</li>
                        <li>Robotics and Control</li>
                        <li>Acoustics</li>
                        <li>Energy Systems</li>
                        <li>Vibrations and Applied Mechanics</li>
                        <li>Materials Engineering</li>
                        <li>Electrospinning</li>
                        <li>Fluid Mechanics</li>
                        <li>Instrumentation and Control</li>
                        <li>Manufacturing</li>
                        <li>Stirling-Cycle</li>
                        <li>Natural Materials and Biocomposites</li>
                        <li>Robotics, Control and Instrumentation</li>
                        <li>Thermodynamics</li>
                        <li>Other related topics </li>
                    </ul>
                </div>
            </div>
            <button type="button" class="btn btn-warning d-block ml-auto" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>

<div id="informationTechnologyModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content text-white bg-success">
        <div class="modal-body p-md-5">
            <h6 class="text-white text-center bold mb-0">TOPICS UNDER</h6>
            <h1 class="text-warning text-center bold mb-4">INFORMATION TECHNOLOGY</h1>
            <div class="row">
                <div class="col-12 col-md-6">
                    <ul class="mb-0">
                        <li>Firewalls and Internet Security</li>
                        <li>Research Laboratory on Network</li>
                        <li>Digital Arts</li>
                        <li>Multi-Media</li>
                        <li>Cyber Security</li>
                        <li>Data Base Management IT</li>
                        <li>Data Mining and Data Fusion</li>
                        <li>Digital Arts</li>
                        <li>E-Commerce and E-Government</li>
                        <li>E-Learning and E-Business</li>
                    </ul>
                </div>
                <div class="col-12 col-md-6">
                    <ul class="mb-0">
                        <li>Emerging Technologies and Applications</li>
                        <li>Games and Simulations</li>
                        <li>ICT & Education</li>
                        <li>Management Information Systems</li>
                        <li>Mobile Networks and Services</li>
                        <li>Image and Data Security</li>
                        <li>Internet Applications and Performances</li>
                        <li>Virtual Schooling</li>
                        <li>Other related topics</li>
                    </ul>
                </div>
            </div>
            <button type="button" class="btn btn-warning d-block ml-auto" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>

<div id="computerScienceModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content text-white bg-success">
        <div class="modal-body p-md-5">
            <h6 class="text-white text-center bold mb-0">TOPICS UNDER</h6>
            <h1 class="text-warning text-center bold mb-4">COMPUTER SCIENCE</h1>
            <div class="row">
                <div class="col-12 col-md-6">
                    <ul class="mb-0">
                        <li>Algorithm Engineering</li>
                        <li>Computational Intelligence</li>
                        <li>Computer Graphics and Image Processing</li>
                        <li>Computer Vision</li>
                        <li>Human Computer Interaction and Multimedia Lab</li>
                        <li>Computer Music</li>
                        <li>Intelligent and Adaptive Educational Systems</li>
                        <li>Management, Performance, and Simulation </li>
                        <li>Software Engineering and Visualisation Group</li>
                        <li>Knowledge Management and & Decision Making</li>
                    </ul>
                </div>
                <div class="col-12 col-md-6">
                    <ul class="mb-0">
                        <li>Mathematical Models</li>
                        <li>Mathematical Modelling Simulation</li>
                        <li>Mobile Networks and Services</li>
                        <li>Signal and Image Processing</li>
                        <li>Sixth Sense Technology</li>
                        <li>Speech and Audio Processing</li>
                        <li>Software Engineering</li>
                        <li>Data Base Management IT</li>
                        <li>Data Mining and Data Fusion</li>
                        <li>Other related topics</li>
                    </ul>
                </div>
            </div>
            <button type="button" class="btn btn-warning d-block ml-auto" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>


<div id="paperMainSection" class="section container-fluid py-5">
    <div class="container text-center bold my-5">
        <h6 class="d-none d-md-block" style="letter-spacing: 0.1rem;">TECHNOFEST 2018 | RESEARCH PRESENTATION</h6>
        <h6 class="d-md-none d-block small" style="letter-spacing: 0.1rem;">TECHNOFEST 2018 | RESEARCH PRESENTATION</h6>
        <h1 class="d-none d-md-block display-2 green">CALL FOR PAPERS</h1>
        <h2 class="d-md-none d-block green">CALL FOR PAPERS</h2>
        <h6 class="d-none d-md-block" style="letter-spacing: 0.1rem;">APRIL 26, 2018 &bull; FAR EASTERN UNIVERSITY - INSTITUTE OF TECHNOLOGY </h6>
        <h6 class="d-md-none d-block small" style="letter-spacing: 0.1rem;">APRIL 26, 2018 &bull; FAR EASTERN UNIVERSITY - INSTITUTE OF TECHNOLOGY </h6>
    </div>
<!--</div>-->

<!--<div id="paperAboutSection" class="section container-fluid py-5 py-md-0">-->
    <div class="container bg-warning py-5 p-md-5">
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <h1 class="d-none d-md-block display-2 text-white bold">ABOUT THE EVENT</h1>
                <h2 class="d-md-none d-block text-white text-center bold mb-3">ABOUT THE EVENT</h2>
            </div>
            <div class="col-12 col-md-6 text-justify">
                <p><span class="font-weight-bold">FEU Institute of Technology</span> (formerly FEU – East Asia College) is a vibrant community of technology scholars and professionals seeking to serve the Nation, the Industry, and the World. FEU Tech is proud to provide the ultimate academic experience. Its rich, vibrant community allows students to make life-long friends, have unforgettable moments, get real-world training, and enjoy the ideal campus life. From experiencing the world through FEU Tech’s study abroad program or networking with over 700 industry partners, students get to live college like nowhere else in the world.</p>
                <p>The <span class="font-weight-bold">1st INTERNATIONAL TECHNOLOGY FESTIVAL 2018: Convention on Information Technology and Engineering</span> with the theme <span class="font-weight-bold font-italic">“Fortifying Technology and Human Values in Achieving Sustainable Innovations”</span> scheduled on <span class="font-weight-bold">April 25-27, 2018</span> at <span class="font-weight-bold">FEU Institute of Technology, Sampaloc, Manila, Philippines</span>.</p>
            </div>
        </div>
    </div>
<!--</div>-->

<!--<div id="paperGeneralGuidelinesSection" class="section container-fluid py-3 py-md-5">-->
    <div class="container py-5">
        <div class="row">
            <div class="col-12 text-center mb-3 bold">
                <h5 class="font-italic" style="letter-spacing: 0.1rem;">GUIDELINES</h5>
                <h1 class="d-none d-md-block display-3 green">GENERAL</h1>
                <h1 class="d-md-none d-block green">GENERAL</h1>
            </div>

            <div class="col-12 col-md-6 text-justify px-md-5">
                <p>Any delegate to the <span class="text-success font-weight-bold">TECHNOLOGY FESTIVAL 2018</span> is eligible to present his/her research during the paper presentation.</p>
                <p>A delegate who wishes to present his/her research output/s must submit the his/her manuscript in <span class="font-weight-bold"><span class="text-success">MS Word format</span> on or before February 28, 2018 (Wednesday)</span> to
                    <ul class="ml-3 list-unstyled font-weight-bold">
                        <li>momaglipas@feutech.edu.ph</li>
                        <li>rqdamian@feutech.edu.ph</li>
                        <li>jruedas26@gmail.com</li>
                    </ul>
                </p>
                <p>The manuscript must be written in accordance with the prescribed format of the organizer. This will be <span class="text-success font-weight-bold">TEN (10) minutes presentation</span> and <span class="text-success font-weight-bold">FIVE (5) minutes question and answer</span>.</p>
                <p>The submitted manuscript <span class="font-weight-bold text-success">must not exceed 15 pages</span> and must contain the following:
                    <ol type="a" class="">
                        <li>Title Page</li>
                        <li>Author/s, Affiliations, Email Address, and Contact Number/s</li>
                        <li>Abstract – <span class="font-weight-bold text-success">Deadline is on April 20, 2018 (Friday)</span></li>
                        <li>Introduction</li>
                        <li>Methodology</li>
                        <li>Results and Discussions</li>
                        <li>Conclusions, Implications, and Recommendations of the Study</li>
                        <li>References or Literature Cited  </li>
                    </ol>
                </p>
            </div>

            <div class="col-12 col-md-6 text-justify px-md-5">
                <p>The Documentation and Program, Research Committees shall evaluate the manuscript and shall communicate to you the result <span class="text-success font-weight-bold">at least one month after your submission</span>.</p>
                <p>A delegate who submitted his/her research output <span class="text-success font-weight-bold">shall receive a letter of acceptance from the FEU TECH’s Documentation and Program, Research Committees</span>. Attached to the letter of acceptance is the schedule of the presentation as well as the specific strand in which his/her research belongs.</p>
                <p>Only <span class="text-success font-weight-bold">ten (10) papers/researches from Engineering, ten (10) papers/researches from Information Technology and fifteen (15) papers from other Academic Fields</span> will be ACCEPTED in the convention.</p>
                <p>Topic Papers/Researches would focus on the following:
                    <ul class="h5 text-left">
                        <li class="mb-2">
                            <a class="text-success font-weight-bold" href="#otherAcademicDisciplinesModal" data-toggle="modal"><ins>OTHER ACADEMIC DISCIPLINES</ins></a>
                            <br><small class="text-dark">(Studies with emphasis on Human Values and Technology)</small>
                        </li>
                        <li class="mb-2">
                            <a class="text-success font-weight-bold" href="#engineeringModal" data-toggle="modal"><ins>ENGINEERING</ins></a>
                        </li>
                        <li class="mb-2">
                            <a class="text-success font-weight-bold" href="#informationTechnologyModal" data-toggle="modal"><ins>INFORMATION TECHNOLOGY</ins></a>
                        </li>
                        <li>
                            <a class="text-success font-weight-bold" href="#computerScienceModal" data-toggle="modal"><ins>COMPUTER SCIENCE</ins></a>
                        </li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
<!--</div>-->

<div id="rectangle" class="mb-5" style="width: 50%; height:5px; margin:auto; background-color:#d0cece;"></div>
<!--<div id="paperSpecifiGuidelinesSection" class="section container-fluid py-3 py-md-5">-->
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3 bold">
                <h5 class="font-italic" style="letter-spacing: 0.1rem;">GUIDELINES</h5>
                <h1 class="d-none d-md-block display-3 green">SPECIFIC</h1>
                <h1 class="d-md-none d-block green">SPECIFIC</h1>
            </div>

            <div class="col-12 col-md-6 text-justify px-md-5">
                <p>A delegate who will submit his/her research output must use the following format:
                    <ol type="a">
                        <li><span class="text-success font-weight-bold">Introduction</span>. This explains the rationale of the study, the purpose and objectives of the study, and the significance of the study. This is intended to establish a strong background of the need to conduct the study.</li>
                        <li><span class="text-success font-weight-bold">Methodology</span>. This is the most important part of the article. It should include, among others, the research design, the subjects or respondents, the sample and the sampling technique, the data collection procedure, data analysis and the duration of the study. This part is the most often carefully scrutinized by the reviewers and readers.</li>
                        <li><span class="text-success font-weight-bold">Results and Discussion</span>. This contains the important findings of the study and the researcher’s discussion and analysis of the findings vis-à-vis the research objectives or the research questions.</li>
                        <li><span class="text-success font-weight-bold">Conclusions, Implications and Recommendations</span>. This contains the conclusion of the study, the implications of the study for policy and practice and also recommendations of the study.</li>
                        <li><span class="text-success font-weight-bold">References</span>. This contains the bibliography of the references you have used in the study. Parenthetical documentation if often used in journal articles as it saves a lot of space.</li>
                    </ol>
                </p>
            </div>

            <div class="col-12 col-md-6 text-justify px-md-5">
                <p>A delegate whose research paper is accepted for presentation must prepare a <span class="text-success font-weight-bold">10-minute PowerPoint presentation</span> of his/her study.</p>
                <p>There will be <span class="text-success font-weight-bold">five minutes interval</span> after the presentation to give time for the panel and the audience to ask questions and to clarify from the presenter.</p>
                <p>The research presenter will be given a certificate and a token of appreciation as a symbol of gratitude of FEU Institute of Technology for his/her time and effort to present his/her study which somehow encourages other people to also conduct researches.</p>
                <p>The best paper from each discipline (Engineering, Information Technology and Other Academic Discipline) will receive a certificate and cash prize.</p>
            </div>
        </div>
    </div>
</div>

<div id="paperRegisterSection" class="section container-fluid bg-warning text-black p-5">
    <div class="row align-items-center">
        <div class="col-12 col-md-6">
            <h4 class="d-none d-md-block mb-0" style="letter-spacing: 0.1em;">TECHNOFEST 2018 | RESEARCH PRESENTATION</h4>
            <p class="d-md-none d-block text-center mb-0" style="font-size: 0.75rem; letter-spacing: 0.1em;">TECHNOFEST 2018 | RESEARCH PRESENTATION</p>
            <h1 class="d-none d-md-block display-3 bold mb-0">CALL FOR PAPERS</h1>
            <h2 class="d-md-none d-block text-center bold mb-2">CALL FOR PAPERS</h2>
        </div>

        <div class="col-12 col-md-6 text-center">
            <a href="<?= base_url('technofest/register') ?>" class="d-none d-md-block btn btn-success btn-lg btn-block bold" style="font-size: 2.5rem;">REGISTER</a>
            <a href="<?= base_url('technofest/register') ?>" class="d-md-none d-inline btn btn-success btn-lg bold">REGISTER</a>
        </div>
    </div>
</div>

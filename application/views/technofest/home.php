<div id="mainSection" class="section container-fluid front-page parallax">
    <div class="container">
        <div class="row justify-content-center align-content-center full-page">
            <div class="col-12 my-2 text-center">
                <img src="<?= base_url('assets/res/technofest/logo/logo.png') ?>" class="d-none d-md-block mx-auto img-fluid mb-3" width="70%" alt="logo">
                <img src="<?= base_url('assets/res/technofest/logo/logo.png') ?>" class="d-block d-md-none img-fluid w-100 mb-3" alt="logo">
                <h4 class="d-none d-md-block text-center font-weight-bold w-75 mx-auto">"Fortifying Technology and Human Values in Achieving Sustainable Innovations"</h4>
                <p class="d-md-none d-block text-center font-weight-bold w-75 mx-auto" style="line-height: 1.2rem;">"Fortifying Technology and Human Values in Achieving Sustainable Innovations"</p>
                <h5 class="d-none d-md-block bold" style="letter-spacing: 0.5px;">April 25-27, 2018&nbsp;&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;Far Eastern University Institute of Technology&nbsp;&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;Manila, Philippines</h4>
                <h6 class="d-md-none d-block bold" style="letter-spacing: 0.5px;">April 25-27, 2018<br>Far Eastern University Institute of Technology<br>Manila, Philippines</h6>
            </div>
            <div class="w-100"></div>
            <div class="col-12 py-2 text-center scroll-spy-nav">
                <a href="#registerSection" class="d-none d-md-inline-block mx-auto mt-2 btn btn-outline-dark btn-lg bold" style="border-width: .25rem; font-size: 2rem;">REGISTER NOW</a>
                <a href="#registerSection" class="d-inline-block d-md-none mx-auto btn btn-outline-dark btn-lg w-75 bold" style="border-width: .2rem; font-size: 1.25rem;">REGISTER NOW</a>
            </div>
        </div>
    </div>
</div>

<div id="aboutSection" class="section container-fluid">
    <div class="row">
        <div class="col-12 col-md-4 d-flex align-items-center p-5">
            <div class="row bold">
                <div class="col-12">
                    <p class="d-none d-md-block bold space">
                        <span class="display-4 text-white">ABOUT</span><br>
                        <span class="display-4 green">TECHNOFEST</span><br>
                        <span class="display-4" style="color:#353535;">2018</span>
                    </p>
                    <p class="d-md-none d-block bold" style="line-height: 1rem;">
                        <span class="h1 text-white">ABOUT</span><br>
                        <span class="h1 green">TECHNOFEST</span><br>
                        <span class="h1" style="color:#353535;">2018</span>
                    </p>
                    <div id="rectangle" style="width: 80px; height:10px; background-color:white"></div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-8" style="background-color:#dfdfdf;">
            <div class="row align-items-center p-4 p-md-5">
                <div class="w-100"></div>
                <div class="col-12">
                    <p class="d-none d-md-block text-justify my-md-5 mx-md-4" style="letter-spacing: .1rem; line-height: 1.75rem;">
                    TECHNOLOGY FESTIVAL OR TECHNOFEST 2018 is an annual event organized by the FEU Institute of Technology that gathers professionals, scientists, faculty and students to celebrate technology and the different innovations that made an impact on people and their communities. This year, TECHNOFEST focuses on the theme “Fortifying Human Values and Technology in Sustainable Innovations”. Delegates from different colleges and universities are invited to be part of the workshops, seminars and talks featuring distinguished personalities from the industry. Some of the topics to be discussed are the challenges and fallbacks of technology and its effect in human values. <br><br>
                    This year, TECHNOFEST has invited delegates from international countries from nearby Asia Pacific Region. It is the event’s goal to be able to learn from our counterparts abroad as they share with us their discoveries about technology and how we can apply them to our environment.
                    </p>
                    <p class="d-md-none d-block my-3 text-justify" style="line-height: 1.75rem;">
                    TECHNOLOGY FESTIVAL OR TECHNOFEST 2018 is an annual event organized by the FEU Institute of Technology that gathers professionals, scientists, faculty and students to celebrate technology and the different innovations that made an impact on people and their communities. This year, TECHNOFEST focuses on the theme “Fortifying Human Values and Technology in Sustainable Innovations”. Delegates from different colleges and universities are invited to be part of the workshops, seminars and talks featuring distinguished personalities from the industry. Some of the topics to be discussed are the challenges and fallbacks of technology and its effect in human values. <br><br>
                    This year, TECHNOFEST has invited delegates from international countries from nearby Asia Pacific Region. It is the event’s goal to be able to learn from our counterparts abroad as they share with us their discoveries about technology and how we can apply them to our environment.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="historySection" class="section container-fluid bg-light py-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 py-4">
                <h1 class="d-none d-md-block display-4 text-center bold" style="letter-spacing: 2px;">HISTORY OF TECHNOFEST</h1>
                <h1 class="d-md-none d-block text-center bold" style="letter-spacing: 2px;">HISTORY OF TECHNOFEST</h1>
                <div id="rectangle" style="background-color: #000; width: 80px; height:10px; margin:auto; padding-top:-5%;"></div>
            </div>

            <div class="col-12">
                <p class="d-none d-md-block text-justify my-4" style="letter-spacing: .1rem; line-height: 1.75rem;">
                Technology Festival or TECHNOFEST was a collaboration of the Student Activities and Development Unit (SADU) and the Student Coordinating Council (SCC) in their desire to support FEU Tech’s vision to be a premier technological educational institution in the Philippines.
                In the past four years, TECHNOFEST has been a much-anticipated activity in the institution with the exhibit of pioneering Engineering and Information Technology projects produced by our students as part of Project-Based Learning. Also notable were the speakers who have graced the TECHNOFEST stage; from alumni who have made it big in the IT arena to the industry’s popular gamechangers
                And in the coming years, TECHNOFEST will continue to be the convergence for innovators and their breakthrough technologies.
                </p>
                <p class="d-md-none d-block text-justify my-4" style="line-height: 1.75rem;">
                Technology Festival or TECHNOFEST was a collaboration of the Student Activities and Development Unit (SADU) and the Student Coordinating Council (SCC) in their desire to support FEU Tech’s vision to be a premier technological educational institution in the Philippines.
                In the past four years, TECHNOFEST has been a much-anticipated activity in the institution with the exhibit of pioneering Engineering and Information Technology projects produced by our students as part of Project-Based Learning. Also notable were the speakers who have graced the TECHNOFEST stage; from alumni who have made it big in the IT arena to the industry’s popular gamechangers
                And in the coming years, TECHNOFEST will continue to be the convergence for innovators and their breakthrough technologies.
                </p>
            </div>
        </div>
    </div>
</div>

<div id="themeSection" class="section container-fluid text-black bg-light py-5 parallax">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 py-4">
                <h1 class="d-none d-md-block display-4 text-center bold" style="letter-spacing: 2px;">ABOUT THE THEME</h1>
                <h1 class="d-md-none d-block text-center bold" style="letter-spacing: 2px;">ABOUT THE THEME</h1>
                <div id="rectangle" style="background-color: #000; width: 80px; height:10px; margin:auto; padding-top:-5%;"></div>
            </div>

            <div class="col-12">
                <p class="d-none d-md-block text-justify my-4" style="letter-spacing: .1rem; line-height: 1.75rem;">
                There is commonly no accepted description of human values despite its position to human society and its strategic impact on engineering, science and advancement of human civilization. 
                It may best be defined as spirits and principles‘ regarding what is of strong  worth in our individual and collective actions, values inherited in society. 
                Ideologies and shared ethical and moral beliefs, which are the binding forces for communities.
                <br><br>
                While, the Information technology is now pervasive in the lives of people across the world. 
                These technologies take many forms such as smart phones, personal computers, mobile phone applications, the internet, web, digital assistants, and cloud computing.
                <br><br>
                The human developments in science and technology when synergistically combined should  strengthen human values, if the technology is constructive. On the other hand, technology which weakens the human values, which is disruptive. 
                The success of scientific and technological advancement and development depends on how deep the human values are  embedded in technologies.
                <br><br>
                Absolutely, the convention focuses on the concept on how Technology can weaken or strengthen its impact in human values in achieving sustainable innovations. 
                It targets to present different innovative projects which are useful to humanity while fortifying the real essence of human values.
                </p>
                <p class="d-md-none d-block text-justify my-4" style="line-height: 1.75rem;">
                There is commonly no accepted description of human values despite its position to human society and its strategic impact on engineering, science and advancement of human civilization. 
                It may best be defined as spirits and principles‘ regarding what is of strong  worth in our individual and collective actions, values inherited in society. 
                Ideologies and shared ethical and moral beliefs, which are the binding forces for communities.
                <br><br>
                While, the Information technology is now pervasive in the lives of people across the world. 
                These technologies take many forms such as smart phones, personal computers, mobile phone applications, the internet, web, digital assistants, and cloud computing.
                <br><br>
                The human developments in science and technology when synergistically combined should  strengthen human values, if the technology is constructive. On the other hand, technology which weakens the human values, which is disruptive. 
                The success of scientific and technological advancement and development depends on how deep the human values are  embedded in technologies.
                <br><br>
                Absolutely, the convention focuses on the concept on how Technology can weaken or strengthen its impact in human values in achieving sustainable innovations. 
                It targets to present different innovative projects which are useful to humanity while fortifying the real essence of human values.
                </p>
            </div>
        </div>
    </div>
</div>

<div id="gallerySection" class="section container-fluid parallax">
    <div class="row">
        <div class="col-12 col-md-4 order-1 order-md-6 py-5 p-md-5" style="background-color:#fdc310;">
            <h1 class="d-none d-md-block display-4 text-black text-center bold" style="letter-spacing: 2px; padding-top:0.7em;">GALLERY</h1>
            <h1 class="d-md-none d-block text-black text-center bold" style="letter-spacing: 2px;">GALLERY</h1>
            <div id="rectangle" style="width: 80px; height:10px; margin:auto; padding-top:-5%; background-color: #000;"></div>
        </div>

        <div class="col-6 col-md-4 order-2 p-0"><img src="<?= base_url('assets/res/technofest/gallery/A1.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
        <div class="col-6 col-md-4 order-3 p-0"><img src="<?= base_url('assets/res/technofest/gallery/A2.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
        <div class="col-6 col-md-4 order-4 p-0"><img src="<?= base_url('assets/res/technofest/gallery/A3.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
        <div class="col-6 col-md-4 order-5 p-0"><img src="<?= base_url('assets/res/technofest/gallery/A4.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
        <div class="col-6 col-md-4 order-7 p-0"><img src="<?= base_url('assets/res/technofest/gallery/A5.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
        <div class="col-6 col-md-4 order-8 p-0"><img src="<?= base_url('assets/res/technofest/gallery/A6.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
        <div class="col-6 col-md-4 order-9 p-0"><img src="<?= base_url('assets/res/technofest/gallery/A7.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
        <div class="col-6 col-md-4 order-10 p-0"><img src="<?= base_url('assets/res/technofest/gallery/A8.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
    </div>
</div>

<div id="speakerSection" class="section container-fluid py-5 text-white parallax">
    <div class="row justify-content-center align-items-stretch">
        <div class="col-12 py-4">
            <h1 class="d-none d-md-block display-4 text-center bold" style="letter-spacing: 2px;">SPEAKERS</h1>
            <h1 class="d-md-none d-block text-center bold" style="letter-spacing: 2px;">SPEAKERS</h1>
            <div id="rectangle" class="bg-white" style="width: 80px; height:10px; margin:auto; padding-top:-5%;"></div>
        </div>

        <div class="w-100 mb-4"></div>

        <div class="col-12 py-2">
            <h1 class="d-none d-md-block text-center text-uppercase bold">Keynote Speaker</h1>
            <h2 class="d-md-none d-block text-center text-uppercase bold">Keynote Speaker</h2>
            <div id="rectangle" class="bg-white mb-4" style="width: 80px; height:7px; margin:auto; padding-top:-5%;"></div>
        </div>

        <div class="col-9 col-md-2 px-3">
            <img class="img-fluid rounded-circle bg-warning p-2 mx-auto d-block w-75" src="<?= base_url('assets/res/technofest/speakers/ludwig-federigan.jpg') ?>" alt="Speaker">
            <h5 class="bg-warning rounded text-uppercase text-black text-center p-1 mt-2 bold">Ludwig O. Federigan</h4>
            
            <h6 class="text-white text-center shadow-text">
                Climate Reality Leader of The Climate Reality Project<br><span class="font-weight-bold">Philippines</span>
            </h6>
        </div>

        <div class="w-100 mb-4"></div>

        <div class="col-12 py-2">
            <h1 class="d-none d-md-block text-center text-uppercase bold">Plenary Speakers</h1>
            <h2 class="d-md-none d-block text-center text-uppercase bold">Plenary Speakers</h2>
            <div id="rectangle" class="bg-white mb-4" style="width: 80px; height:7px; margin:auto; padding-top:-5%;"></div>
        </div>

        <div class="col-9 col-md-2 px-3">
            <img class="img-fluid rounded-circle bg-warning p-2 mx-auto d-block w-75" src="<?= base_url('assets/res/technofest/speakers/arlene-romasanta.jpg') ?>" alt="Speaker">
            <h5 class="bg-warning rounded text-uppercase text-black text-center p-1 mt-2 bold">Arlene A. Romasanta</h4>
            
            <h6 class="text-white text-center shadow-text">
                OIC, Systems and Infrastructure Development Service of Department of Information and Communication Technology<br><span class="font-weight-bold">Philippines</span>
            </h6>
        </div>
        <div class="col-9 col-md-2 px-3">
            <img class="img-fluid rounded-circle bg-warning p-2 mx-auto d-block w-75" src="<?= base_url('assets/res/technofest/speakers/manuel-belino.jpg') ?>" alt="Speaker">
            <h5 class="bg-warning rounded text-uppercase text-black text-center p-1 mt-2 bold">Dr. Manuel C. Belino</h4>
            
            <h6 class="text-white text-center shadow-text">
                Master in Theological Studies of Harvard University<br><span class="font-weight-bold">United States of America</span>
            </h6>
        </div>
        <div class="col-9 col-md-2 px-3">
            <img class="img-fluid rounded-circle bg-warning p-2 mx-auto d-block w-75" src="<?= base_url('assets/res/technofest/speakers/rindu-alriavindra.png') ?>" alt="Speaker">
            <h5 class="bg-warning rounded text-uppercase text-black text-center p-1 mt-2 bold">Rindu Alriavindra Funny</h4>
            
            <h6 class="text-white text-center shadow-text">
                Lecturer of Institute Technology Adisutjipto<br><span class="font-weight-bold">Indonesia</span>
            </h6>
        </div>
        <div class="col-9 col-md-2 px-3">
            <img class="img-fluid rounded-circle bg-warning p-2 mx-auto d-block w-75" src="<?= base_url('assets/res/technofest/speakers/michael-choy.jpg') ?>" alt="Speaker">
            <h5 class="bg-warning rounded text-uppercase text-black text-center p-1 mt-2 bold">Dr. Michael Choy Seng Kim, Ed.D</h4>
            
            <h6 class="text-white text-center shadow-text">
                Founder of Dioworks Learning<br><span class="font-weight-bold">Singapore</span>
            </h6>
        </div>

        <div class="w-100 mb-2"></div>


        <div class="col-9 col-md-2 px-3">
            <img class="img-fluid rounded-circle bg-warning p-2 mx-auto d-block w-75" src="<?= base_url('assets/res/technofest/speakers/yacine-petitprez.jpg') ?>" alt="Speaker">
            <h5 class="bg-warning rounded text-uppercase text-black text-center p-1 mt-2 bold">Yacine Petitprez</h4>
            
            <h6 class="text-white text-center shadow-text">
                IT Manager of Ingedata International
            </h6>
        </div>
        <div class="col-9 col-md-2 px-3">
            <img class="img-fluid rounded-circle bg-warning p-2 mx-auto d-block w-75" src="<?= base_url('assets/res/technofest/speakers/alberto-amorsolo.png') ?>" alt="Speaker">
            <h5 class="bg-warning rounded text-uppercase text-black text-center p-1 mt-2 bold">Dr. Alberto Amorsolo, Jr.</h4>
            
            <h6 class="text-white text-center shadow-text">
                Ph.D. in Materials Science of University of Rochester<br><span class="font-weight-bold">United States of America</span>

            </h6>
        </div>
        <div class="col-9 col-md-2 px-3">
            <img class="img-fluid rounded-circle bg-warning p-2 mx-auto d-block w-75" src="<?= base_url('assets/res/technofest/speakers/analiza-teh.jpg') ?>" alt="Speaker">
            <h5 class="bg-warning rounded text-uppercase text-black text-center p-1 mt-2 bold">Atty. Analiza Rebuelta-Teh</h4>
            
            <h6 class="text-white text-center shadow-text">
                Interim Undersecretary for Mining of Department of Environment and Natural Resources (DENR)<br><span class="font-weight-bold">Philippines</span>
            </h6>
        </div>
        <div class="col-9 col-md-2 px-3">
            <img class="img-fluid rounded-circle bg-warning p-2 mx-auto d-block w-75" src="<?= base_url('assets/res/technofest/speakers/virgilio-leyba.jpg') ?>" alt="Speaker">
            <h5 class="bg-warning rounded text-uppercase text-black text-center p-1 mt-2 bold">Virgilio S. Leyba</h4>
            
            <h6 class="text-white text-center shadow-text">
                Information and Technology Services Department Manager of National Power Corporation<br><span class="font-weight-bold">Philippines</span>
            </h6>
        </div>

        <div class="w-100 mb-4"></div>

        <div class="col-10 col-md-4">
            <a href="<?= base_url('technofest/speaker') ?>" class="btn btn-warning btn-lg btn-block font-weight-bold text-uppercase">View Speakers' Profile</a>
        </div>
    </div>
</div>

<!-- <div id="speakerAlternateSection" class="section container-fluid px-0">
    <div class="row align-items-center mx-0 w-100">
        <div class="col-12 col-md-8 p-5">
            <h1 class="d-none d-md-block display-4 text-center text-black bold" style="letter-spacing: 2px;">SPEAKERS</h1>
            <h1 class="d-md-none d-block text-center text-black bold" style="letter-spacing: 2px;">SPEAKERS</h1>
            <div id="rectangle" class="bg-black" style="width: 80px; height:10px; margin:auto; padding-top:-5%;"></div>
            <a href="<?= base_url('technofest/speaker') ?>" class="btn btn-block btn-outline-warning text-black font-weight-bold mt-4 mx-auto w-50" style="border-color: #000; border-radius: 5rem; border-width: 0.2rem;">LEARN MORE</a>
        </div>
        <div class="col-6 col-md-2 p-0">
            <img class="img-fluid bg-warning d-block grayscale w-100" src="<?= base_url('assets/res/technofest/speakers/manuel-belino.jpg') ?>" alt="Speaker">
        </div>
        <div class="col-6 col-md-2 p-0">
            <img class="img-fluid bg-warning d-block grayscale w-100" src="<?= base_url('assets/res/technofest/speakers/michael-choy.jpg') ?>" alt="Speaker">
        </div>
        <div class="col-6 col-md-2 p-0">
            <img class="img-fluid bg-warning d-block grayscale w-100" src="<?= base_url('assets/res/technofest/speakers/rindu-alriavindra.png') ?>" alt="Speaker">
        </div>
        <div class="col-6 col-md-2 p-0">
            <img class="img-fluid bg-warning d-block grayscale w-100" src="<?= base_url('assets/res/technofest/speakers/arlene-romasanta.jpg') ?>" alt="Speaker">
        </div>
        <div class="col-6 col-md-2 p-0">
            <img class="img-fluid bg-warning d-block grayscale w-100" src="<?= base_url('assets/res/technofest/speakers/yacine-petitprez.jpg') ?>" alt="Speaker">
        </div>
        <div class="col-6 col-md-2 p-0">
            <img class="img-fluid bg-warning d-block grayscale w-100" src="<?= base_url('assets/res/technofest/speakers/niel-lopez.jpg') ?>" alt="Speaker">
        </div>
        <div class="col-6 col-md-2 p-0">
            <img class="img-fluid bg-warning d-block grayscale w-100" src="<?= base_url('assets/res/technofest/speakers/alberto-amorsolo.png') ?>" alt="Speaker">
        </div>
        <div class="col-6 col-md-2 p-0">
            <img class="img-fluid bg-warning d-block grayscale w-100" src="<?= base_url('assets/res/technofest/speakers/rosemary-seva.jpg') ?>" alt="Speaker">
        </div>
    </div>
</div> -->

<div id="paperSection" class="section container-fluid text-black py-5 p-md-5 parallax">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-6">
                <h4 class="d-none d-md-block mb-0" style="letter-spacing: 0.1em;">TECHNOFEST 2018 | RESEARCH PRESENTATION</h4>
                <p class="d-md-none d-block text-center mb-0" style="font-size: 0.75rem; letter-spacing: 0.1em;">TECHNOFEST 2018 | RESEARCH PRESENTATION</p>
                <h1 class="d-none d-md-block display-3 bold mb-0">CALL FOR PAPERS</h1>
                <h2 class="d-md-none d-block text-center bold mb-0">CALL FOR PAPERS</h2>
                <div id="rectangle" class="d-none d-md-block" style="width: 160px; height:17px; padding-top:-5%; background-color:#000"></div>
                <div id="rectangle" class="d-md-none d-block mx-auto" style="width: 100px; height:7px; padding-top:-5%; background-color:#000"></div>
                <a href="<?= base_url('technofest/scholarship') ?>" class="d-none d-md-inline-block btn btn-warning btn-lg font-weight-bold mt-4" style="border-width: 0.2rem; border-color; #000;">APPLY FOR SCHOLARSHIP</a>
                <div class="w-100"></div>
                <a href="<?= base_url('technofest/paper') ?>" class="d-none d-md-inline-block btn btn-outline-dark btn-lg mt-4" style="border-width: 0.2rem; border-color; #000;">LEARN MORE</a>
            </div>

            <div class="col-12 col-md-6 py-3">
                <h2 class="d-none d-md-block bold">OBJECTIVES</h1>
                <h5 class="d-md-none d-block text-center bold">OBJECTIVES</h4>
                <ol class="d-none d-md-block text-justify lead" style="letter-spacing: 0.1rem;">
                    <li>Celebrate the latest trends in technology
                    <li>Showcase the exceptional case studies and theses of students and professionals</li>
                    <li>Encourage academic competence among students and interest for relevant fields of study</li>
                    <li>Create the culture of research among college students and professional in the fields of Information and Technology</li>
                    <li>Highlight different related studies/researches that have emphasis on Technology and Human Values</li>
                </ol>
                <ol class="d-md-none d-block">
                    <li>Celebrate the latest trends in technology
                    <li>Showcase the exceptional case studies and theses of students and professionals</li>
                    <li>Encourage academic competence among students and interest for relevant fields of study</li>
                    <li>Create the culture of research among college students and professional in the fields of Information and Technology</li>
                    <li>Highlight different related studies/researches that have emphasis on Technology and Human Values</li>
                </ol>
            </div>

            <div class="col-10 col-md-12 text-center">
                <a href="<?= base_url('technofest/scholarship') ?>" class="d-md-none d-block btn btn-warning btn-block btn-lg mx-auto" style="border-width: 0.2rem;">APPLY FOR SCHOLARSHIP</a>
                <a href="<?= base_url('technofest/paper') ?>" class="d-md-none d-block btn btn-dark btn-block btn-lg mx-auto" style="border-width: 0.2rem;">LEARN MORE</a>
            </div>
        </div>
    </div>
</div>

<div id="competitionSection" class="section container-fluid bg-success py-5 p-md-5 parallax">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <h1 class="d-none d-md-block text-white text-center bold display-3 mb-0">ACADEMIC COMPETITIONS</h1>
                <h2 class="d-md-none d-block text-white text-center bold mb-0">ACADEMIC COMPETITIONS</h2>
                <div id="rectangle" class="d-none d-md-block mx-auto mb-5" style="width: 180px; height:17px; padding-top:-5%; background-color:#fff"></div>
                <div id="rectangle" class="d-md-none d-block mx-auto mb-5" style="width: 120px; height:10px; padding-top:-5%; background-color:#fff"></div>
            </div>

            <div class="col-12 col-md-6 order-2 order-md-1">
                <?php foreach($competitions as $competition): ?>
                <ul class="text-white h1 list-unstyled">
                    <li>
                        <h2 class="d-none d-md-block font-weight-bold text-warning semiit"><?= $competition['name'] ?></h2>
                        <h4 class="d-md-none d-block font-weight-bold text-warning semiit"><?= $competition['name'] ?></h4>
                        <h5 class="d-none d-md-block text-uppercase text-white semiit"><?= $competition['program']['name'] ?></h5>
                        <h6 class="d-md-none d-block text-uppercase text-white semiit"><?= $competition['program']['name'] ?></h6>
                    </li>
                </ul>
                <?php endforeach; ?>

                <a href="<?= base_url('technofest/competition') ?>" class="d-none d-md-inline-block btn btn-outline-light btn-lg mt-4" style="border-width: 0.2rem;">LEARN MORE</a>
                <a href="<?= base_url('technofest/competition') ?>" class="d-md-none btn btn-outline-light btn-block mt-4" style="border-width: 0.2rem;">LEARN MORE</a>
            </div>

            <div class="col-12 col-md-6 order-1 order-md-2">
                <div id="competitionCarousel" class="carousel slide mb-5 mb-md-0" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="<?= base_url('assets/res/technofest/competitions/posters/1.png') ?>" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?= base_url('assets/res/technofest/competitions/posters/3.jpg') ?>" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?= base_url('assets/res/technofest/competitions/posters/4.jpg') ?>" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?= base_url('assets/res/technofest/competitions/posters/5.jpg') ?>" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?= base_url('assets/res/technofest/competitions/posters/6.jpg') ?>" alt="First slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#competitionCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#competitionCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="scheduleSection" class="section container-fluid py-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 py-4">
                <h1 class="d-none d-md-block display-4 text-center bold" style="letter-spacing: 2px; color:#036c44;">CONFERENCE ACTIVITIES</h1>
                <h1 class="d-md-none d-block text-center bold" style="letter-spacing: 2px; color:#036c44;">CONFERENCE ACTIVITIES</h1>
                <div id="rectangle" style="width: 80px; height:10px; margin:auto; padding-top:-5%; background-color:#036c44"></div>
            </div>

            <div class="col-12 text-center py-4">
                <div class="d-none d-md-inline btn-group btn-group-lg">
                    <button data-target="#day1Schedule" class="schedule-tab btn btn-warning text-white bold">DAY 1</button>
                    <button data-target="#day2Schedule" class="schedule-tab btn btn-dark text-white bold">DAY 2</button>
                    <button data-target="#day3Schedule" class="schedule-tab btn btn-dark text-white bold">DAY 3</button>
                </div>

                <div class="d-md-none d-inline btn-group">
                    <button data-target="#day1Schedule" class="schedule-tab btn btn-warning text-white bold">DAY 1</button>
                    <button data-target="#day2Schedule" class="schedule-tab btn btn-dark text-white bold">DAY 2</button>
                    <button data-target="#day3Schedule" class="schedule-tab btn btn-dark text-white bold">DAY 3</button>
                </div>
            </div>

            <div id="day1Schedule" class="schedule-panel col-12 col-md-8 py-4">
                <h4 class="text-center text-uppercase bold" style="letter-spacing: 2px; color:#036c44;">The Beginning of Technological Resilience: Techno Ores</h4>
                <h5 class="text-center text-uppercase bold" style="letter-spacing: 2px; color:#036c44;">Day 1 | April 25, 2018 (Wednesday)</h5>
                <table class="table table-sm mx-auto">
                    <tbody>
                        <tr>
                            <th scope="row">9:00 AM</th>
                            <td>
                                <span class="d-block">Registration</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">2:30 PM</th>
                            <td>
                                <span class="d-block">Parade</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">3:00 PM</th>
                            <td>
                                <span class="d-block">Ecumenical Prayer</span>
                                <span class="d-block">Philippine National Anthem</span>
                                <span class="d-block">Artist Connection - Music Synergy</span>

                                <span class="d-block mt-2">TECHNOFEST History</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">4:15 PM</th>
                            <td>
                                <span class="d-block font-weight-bold">Welcome Address</span>
                                <span class="d-block">Dr. Michael Alba</span>
                                <span class="d-block">President, Far Eastern University</span>

                                <span class="d-block font-weight-bold mt-2">Keynote Speech</span>

                                <span class="d-block font-weight-bold mt-2">Opening Message</span>
                                <span class="d-block">Dr. May Rose C. Imperial</span>
                                <span class="d-block">Senior Director, Academic Sevices</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">4:30 PM - 5:30 PM</th>
                            <td>
                                <span class="d-block font-weight-bold">Pitching Session</span>
                                <span class="d-block">Industry Partners</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">6:00 PM</th>
                            <td>
                                <span class="d-block">Welcome Dinner and Socialization</span>
                                <span class="d-block">Artist Connection</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="day2Schedule" class="schedule-panel col-12 col-md-8 py-4" style="display: none;">
                <h4 class="text-center text-uppercase bold" style="letter-spacing: 2px; color:#036c44;">Forging Technology: Preparing the Furnace</h4>
                <h5 class="text-center text-uppercase bold" style="letter-spacing: 2px; color:#036c44;">Day 2 | April 26, 2018 (Thursday)</h5>
                <table class="table table-sm mx-auto">
                    <tbody>
                        <tr>
                            <th scope="row">9:00 AM</th>
                            <td>
                                <span class="d-block font-weight-bold">Cultural Presentation</span>
                                <span class="d-block">Artist Connection</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">9:10 AM - 11:40 AM</th>
                            <td>
                                <span class="d-block font-weight-bold">Mr. Yacine Petitprez</span>
                                <span class="d-block">IT Manager, IngeData</span>

                                <span class="d-block font-weight-bold mt-2">Dr. Manuel C. Belino</span>
                                <span class="d-block">Senior Director for Engineering, FEU Institute of Technology</span>

                                <span class="d-block font-weight-bold mt-2">Dir. Arlene A. Romasanta</span>
                                <span class="d-block">OIC, Systems and Infrastructure Development Service (SID), <br/>Department of Information and Communication Technology</span>
                            
                                <span class="d-block font-weight-bold mt-2">Ms. Rindu Alriavindra Funny</span>
                                <span class="d-block">Lecturer, Institute of Technology Adisutjipto</span>

                                <span class="d-block font-weight-bold mt-2">Neil Stephen A. Lopez</span>
                                <span class="d-block">Assistant Professor, Vice Chair and External Affairs Coordinator,<br>Mechanical Engineering, De La Salle University</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">11:40 AM</th>
                            <td>
                                <span class="d-block">Interactive Forum</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">12:30 PM</th>
                            <td>
                                <span class="d-block">Buffet Lunch</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">1:30 PM - 5:30 PM</th>
                            <td>
                                <span class="d-block">Call for Papers</span>
                                <span class="d-block">Best Project Presentation</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">6:00 PM</th>
                            <td>
                                <span class="d-block">Buffet Dinner</span>
                                <span class="d-block">Cultural Presentation</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="day3Schedule" class="schedule-panel col-12 col-md-8 py-4" style="display: none;">
                <h4 class="text-center text-uppercase bold" style="letter-spacing: 2px; color:#036c44;">Connecting Cables: Pounding the Ores of Knowledge and Wisdom  </h4>
                <h5 class="text-center text-uppercase bold" style="letter-spacing: 2px; color:#036c44;">Day 3 | April 27, 2018 (Friday)</h5>
                <table class="table table-sm mx-auto">
                    <tbody>
                        <tr>
                            <th scope="row">7:00 AM</th>
                            <td>
                                <span class="d-block font-weight-bold">Academic Competitions</span>
                                <span class="d-block">Civil Engineering</span>
                                <span class="d-block">Computer Science</span>
                                <span class="d-block">Electronics Engineering</span>
                                <span class="d-block">Electrical Engineering</span>
                                <span class="d-block">Mechanical Engineering</span>
                                <span class="d-block">Information Technology</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">9:10 AM</th>
                            <td>
                                <span class="d-block font-weight-bold">Alberto V. Amorsolo, Jr., Ph.D</span>
                                <span class="d-block">Professor, UP College of Engineering</span>

                                <span class="d-block font-weight-bold mt-2">Dr. Michael Choy Seng Kim, Ed.D</span>
                                <span class="d-block">Founder of Dioworks Learning, Singapore</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">11:40 AM</th>
                            <td>
                                <span class="d-block font-weight-bold">Interactive Forum</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">12:30 PM</th>
                            <td>
                                <span class="d-block">Buffet Lunch</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">1:30 PM - 4:30 PM</th>
                            <td>
                                <span class="d-block font-weight-bold">Dr. Rosemary R. Seva</span>
                                <span class="d-block">Dean, College of Engineering, De La Salle University</span>

                                <span class="d-block font-weight-bold mt-2">Mr. Virgilio Leyba</span>
                                <span class="d-block">National Power Corporation, ISTD Department</span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">4:30 PM</th>
                            <td>
                                <span class="d-block">Awarding</span>
                                <span class="d-block">Closing Ceremony</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="registerSection" class="section container-fluid py-5 parallax">
    <div class="container">
        <div class="row">
            <div class="col-12 py-4">
                <h1 class="d-none d-md-block display-4 text-center bold text-dark" style="letter-spacing: 2px;">REGISTRATION</h1>
                <h1 class="d-md-none d-block text-center bold text-dark" style="letter-spacing: 2px;">REGISTRATION</h1>
                <div id="rectangle" class="bg-dark" style="width: 80px; height:10px; margin:auto; padding-top:-5%;"></div>
            </div>

            <div class="col-12">
                <div class="row align-items-stretch justify-content-center justify-content-md-around">
                    <div class="col-12 col-md-4 py-4 py-md-1">
                        <div class="d-flex flex-column h-100">
                            <div class="rounded-top p-3 text-center h-100" style="background-color:#036c44;">
                                <h2 class="d-none d-md-block text-warning font-italic bold">LOCAL DELEGATES <small>WITHOUT ACCOMMODATION</small></h1>
                                <h4 class="d-md-none d-block text-warning font-italic bold">LOCAL DELEGATES <small>WITHOUT ACCOMMODATION</small></h4>

                                <h1 class="d-none d-md-block display-3 bold text-white mb-0"><small><sup>$</sup></small> 90</h1>
                                <h1 class="d-md-none d-block display-4 bold text-white mb-0"><small><sup>$</sup></small> 90</h1>
                                <h4 class="d-none d-md-block text-white font-weight-bold font-italic">PER HEAD</h4>
                                <h6 class="d-md-none d-block text-white font-weight-bold font-italic">PER HEAD</h6>
                            </div>
                            <div class="rounded-bottom bg-light p-3"><h4 class="font-weight-bold">Delegates</h4>
                                <ul class="mb-2">
                                    <li>Students</li>
                                    <li>Professionals</li>
                                    <li>Paper Presenters</li>
                                </ul>
                                <hr>
                                <h4 class="font-weight-bold">Inclusion</h4>
                                <ul>
                                    <li>Food for 3 days</li>    
                                    <li>No Accommodation</li>
                                    <li>Convention Kits</li>
                                    <li>Certificate of Attendance</li>
                                </ul>
                                <?= form_open('technofest/register') ?>
                                <?= form_hidden('locale', 'local') ?>
                                <?= form_hidden('accommodation', 'live_out') ?>
                                <button type="submit" class="d-none d-md-block btn bold btn-lg text-white btn-dark w-75 mx-auto">REGISTER</button>
                                <button type="submit" class="d-md-none d-block btn bold text-white btn-dark w-75 mx-auto">REGISTER</button>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-4 py-4 py-md-1">
                        <div class="d-flex flex-column h-100">
                            <div class="rounded-top p-3 text-center h-100" style="background-color:#036c44;">
                                <h2 class="d-none d-md-block text-warning font-italic bold">LOCAL DELEGATES <small>WITH ACCOMMODATION</small></h1>
                                <h4 class="d-md-none d-block text-warning font-italic bold">LOCAL DELEGATES <small>WITH ACCOMMODATION</small></h4>

                                <h1 class="d-none d-md-block display-3 bold text-white mb-0"><small><sup>$</sup></small> 125</h1>
                                <h1 class="d-md-none d-block display-4 bold text-white mb-0"><small><sup>$</sup></small> 125</h1>
                                <h4 class="d-none d-md-block text-white font-weight-bold font-italic">PER HEAD</h4>
                                <h6 class="d-md-none d-block text-white font-weight-bold font-italic">PER HEAD</h6>
                            </div>
                            <div class="rounded-bottom bg-light p-3">
                                <h4 class="font-weight-bold">Delegates</h4>
                                <ul class="mb-2">
                                    <li>Students</li>
                                    <li>Professionals</li>
                                    <li>Paper Presenters</li>
                                </ul>
                                <hr>
                                <h4 class="font-weight-bold">Inclusion</h4>
                                <ul>
                                    <li>Food for 3 days</li>
                                    <li>Accommodation for 3 days and 2 nights</li>
                                    <li>Convention Kits</li>
                                    <li>Certificate of Attendance</li>
                                </ul>
                                <?= form_open('technofest/register') ?>
                                <?= form_hidden('locale', 'local') ?>
                                <?= form_hidden('accommodation', 'live_in') ?>
                                <button type="submit" class="d-none d-md-block btn bold btn-lg text-white btn-dark w-75 mx-auto">REGISTER</button>
                                <button type="submit" class="d-md-none d-block btn bold text-white btn-dark w-75 mx-auto">REGISTER</button>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>
                    

                    <div class="col-12 col-md-4 py-4 py-md-1">
                        <div class="d-flex flex-column h-100">
                            <div class="rounded-top p-3 text-center h-100" style="background-color:#036c44;">
                                <h2 class="d-none d-md-block text-warning font-italic bold">INTERNATIONAL DELEGATES</h1>
                                <h4 class="d-md-none d-block text-warning font-italic bold">INTERNATIONAL DELEGATES</h4>

                                <h1 class="d-none d-md-block display-3 bold text-white mb-0"><small><sup>$</sup></small> 150</h1>
                                <h1 class="d-md-none d-block display-4 bold text-white mb-0"><small><sup>$</sup></small> 150</h1>
                                <h4 class="d-none d-md-block text-white font-weight-bold font-italic">PER HEAD</h4>
                                <h6 class="d-md-none d-block text-white font-weight-bold font-italic">PER HEAD</h6>
                            </div>
                            <div class="rounded-bottom bg-light p-3">
                                <h4 class="font-weight-bold">Delegates</h4>
                                <ul class="mb-2">
                                    <li>Students</li>
                                    <li>Professionals</li>
                                    <li>Paper Presenters</li>
                                </ul>
                                <hr>
                                <h4 class="font-weight-bold">Inclusion</h4>
                                <ul>
                                    <li>Food for 3 days</li>
                                    <li>Accommodation for 3 days and 2 nights</li>
                                    <li>Convention Kits</li>
                                    <li>Certificate of Attendance</li>
                                </ul>
                                <?= form_open('technofest/register') ?>
                                <?= form_hidden('locale', 'international') ?>
                                <?= form_hidden('accommodation', 'live_in') ?>
                                <button type="submit" class="d-none d-md-block btn bold btn-lg text-white btn-dark w-75 mx-auto">REGISTER</button>
                                <button type="submit" class="d-md-none d-block btn bold text-white btn-dark w-75 mx-auto">REGISTER</button>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div id="sponsorSection" class="section container-fluid py-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <h1 class="d-none d-md-block display-4 text-center bold" style="letter-spacing: 2px; color:#036c44;">CONFERENCE PARTNERS</h1>
                <h1 class="d-md-none d-block text-center bold" style="letter-spacing: 2px; color:#036c44;">CONFERENCE PARTNERS</h1>
                <div id="rectangle" style="width: 80px; height:10px; margin:auto; padding-top:-5%; background-color:#036c44;"></div>
            </div>

            <div class="w-100"></div>

            <?php for($i = 0; $i < 6; $i++): ?>
            <div class="col-4 col-md-2">
                <img class="img-fluid d-block m-auto p-4" src="<?= base_url('assets/res/technofest/sponsors/'.($i+1).'.png') ?>" alt="Sponsor">
            </div>
            <?php endfor; ?>
        </div>
    </div>
</div> -->

<div id="locationSection" class="section container-fluid section p-0 m-0">
    <div class="d-none d-md-block position-absolute w-100 p-5">
        <h1 class="d-none d-md-block display-4 text-center bold text-dark" style="letter-spacing: 2px; color">CONFERENCE VENUE</h1>
        <!-- <h1 class="d-md-none d-block text-center bold text-dark" style="letter-spacing: 2px; color">CONFERENCE VENUE</h1> -->
        <div id="rectangle" class="d-none d-md-block bg-dark" style="width: 80px; height:10px; margin:auto; padding-top:-5%;"></div>
    </div>
    <iframe frameborder="0" style="border:0; width: 100%; height: 30em;" 
    src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJWbJOsfjJlzMRjgaarMoSTa0&key=AIzaSyA0n-yj_uxS0tEuiwZLrDUjhfvctwwFDxw" 
    allowfullscreen></iframe>
</div>
<div id="mainSectionVideo" class="section container-fluid bg-white p-0">
    <video class="w-100 p-0 m-0" playsinline autoplay muted loop>
        <source src="<?= base_url('assets/res/ambassadors/bg/bg-vid.mp4') ?>" type="video/mp4">
    </video>
</div>

<div id="videoSection" class="section container-fluid py-5 bg-white">
    <div class="container text-center">
        <h5 class="text-dark">TEASER</h5>
        <h1 class="d-none d-md-block IA_head display-2 font-bold">Video</h1>
        <h1 class="d-md-none d-block IA_head display-4 font-bold">Video</h1>
        <div class="video-container">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/kIiRxEr4G3Q?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </div>
    </div>
</div>

<div id="eventCountdownSection" class="section container-fluid">
    <div class="container">
        <div class="row align-items-center py-5">
            <div class="col-sm-7 text-center">
                <h5 class="text-white">Countdown to</h5>
                <h1 class="IA_head display-4 font-bold">iTam Ambassador 2017</h1>
            </div>
            <div class="col-sm-5">
                <h1 class="countdown d-none d-md-block display-4 text-secondary"></h1>
                <h1 class="countdown d-md-none d-block text-center text-secondary"></h1>
            </div>
        </div>
    </div>
</div>

<div id="aboutSection" class="section container-fluid bg-white">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center pt-5">
                <div class="col"><h5 class="text-dark">ABOUT</h5></div>
                <div class="col"><h1 class="d-none d-md-block IA_head display-2 font-bold">iTam Ambassador</h1></div>
                <div class="col"><h1 class="d-md-none d-block IA_head display-4 font-bold">iTam Ambassador</h1></div>
            </div>
            <div class="col-12 text-center">
                <p class="desc text-justify text-dark pb-5"><?= LOREM_IPSUM. LOREM_IPSUM ?></p>
            </div>
        </div>
    </div>
</div>

<div id="historySection" class="section container-fluid d-none">
        <div class="row">

            <div class="col-12 col-md-6 order-1 order-md-8 p-md-7 py-5" style="background-color: #000000;">
                <div class="col pt-4"><h5 class="d-none d-md-block text-white text-center">HISTORY</h5></div>
                <div class="col"><h1 class="IA_head d-none d-md-block display-3 font-bold text-center">iTam Ambassadors</h1></div>
                <div class="col"><h5 class="d-none d-md-block text-white text-center">through the years</h5></div>

                <div class="col"><h5 class="d-md-none d-block text-white text-center">HISTORY</h5></div>
                <div class="col"><h1 class="IA_head d-md-none d-block display-4 font-bold text-center">iTam Ambassadors</h1></div>
                <div class="col"><h5 class="d-md-none d-mblock text-white text-center">through the years</h5></div>
            </div>

            <div class="col-6 col-md-3 order-2 p-0"><img src="<?= base_url('assets/res/ambassadors/candidates/test-2.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div> 
            <div class="col-6 col-md-3 order-3 p-0"><img src="<?= base_url('assets/res/ambassadors/candidates/test-2.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
            <div class="col-6 col-md-3 order-4 p-0"><img src="<?= base_url('assets/res/ambassadors/candidates/test-2.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
            <div class="col-6 col-md-3 order-5 p-0"><img src="<?= base_url('assets/res/ambassadors/candidates/test-2.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div> 
            <div class="col-6 col-md-3 order-6 p-0"><img src="<?= base_url('assets/res/ambassadors/candidates/test-2.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div> 
            <div class="col-6 col-md-3 order-7 p-0"><img src="<?= base_url('assets/res/ambassadors/candidates/test-2.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
            <div class="col-6 col-md-3 order-9 p-0"><img src="<?= base_url('assets/res/ambassadors/candidates/test-2.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div> 
            <div class="col-6 col-md-3 order-10 p-0"><img src="<?= base_url('assets/res/ambassadors/candidates/test-2.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
            <div class="col-6 col-md-3 order-11 p-0"><img src="<?= base_url('assets/res/ambassadors/candidates/test-2.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>
            <div class="col-6 col-md-3 order-12 p-0"><img src="<?= base_url('assets/res/ambassadors/candidates/test-2.jpg') ?>" class="gallery img-fluid" alt="Gallery"></div>    
        </div>   
</div>

<div id="profileSection" class="section container-fluid bg-black">
    <div class="container">
        <div class="row align-items-center py-5">
            <div class="col-12 col-md-7 text-center">
                <h5 class="text-white">PROFILE</h5>
                <h1 class="d-none d-md-block IA_head display-2 font-bold">Candidates</h1>
                <h1 class="d-md-none d-block IA_head display-4 font-bold">Candidates</h1>
            </div>
            <div class="col-12 col-md-5">
                <center><form action="<?= base_url('ambassadors/vote') ?>">
                <button class="vote-btn btn btn-lg btn-warning"><strong>Vote</strong></button>
                </form></center>
            </div>
        </div>
    </div>
</div>

<div id="candidatesSection" class="section container-fluid bg-white">
    <div class="row align-items-center">
        <?php $ctr = 0; foreach($candidates as $candidate): ?>

        <?php if($ctr / 2 % 2 == 0): ?>
        <div class="d-none d-md-block col-md-3 p-0">
            <img src="<?= base_url('assets/res/ambassadors/candidates/sqr/'.strtoupper($candidate['program_code']).'_'.$candidate['gender'].'.jpg') ?>" class="gallery img-fluid" alt="Gallery">
        </div>
        <?php endif; ?>
        
        <div class="d-md-none d-block col-12 p-0">
            <img src="<?= base_url('assets/res/ambassadors/candidates/sqr/'.strtoupper($candidate['program_code']).'_'.$candidate['gender'].'.jpg') ?>" class="gallery img-fluid" alt="Gallery">
        </div>
        <div class="col-12 col-md-3 py-3 p-md-0 text-center">
            <h4><?= $candidate['first_name'].' '.$candidate['last_name'] ?></h4>
            <p class="desc text-dark text-center"><?= $candidate['program']['name'] ?> <?= $candidate['gender'] == 'M' ? 'Ambassador' : 'Ambassadress' ?></p>
        </div>

        <?php if($ctr / 2 % 2 == 1): ?>
        <div class="d-none d-md-block col-md-3 p-0">
            <img src="<?= base_url('assets/res/ambassadors/candidates/sqr/'.strtoupper($candidate['program_code']).'_'.$candidate['gender'].'.jpg') ?>" class="gallery img-fluid" alt="Gallery">
        </div>
        <?php endif; ++$ctr;?>

        <?php endforeach; ?>
    </div>   
</div>

<div id="sponsorSection" class="section container-fluid bg-black">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center pt-5">
                <div class="col"><h5 class="text-white">SPONSORS</h5></div>
                <div class="col"><h1 class="d-none d-md-block IA_head display-2 font-bold">Event Partners</h1></div>
                <div class="col"><h1 class="d-md-none d-block IA_head display-4 font-bold">Event Partners</h1></div>
            </div>

            <?php for($i = 1; $i <= 1; $i++): ?>
            <div class="col-6 col-md-2">
                <img class="img-fluid" src="<?= base_url('assets/res/ambassadors/sponsors/'.$i.'.png') ?>" alt="Sponsor">
            </div>
            <?php endfor; ?>
        </div>
    </div>
</div>
<div id="voteModal" class="modal fade show" role="dialog">
    <div class="modal-dialog">

    <div class="modal-content border border-warning rounded bg-black text-white">
        <div class="modal-body">
            <div class="row justify-content-center">
                <!-- <img class="img-fluid d-block m-auto w-50" src="<?= base_url('assets/res/ambassadors/logo/logo.png'); ?>"/> -->
                <h1>Login to vote</h1>
                <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false" onlogin="window.location.reload()"></div>
            </div>
        </div>
    </div>

    </div>
</div>

<div id="candidateModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <div class="modal-content border border-warning rounded-0 bg-black text-white">
        <div class="modal-body">
            <div class="row align-items-center text-center">
                <div class="col-12">
                    <img id="candidateModalImage" class="img-fluid rounded-0" alt="Candidate">
                </div>

                <div class="col-12 py-md-4">
                    <h1 id="candidateModalName" class="text-warning title mt-3"></h1>
                    <h6 id="candidateModalCourse" class="font-weight-bold"></h6>
                    <!-- <p id="candidateModalDescription" class="text-justify desc m-3"></p> -->

                    <div class="btn-group-vertical w-50 mt-3">
                        <a href="#" id="candidateModalVote" class="btn btn-warning" data-dismiss="modal">Vote</a>
                        <a href="#" class="btn btn-dark btn-sm" data-dismiss="modal">Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
</div>

<div id="mainSectionVideo" class="section container-fluid bg-black p-0">
    <video class="w-100 p-0 m-0" playsinline autoplay muted loop>
        <source src="<?= base_url('assets/res/ambassadors/bg/bg-vid.mp4') ?>" type="video/mp4">
    </video>
</div>

<!-- <div id="mainSection" class="section container-fluid py-5 py-md-4 parallax">
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <img class="img-fluid d-block m-auto w-75" src="<?= base_url('assets/res/ambassadors/logo/logo-edit.png'); ?>"/>
        </div>
    </div>
</div> -->

<div id="eventCountdownSection" class="section container-fluid">
    <div class="container">
        <div class="row align-items-center py-5">
            <div class="col-sm-7 text-center">
                <h5 class="text-white">Countdown to</h5>
                <h1 class="IA_head display-4 font-bold">Coronation Night</h1>
            </div>
            <div class="col-sm-5">
                <h1 class="countdown d-none d-md-block display-4 text-secondary"></h1>
                <h1 class="countdown d-md-none d-block text-center text-secondary"></h1>
            </div>
        </div>
    </div>
</div>

<div id="candidateSection" class="section container-fluid bg-black">
    <div class="row justify-content-center">
        <div class="col-12 py-2">
            <h1 class="d-none d-md-block text-center text-light mt-4" style="margin-bottom: -40px;">Your</h1>
            <h1 class="d-none d-md-block title text-warning display-2 text-center">Candidates</h1>
            <h2 class="d-md-none d-block text-center font-weight-bold text-light mt-4" style="margin-bottom: -10px;">Your</h2>
            <h1 class="d-md-none d-block title text-warning display-4 font-weight-bold text-center">Candidates</h1>
        </div>
        <div class="col-12 px-2 px-md-0 py-md-2">
            <div class="card-group">
                <?php foreach($programs as $program): ?>
                <div class="candidate-card card border-0 rounded-0 bg-transparent" data-program="<?= $program['code'] ?>" data-gender="M">
                    <img class="candidate-img card-img rounded-0 img-fluid" src="<?= base_url('assets/res/ambassadors/candidates/vote/'.strtoupper($program['code']).'_M.jpg') ?>" alt="Candidate" data-toggle="modal" data-target="#candidateModal" data-program="<?= $program['code'] ?>" data-gender="M"/>
                    <div class="card-body text-center p-2">
                        <button type="button" class="vote-btn btn btn-warning btn-block" data-program="<?= $program['code'] ?>" data-gender="M">Vote</button>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-12 px-2 px-md-0 py-md-2">
            <div class="card-group">
                <?php foreach($programs as $program): ?>
                <div class="candidate-card card border-0 rounded-0 bg-transparent" data-program="<?= $program['code'] ?>" data-gender="F">
                    <img class="candidate-img card-img rounded-0 img-fluid" src="<?= base_url('assets/res/ambassadors/candidates/vote/'.strtoupper($program['code']).'_F.jpg') ?>" alt="Candidate"  data-toggle="modal" data-target="#candidateModal" data-program="<?= $program['code'] ?>" data-gender="F"/>
                    <div class="card-body text-center p-2">
                        <button type="button" class="vote-btn btn btn-warning btn-block" data-program="<?= $program['code'] ?>" data-gender="F">Vote</button>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']        = 'Views/index';
$route['404_override']              = 'Views/error_404';
$route['translate_uri_dashes']      = FALSE;

# Policy
$route['policy']['GET']                         = 'Views/index/policy';

# Migrate
$route['migrate/(:any)/(:num)']     = 'Migrate/index/$1/$2';
$route['migrate/(:any)']            = 'Migrate/index/$1';
$route['migrate']                   = 'Migrate/index';

# Admin
$route['admin/(:any)']                          = 'Controls/index/$1';
$route['admin']                                 = 'Controls/index';

# iTam Ambassadors
$route['ambassadors/(:any)']                    = 'Ambassadors/index/$1';
$route['ambassadors']                           = 'Ambassadors/index';

# TechnoFest
$route['technofest/scholarship']['POST']        = 'Technofest/scholarship_submission';
$route['technofest/register']['POST']           = 'Technofest/index/register';
$route['technofest/(:any)']                     = 'Technofest/index/$1';
$route['technofest']                            = 'Technofest/index';

# TechnoRun
$route['technorun/(:any)']                      = 'Technorun/index/$1';
$route['technorun']                             = 'Technorun/index';

# Election
$route['election/(:any)/(:any)']                = 'Election/$1/$2';
$route['election/(:any)']                       = 'Election/$1';
$route['election']                              = 'Election/index';

# Main Site
$route['(:any)']                                = 'Main/index/$1';

# API
$route['api/ia/candidate']['GET']               = 'Ambassadors/get_candidate';
$route['api/ia/vote']['GET']                    = 'Ambassadors/get_vote';
$route['api/ia/vote']['POST']                   = 'Ambassadors/post_vote';

$route['api/tf/get_participant']['GET']                     = 'Technofest/get_participant';
$route['api/tf/get_scholarship_applicant']['GET']           = 'Technofest/get_scholarship_applicant';
$route['api/tf/get_all_participants']['GET']                = 'Technofest/get_all_participants';
$route['api/tf/get_all_scholarship_applicants']['GET']      = 'Technofest/get_all_scholarship_applicants';
$route['api/tf/get_all_competitors']['GET']                 = 'Technofest/get_all_competitors';
$route['api/tf/register']['POST']                           = 'Technofest/insert_participant';
$route['api/tf/register_scholarship']['POST']               = 'Technofest/scholarship_submission';
$route['api/tf/update_status']['POST']                      = 'Technofest/update_participant_status';
$route['api/tf/delete_participant']['POST']                 = 'Technofest/delete_participant';

$route['api/tr/get_all_participants']['GET']    = 'Technorun/get_all_participants';
$route['api/tr/search']['GET']                  = 'Technorun/search_participant';
$route['api/tr/register']['POST']               = 'Technorun/insert_participant';
$route['api/tr/update_status']['POST']          = 'Technorun/update_participant_status';
$route['api/tr/update_claim']['POST']           = 'Technorun/update_participant_claimed';
$route['api/tr/update_type']['POST']            = 'Technorun/update_participant_type';
$route['api/tr/update_size']['POST']            = 'Technorun/update_participant_size';
$route['api/tr/delete']['POST']                 = 'Technorun/delete_participant';
$route['api/tr/upload_receipt']['POST']         = 'Technorun/upload_receipt';
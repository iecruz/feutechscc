<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IA_candidate_model extends MY_Model
{
    protected $_table       = 'ia_candidates';
    protected $return_type  = 'array';
    protected $belongs_to   = [
        'program' => ['model' => 'Program_model', 'primary_key' => 'program_code']
    ];

    public function __construct()
    {
        parent::__construct();
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program_model extends MY_Model
{
    protected $_table       = 'programs';
    protected $primary_key  = 'code';
    protected $return_type  = 'array';

    public function __construct()
    {
        parent::__construct();
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_access_model extends MY_Model
{
    protected $_table       = 'account_access';
    protected $return_type  = 'array';

    public function __construct()
    {
        parent::__construct();
    }
}
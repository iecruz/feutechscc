<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TF_competitor_model extends MY_Model
{
    protected $_table       = 'technofest_competitors';
    protected $return_type  = 'array';
    protected $belongs_to   = [
        'competition' => ['model' => 'TF_competition_model', 'primary_key' => 'competition_id'],
        'participant' => ['model' => 'TF_participant_model', 'primary_key' => 'participant_id']
    ];

    public function __construct()
    {
        parent :: __construct();
    }
}
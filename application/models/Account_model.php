<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_model extends MY_Model
{
    protected $_table       = 'accounts';
    protected $return_type  = 'array';
    protected $has_many     = [
        'account_access' => [
            'model'         => 'Account_access_model',
            'primary_key'   => 'account_id'
        ]
    ];

    public function __construct()
    {
        parent :: __construct();
    }

    public function authenticate($username, $password)
    {
        $user = $this->get_by(['username' => $username]);
        
        return $user && password_verify($password, $user['password']);
    }

    public function get_with_access_by($where)
    {
        $user = $this->get_by($where);

        $access = $this->account_access->get_many_by(['account_id' => $user['id']]);
        
        foreach($access as $index => $user_access)
        {
            $user['access'][] = $user_access['access_id'];
        }

        return $user;
    }    
}

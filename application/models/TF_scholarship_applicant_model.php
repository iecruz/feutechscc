<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TF_scholarship_applicant_model extends MY_model
{
    protected $_table       = 'technofest_scholarship_applicants';
    protected $return_type  = 'array';
    protected $belongs_to = [
        'participant' => ['model' => 'TF_participant_model', 'primary_key' => 'participant_id']
    ];

    public function __construct()
    {
        parent :: __construct();
    }
}

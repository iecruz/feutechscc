<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IA_vote_model extends MY_Model
{
    protected $_table       = 'ia_votes';
    protected $return_type  = 'array';

    public function __construct()
    {
        parent::__construct();
    }

    public function insert_vote($data)
    {
        $vote = $this->get($data['id']);

        if ($vote == null)
        {
            $insert_data = [
                'id' => $data['id']
            ];

            $this->insert($insert_data);
            $vote = $this->get($data['id']);
        }

        if ($data['gender'] == 'M') 
        {
            if ($vote['vote_male']) 
            {
                $vote_status = 'update';
            } 
            else 
            {
                $vote_status = 'insert';
            }
            
            $update_data = ['vote_male' => $data['program_code']];

            $response_data['status'] = $this->update($data['id'], $data);
        }
        else if ($data['gender'] == 'F')
        {
            if($vote['vote_female']) 
            {
                $vote_status = 'update';
            }
            else
            {
                $vote_status = 'insert';
            }

            $update_data = ['vote_female' => $data['program_code']];

            $response_data['status'] = $this->update($data['id'], $update_data);
        }

        $select_data = [
            'program_code' => $data['program_code'], 
            'gender' => $data['gender']
        ];

        $response_data['candidate'] = $this->get_by($select_data);
        $response_data['query']     = $vote_status;

        return $response_data;
    }
}
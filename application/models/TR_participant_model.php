<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TR_participant_model extends MY_Model
{
    protected $_table       = 'technorun_participants';
    protected $return_type  = 'array';

    public function __construct()
    {
        parent :: __construct();
    }

    public function search($where)
    {
        foreach ($where as $field => $value)
        {
            $this->db->like('UPPER('.$field.')', strtoupper($value));
        }

        return $this->db->select('id, first_name, last_name, school_company, singlet_size, type, status')
            ->from($this->_table)
            ->get()
            ->result_array();
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TF_competition_model extends MY_Model
{
    protected $_table       = 'technofest_competitions';
    protected $return_type  = 'array';
    protected $belongs_to   = [
        'program' => ['model' => 'Program_model', 'primary_key' => 'program_code']
    ];

    public function __construct()
    {
        parent :: __construct();
    }
}
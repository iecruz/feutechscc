<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access_model extends MY_Model
{
    protected $_table       = 'access';
    protected $return_type  = 'array';

    public function __construct()
    {
        parent::__construct();
    }
}
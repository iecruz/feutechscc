<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TF_participant_model extends MY_Model
{
    protected $_table       = 'technofest_participants';
    protected $return_type  = 'array';

    public function __construct()
    {
        parent :: __construct();
    }
}
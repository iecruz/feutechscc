<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_concern_equipments extends CI_migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'              => 'INT',
                'constraint'        => '5',
                'unsigned'          => true,
                'auto_increment'    => true
            ],

            'record_id' => [
                'type'              => 'INT',
                'constraint'        => '5',
                'unsigned'          => true,
            ],

            'building' => [
                'type'              => 'VARCHAR',
                'constraint'        => '128'
            ],

            'laboratory' => [
                'type'              => 'VARCHAR',
                'constraint'        => '32'
            ],

            'equipment' => [
                'type'              => 'VARCHAR',
                'constraint'        => '128'
            ]
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('concern_equipments', true);

        $this->db->query('ALTER TABLE concern_equipments ADD CONSTRAINT concern_equipments_ibfk_1 FOREIGN KEY (record_id) REFERENCES concern_records (id)');
    }

    public function down()
    {
        $this->dbforge->drop_table('concern_equipments', true);
    }
}
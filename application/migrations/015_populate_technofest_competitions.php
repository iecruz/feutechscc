<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_populate_technofest_competitions extends CI_migration
{
    public function up()
    {
        $records = [
            [
                'name' => 'Rube Goldberg Competition',
                'description' => 
                    'Rube Goldberg is simple and compound machines are designed to make work easier. 
                    Through the cartoons of Rube Goldberg, students are engaged in critical thinking about the way his invention make simple tasks even harder to complete.',
                'program_code' => 'CE',
                'price' => '19.99',
                'unit' => 'team'
            ],

            [
                'name' => 'Carrera CpE',
                'description' => 
                    'Carrera CpE is an \'amazing race\' inspired competitions exclusive 
                    for computer engineering students. Participants\' knowledge are 
                    challenged particularly in general engineering, general information 
                    concerning computer systems, and topics included in the course\'s 
                    curriculum. At the same time, they are challenged physically as 
                    the pitstops are scattered around the university\'s premises. 
                    Fun and exciting experience awaits as the Carrera unfolds. 
                    Prizes and bragging rights are awarded to those who are worthy 
                    to be called the first ever Carrera CpE MASTERS!',
                'program_code' => 'CpE',
                'price' => '0.00',
                'unit' => 'person'
            ],

            [
                'name' => 'SHS Android Workshop and Competition',
                'description' => 
                    'This event will provide seminars about Android Programming. 
                    This event will also provide programming competitions based on the Android Language that can help the participants understand more the language. 
                    Also, seminars will cover the basic up to the intermediate aspects of Android Programming. 
                    There will be prizes in store for the winners of the programming competition.',
                'program_code' => 'CS',
                'price' => '0.00',
                'unit' => 'person'
            ],
            
            [
                'name' => 'POSTE: Practicing Outstanding Skills and Technical Endeavors',
                'description' => 
                    'The event is about testing the mental and physical capabilities of the participants in a quiz-like amazing race. 
                    The first round is a qualifying round where participants group into fives to take an individual exam to accumulate certain points to proceed to the next round where they will partake to an amazing race-like game. 
                    The second round is a race where teams go head to head in finishing certain tasks that is assigned in a station, the fastest teams to finish the race will move on to the final round. 
                    The final round is a quiz-bee competition with a twist that consists of easy, moderate and difficult rounds.',
                'program_code' => 'ECE',
                'price' => '19.99',
                'unit' => 'team'
            ],

            [
                'name' => '5-Minute Circuit',
                'description' => 
                    'This tends to test the skill of every electrical engineering student to do a circuit within the given time. 
                    There are only 5 teams and each team only has 5 members. 
                    Each member is given only 1 minute to do a part of the circuit before passing it on to the next team member. 
                    This game will practice the engineering students to be calm and concentrate when they have a limited time only to do their part. 
                    The winner of this game will be judged by their creativity making a circuit and when their circuit will operate properly.',
                'program_code' => 'EE',
                'price' => '9.99',
                'unit' => 'head'
            ],

            [
                'name' => 'Roller Coaster Building Competition',
                'description' => 
                    'This fun, educational, and awesome activity will let the participating Mechanical Engineering Students build their own small-scale model of roller coasters using recyclable materials, mainly by drinking straws; ping pong balls will be used to represent the passengers of the coaster and to test the ride.',
                'program_code' => 'ME',
                'price' => '29.99',
                'unit' => 'team'
            ],
            
            [
                'name' => 'HackaWeb',
                'description' => 
                    'The competition will cater to the general Information Technology Community. 
                    It will be a system design and development competition where participants will be given one whole day to create a web application with analytics that is in line with the Technofest theme. 
                    On the second day of the competition, participants will be pitching and proposing their ideas and web systems.',
                'program_code' => 'IT',
                'price' => '9.99',
                'unit' => 'pair'
            ]
        ];

        $this->db->insert_batch('technofest_competitions', $records);
    }

    public function down()
    {
        $this->db->empty_table('technofest_competitions');
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_technofest_participants extends CI_migration 
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => '5',
                'unsigned' => true,
                'auto_increment' => true
            ],

            'first_name' => [
                'type' => 'VARCHAR',
                'constraint' => '32',
            ],

            'last_name' => [
                'type' => 'VARCHAR',
                'constraint' => '32',
            ],
            
            'type' => [
                'type' => 'VARCHAR',
                'constraint' => '32',
            ],

            'email_address' => [
                'type' => 'VARCHAR',
                'constraint' => '32',
                'null' => true
            ],

            'contact_number' => [
                'type' => 'VARCHAR',
                'constraint' => '32',
                'null' => true
            ],
            
            'school' => [
                'type' => 'VARCHAR',
                'constraint' => '100'
            ],

            'country' => [
                'type' => 'VARCHAR',
                'constraint' => '64'
            ],

            'accommodation' => [
                'type' => 'VARCHAR',
                'constraint' => '16'
            ],

            'food_preference' => [
                'type' => 'VARCHAR',
                'constraint' => '32'
            ],

            'attendee' => [
                'type' => 'BOOLEAN',
                'default' => true
            ],

            'presenter' => [
                'type' => 'BOOLEAN',
                'default' => false
            ],

            'competitor' => [
                'type' => 'BOOLEAN',
                'default' => false
            ],

            'status' => [
                'type' => 'BOOLEAN',
                'default' => false
            ],

            'date_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP'
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('technofest_participants', true);
    }


    public function down()
    {
        $this->dbforge->drop_table('technofest_participants', true);
    }
}
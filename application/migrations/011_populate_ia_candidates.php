<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_populate_ia_candidates extends CI_migration 
{
    public function up()
    {
        $candidates = [
            [
                'first_name'    => 'Renato',
                'last_name'     => 'Verdoza',
                'gender'        => 'M',
                'year'          => '2017',
                'program_code'  => 'CE'
            ],

            [
                'first_name'    => 'Ann',
                'last_name'     => 'Zate',
                'gender'        => 'F',
                'year'          => '2017',
                'program_code'  => 'CE'
            ],

            [
                'first_name'    => 'Allen Joshua',
                'last_name'     => 'Abalos',
                'gender'        => 'M',
                'year'          => '2017',
                'program_code'  => 'CpE'
            ],

            [
                'first_name'    => 'Nicole',
                'last_name'     => 'Bonifacio',
                'gender'        => 'F',
                'year'          => '2017',
                'program_code'  => 'CpE'
            ],

            [
                'first_name'    => 'Xavier',
                'last_name'     => 'Crisostomo',
                'gender'        => 'M',
                'year'          => '2017',
                'program_code'  => 'CS'
            ],

            [
                'first_name'    => 'Alyssa',
                'last_name'     => 'Faustino',
                'gender'        => 'F',
                'year'          => '2017',
                'program_code'  => 'CS'
            ],

            [
                'first_name'    => 'John Louise',
                'last_name'     => 'Adlong',
                'gender'        => 'M',
                'year'          => '2017',
                'program_code'  => 'EE'
            ],

            [
                'first_name'    => 'Nicole',
                'last_name'     => 'Vasquez',
                'gender'        => 'F',
                'year'          => '2017',
                'program_code'  => 'EE'
            ],

            [
                'first_name'    => 'Mark',
                'last_name'     => 'Bautista',
                'gender'        => 'M',
                'year'          => '2017',
                'program_code'  => 'ECE'
            ],

            [
                'first_name'    => 'Franzeska',
                'last_name'     => 'Sambrano',
                'gender'        => 'F',
                'year'          => '2017',
                'program_code'  => 'ECE'
            ],

            [
                'first_name'    => 'Lawrence',
                'last_name'     => 'Esparrago',
                'gender'        => 'M',
                'year'          => '2017',
                'program_code'  => 'IT'
            ],

            [
                'first_name'    => 'Rochelle',
                'last_name'     => 'Padilla',
                'gender'        => 'F',
                'year'          => '2017',
                'program_code'  => 'IT'
            ],

            [
                'first_name'    => 'Hisam',
                'last_name'     => 'Abu Hassira',
                'gender'        => 'M',
                'year'          => '2017',
                'program_code'  => 'ME'
            ],

            [
                'first_name'    => 'Marri',
                'last_name'     => 'Alejo',
                'gender'        => 'F',
                'year'          => '2017',
                'program_code'  => 'ME'
            ]
        ];

        $this->db->insert_batch('ia_candidates', $candidates);
    }

    public function down()
    {
        $this->db->empty_table('ia_candidates');
    }
}
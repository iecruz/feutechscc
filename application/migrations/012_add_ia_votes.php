<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_ia_votes extends CI_migration 
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'VARCHAR',
                'constraint' => '32'
            ],
            
            'vote_male' => [
                'type' => 'VARCHAR',
                'constraint' => '8',
                'null' => true
            ],

            'vote_female' => [
                'type' => 'VARCHAR',
                'constraint' => '8',
                'null' => true
            ],

            'date_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP'
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('ia_votes', true);
    }

    public function down()
    {
        $this->dbforge->drop_table('ia_votes', true);
    }
}
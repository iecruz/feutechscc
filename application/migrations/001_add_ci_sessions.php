<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_ci_sessions extends CI_migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'VARCHAR',
                'constraint' => '128'
            ],
            
            'ip_address' => [
                'type' => 'VARCHAR',
                'constraint' => '45'
            ],
            
            'timestamp' => [
                'type' => 'BIGINT',
                'default' => '0'
            ],
            
            'data' => [
                'type' => 'TEXT'
            ]
        ]);

        $this->dbforge->add_key('timestamp');
        $this->dbforge->create_table('ci_sessions', true);
    }

    public function down()
    {
        $this->dbforge->drop_table('ci_sessions');
    }
}

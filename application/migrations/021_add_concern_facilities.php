<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_concern_facilities extends CI_migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'              => 'INT',
                'constraint'        => '5',
                'unsigned'          => true,
                'auto_increment'    => true
            ],

            'record_id' => [
                'type'              => 'INT',
                'constraint'        => '5',
                'unsigned'          => true,
            ],

            'building' => [
                'type'              => 'VARCHAR',
                'constraint'        => '128'
            ],

            'floor' => [
                'type'              => 'VARCHAR',
                'constraint'        => '16'
            ],

            'room' => [
                'type'              => 'VARCHAR',
                'constraint'        => '32'
            ],

            'facility' => [
                'type'              => 'VARCHAR',
                'constraint'        => '128',
                'null'              => true
            ]
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('concern_facilities', true);

        $this->db->query('ALTER TABLE concern_facilities ADD CONSTRAINT concern_facilities_ibfk_1 FOREIGN KEY (record_id) REFERENCES concern_records (id)');
    }

    public function down()
    {
        $this->dbforge->drop_table('concern_facilites', true);
    }
}
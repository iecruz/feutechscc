<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_technofest_competitors extends CI_migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => '5',
                'unsigned' => true,
                'auto_increment' => true
            ],
            
            'participant_id' => [
                'type' => 'INT',
                'constraint' => '5',
                'unsigned' => true
            ],
            
            'competition_id' => [
                'type' => 'INT',
                'constraint' => '5',
                'unsigned' => true
            ]
        ]);

        $this->dbforge->add_key('participant_id');
        $this->dbforge->add_key('competition_id');
        $this->dbforge->add_key('id', true);

        $this->dbforge->create_table('technofest_competitors', true);

        $this->db->query('ALTER TABLE technofest_competitors ADD CONSTRAINT technofest_competitors_ibfk_1 FOREIGN KEY (participant_id) REFERENCES technofest_participants (id)');
        $this->db->query('ALTER TABLE technofest_competitors ADD CONSTRAINT technofest_competitors_ibfk_2 FOREIGN KEY (competition_id) REFERENCES technofest_competitions (id)');
    }

    public function down()
    {
        $this->dbforge->drop_table('technofest_competitors', true);
    }
}
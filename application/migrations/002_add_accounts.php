<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_accounts extends CI_migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => '5',
                'unsigned' => true,
                'auto_increment' => true
            ],

            'first_name' => [
                'type' => 'VARCHAR',
                'constraint' => '32',
                'null' => true
            ],

            'last_name' => [
                'type' => 'VARCHAR',
                'constraint' => '32',
                'null' => true
            ],

            'email_address' => [
                'type' => 'VARCHAR',
                'constraint' => '64',
                'null' => true
            ],

            'contact_number' => [
                'type' => 'VARCHAR',
                'constraint' => '32',
                'null' => true
            ],

            'username' => [
                'type' => 'VARCHAR',
                'constraint' => '32',
                'unique' => true
            ],

            'password' => [
                'type' => 'TEXT'
            ]
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('accounts', true);
    }


    public function down()
    {
        $this->dbforge->drop_table('accounts', true);
    }
}

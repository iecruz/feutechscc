<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_populate_programs extends CI_migration 
{
    public function up()
    {
        $programs = [
            [
                'code' => 'CE',
                'name' => 'Civil Engineering',
                'full_name' => 'Bachelor of Science Civil Engineering'
            ],

            [
                'code' => 'CpE',
                'name' => 'Computer Engineering',
                'full_name' => 'Bachelor of Science Computer Engineering'
            ],

            [
                'code' => 'CS',
                'name' => 'Computer Science',
                'full_name' => 'Bachelor of Science Computer Science'
            ],

            [
                'code' => 'ECE',
                'name' => 'Electronics Engineering',
                'full_name' => 'Bachelor of Science Electronics Engineering'
            ],

            [
                'code' => 'EE',
                'name' => 'Electrical Engineering',
                'full_name' => 'Bachelor of Science Electrical Engineering'
            ],

            [
                'code' => 'IT',
                'name' => 'Information Technology',
                'full_name' => 'Bachelor of Science Information Technology'
            ],

            [
                'code' => 'ME',
                'name' => 'Mechanical Engineering',
                'full_name' => 'Bachelor of Science Mechanical Engineering'
            ],
        ];

        $this->db->insert_batch('programs', $programs);
    }

    public function down()
    {
        $this->db->empty_table('programs');
    }
}
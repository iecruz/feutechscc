<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_concern_records extends CI_migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'              => 'INT',
                'constraint'        => '5',
                'unsigned'          => true,
                'auto_increment'    => true
            ],

            'category_id' => [
                'type'              => 'INT',
                'constraint'        => '5',
                'unsigned'          => true
            ],

            'student_id' => [
                'type'              => 'VARCHAR',
                'constraint'        => '9',
                'null'              => true
            ],

            'description' => [
                'type'              => 'TEXT'
            ],

            'recommendation' => [
                'type'              => 'TEXT',
                'null'              => true
            ],

            'image_link' => [
                'type'              => 'TEXT',
                'null'              => true
            ]
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('concern_records', true);
    }

    public function down()
    {
        $this->dbforge->drop_table('concern_records', true);
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_access extends CI_migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'VARCHAR',
                'constraint'    => '16'
            ],

            'name' => [
                'type'          => 'VARCHAR',
                'constraint'    => '64'
            ],

            'description' => [
                'type'          => 'TEXT'
            ]
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('access', true);
    }

    public function down()
    {
        $this->dforge->drop_table('access', true);
    }
}
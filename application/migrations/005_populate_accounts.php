<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_populate_accounts extends CI_migration
{
    public function up()
    {
        $records = [
            [
                'first_name'        => 'Ian',
                'last_name'         => 'Cruz',
                'email_address'     => 'iecruz404@gmail.com',
                'contact_number'    => '9777472966',
                'username'          => 'iecruz',
                'password'          =>  password_hash('Minecraft_42', PASSWORD_BCRYPT)
            ],

            [
                'first_name'        => 'John Jovert',
                'last_name'         => 'Ruedas',
                'email_address'     => 'jruedas26@gmail.com',
                'contact_number'    => '9277556612',
                'username'          => 'Trevs26',
                'password'          => password_hash('Jovert26', PASSWORD_BCRYPT)
            ]
        ];
        
        $this->db->insert_batch('accounts', $records);
        
        $records = [
            [
                'username'          => 'feutechscc',
                'password'          => password_hash('scctechnology', PASSWORD_BCRYPT)
            ],
            
            [
                'username'          => 'technorun',
                'password'          => password_hash('technorun2018', PASSWORD_BCRYPT)
            ],
            
            [
                'username'          => 'technofest',
                'password'          => password_hash('technofest2018', PASSWORD_BCRYPT)
            ]
        ];

        $this->db->insert_batch('accounts', $records);
    }

    public function down()
    {
        $this->db->empty_table('accounts');
    }
}
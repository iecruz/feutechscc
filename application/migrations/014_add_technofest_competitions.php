<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_technofest_competitions extends CI_migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => '5',
                'unsigned' => true,
                'auto_increment' => true
            ],

            'name' => [
                'type' => 'TEXT'
            ],

            'description' => [
                'type' => 'TEXT'
            ],

            'program_code' => [
                'type' => 'VARCHAR',
                'constraint' => '4'
            ],

            'price' => [
                'type' => 'DECIMAL',
                'constraint' => '10,2',
                'default' => 0
            ],

            'unit' => [
                'type' => 'VARCHAR',
                'constraint' => '64',
                'null' => true
            ]
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('technofest_competitions', true);

        $this->db->query('ALTER TABLE technofest_competitions ADD CONSTRAINT technofest_competitions_ibfk_1 FOREIGN KEY (program_code) REFERENCES programs (code)');
    }

    public function down()
    {
        $this->dbforge->drop_table('technofest_competitions', true);
    }
}
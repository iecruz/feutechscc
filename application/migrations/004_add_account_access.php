<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_account_access extends CI_migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'account_id'    => [
                'type' => 'INT',
                'constraint' => '5',
                'unsigned' => true,
            ],

            'access_id' => [
                'type'          => 'VARCHAR',
                'constraint'    => '16'
            ]
        ]);

        $this->dbforge->create_table('account_access', true);
        
        $this->db->query('ALTER TABLE account_access ADD CONSTRAINT account_access_ibfk_1 FOREIGN KEY (account_id) REFERENCES accounts (id)');
        $this->db->query('ALTER TABLE account_access ADD CONSTRAINT account_access_ibfk_2 FOREIGN KEY (access_id) REFERENCES access (id)');
    }

    public function down()
    {
        $this->dbforge->drop_table('account_access', true);
    }
}
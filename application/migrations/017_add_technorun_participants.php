<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_technorun_participants extends CI_migration 
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => '5',
                'unsigned' => true,
                'auto_increment' => true
            ],

            'first_name' => [
                'type' => 'VARCHAR',
                'constraint' => '32'
            ],

            'last_name' => [
                'type' => 'VARCHAR',
                'constraint' => '32'
            ],

            'email_address' => [
                'type' => 'VARCHAR',
                'constraint' => '64'
            ],

            'contact_number' => [
                'type' => 'VARCHAR',
                'constraint' => '32',
                'null' => true
            ],
            
            'school_company' => [
                'type' => 'TEXT',
                'null' => true
            ],
            
            'singlet_size' => [
                'type' => 'VARCHAR',
                'constraint' => '8',
                'null' => true
            ],
            
            'mode_of_payment' => [
                'type' => 'VARCHAR',
                'constraint' => '16'
            ],

            'type' => [
                'type' => 'VARCHAR',
                'constraint' => '8'
            ],

            'status' => [
                'type' => 'BOOLEAN',
                'default' => false
            ],

            'singlet_claimed' => [
                'type' => 'BOOLEAN',
                'default' => false
            ],

            'receipt_upload_status' => [
                'type' => 'BOOLEAN',
                'default' => false
            ],

            'receipt_link' => [
                'type' => 'TEXT',
                'null' => true
            ],

            'date_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP'
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('technorun_participants', true);
    }

    public function down()
    {
        $this->dbforge->drop_table('technorun_participants', true);
    }
}
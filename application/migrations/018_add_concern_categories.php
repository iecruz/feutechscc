<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_concern_categories extends CI_migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'              => 'INT',
                'constraint'        => '5',
                'unsigned'          => true,
                'auto_increment'    => true
            ],

            'name' => [
                'type'              => 'VARCHAR',
                'constraint'        => '64',
            ],
            
            'record_count' => [
                'type'              => 'INT',
                'constraint'        => '6',
                'default'           => '0',
            ]
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('concern_categories', true);
    }

    public function down()
    {
        $this->dbforge->drop_table('concern_categories', true);
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_programs extends CI_migration 
{
    public function up()
    {
        $this->dbforge->add_field([
            'code' => [
                'type' => 'VARCHAR',
                'constraint' => '4'
            ],

            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '64'
            ],

            'full_name' => [
                'type' => 'VARCHAR',
                'constraint' => '128'
            ]
        ]);

        $this->dbforge->add_key('code', true);
        $this->dbforge->create_table('programs', true);
    }


    public function down()
    {
        $this->dbforge->drop_table('programs', true);
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_ia_candidates extends CI_migration 
{
    public function up()
    {
        $this->dbforge->add_field([ 
            'id' => [
                'type' => 'INT',
                'constraint' => '5',
                'unsigned' => true,
                'auto_increment' => true
            ],

            'first_name' => [
                'type' => 'VARCHAR',
                'constraint' => '32',
            ],

            'last_name' => [
                'type' => 'VARCHAR',
                'constraint' => '32',
            ],

            'gender' => [
                'type' => 'VARCHAR',
                'constraint' => '1',
            ],

            'year' => [
                'type' => 'VARCHAR',
                'constraint' => '4',
            ],

            'program_code' => [
                'type' => 'VARCHAR',
                'constraint' => '8',
            ],

            'description' => [
                'type' => 'TEXT',
                'null' => true,
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('ia_candidates', true);

        $this->db->query('ALTER TABLE ia_candidates ADD CONSTRAINT ia_candidates_ibfk_1 FOREIGN KEY (program_code) REFERENCES programs (code)');
    }

    public function down()
    {
        $this->dbforge->drop_table('ia_candidates', true);
    }
}
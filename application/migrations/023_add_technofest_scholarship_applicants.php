<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_technofest_scholarship_applicants extends CI_migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'              => 'INT',
                'constraint'        => '5',
                'unsigned'          => true,
                'auto_increment'    => true
            ],

            'participant_id' => [
                'type'              => 'INT',
                'constraint'        => '5',
                'unsigned'          => true
            ],

            'paper_url' => [
                'type'              => 'VARCHAR',
                'constraint'        => '255'
            ],

            'answer_1' => [
                'type'              => 'TEXT'
            ],

            'answer_2' => [
                'type'              => 'TEXT'
            ],

            'created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP'
        ]);

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('technofest_scholarship_applicants', TRUE);

        $this->db->query('ALTER TABLE technofest_scholarship_applicants ADD CONSTRAINT technofest_scholarship_applicants_ibfk_1 FOREIGN KEY (participant_id) REFERENCES technofest_participants (id)');
    }

    public function down()
    {
        $this->dbforge->drop_table('technofest_scholarship_applicants', TRUE);
    }
}
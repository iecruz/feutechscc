<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_populate_concern_categories extends CI_migration
{
    public function up()
    {
        $records = [
            ['name'  => 'Facilities'],
            ['name'  => 'Computer Equipments'],
            ['name'  => 'Academic Policies'],
            ['name'  => 'Faculty'],
            ['name'  => 'Departments'],
            ['name'  => 'Personnels'],
            ['name'  => 'Others']
        ];

        $this->db->insert_batch('concern_categories', $records);
    }

    public function down()
    {
        $this->db->empty_table('concern_category');
    }
}
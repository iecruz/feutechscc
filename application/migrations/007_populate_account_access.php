<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_populate_account_access extends CI_migration
{
    public function up()
    {
        $records = [
            [
                'account_id'    => '1',
                'access_id'     => 'ROOT'
            ],
            [
                'account_id'    => '1',
                'access_id'     => 'SCC'
            ],
            [
                'account_id'    => '1',
                'access_id'     => 'TF_VIEW'
            ],
            [
                'account_id'    => '1',
                'access_id'     => 'TF_MODIFY'
            ],
            [
                'account_id'    => '1',
                'access_id'     => 'TR_VIEW'
            ],
            [
                'account_id'    => '1',
                'access_id'     => 'TR_MODIFY'
            ],
            [
                'account_id'    => '1',
                'access_id'     => 'IA_VIEW'
            ],
            [
                'account_id'    => '1',
                'access_id'     => 'IA_MODIFY'
            ],

            [
                'account_id'    => '2',
                'access_id'     => 'SCC'
            ],
            [
                'account_id'    => '2',
                'access_id'     => 'TF_VIEW'
            ],
            [
                'account_id'    => '2',
                'access_id'     => 'TF_MODIFY'
            ],
            [
                'account_id'    => '2',
                'access_id'     => 'TR_VIEW'
            ],
            [
                'account_id'    => '2',
                'access_id'     => 'TR_MODIFY'
            ],
            [
                'account_id'    => '2',
                'access_id'     => 'IA_VIEW'
            ],
            [
                'account_id'    => '2',
                'access_id'     => 'IA_MODIFY'
            ],

            [
                'account_id'    => '3',
                'access_id'     => 'SCC'
            ],
            [
                'account_id'    => '3',
                'access_id'     => 'TF_VIEW'
            ],
            [
                'account_id'    => '3',
                'access_id'     => 'TF_MODIFY'
            ],
            [
                'account_id'    => '3',
                'access_id'     => 'TR_VIEW'
            ],
            [
                'account_id'    => '3',
                'access_id'     => 'TR_MODIFY'
            ],
            [
                'account_id'    => '3',
                'access_id'     => 'IA_VIEW'
            ],
            [
                'account_id'    => '3',
                'access_id'     => 'IA_MODIFY'
            ],
            
            [
                'account_id'    => '4',
                'access_id'     => 'TR_VIEW'
            ],

            [
                'account_id'    => '5',
                'access_id'     => 'TF_VIEW'
            ]
        ];

        $this->db->insert_batch('account_access', $records);
    }

    public function down()
    {
        $this->db->empty_table('account_access');
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_populate_access extends CI_migration
{
    public function up()
    {
        $records = [
            [
                'id'            => 'ROOT',
                'name'          => 'Root Access',
                'description'   => 'Allow account to modify the system'
            ],

            [
                'id'            => 'SCC',
                'name'          => 'SCC Access',
                'description'   => 'Allow account to modify and view content of SCC webpage'
            ],

            [
                'id'            => 'TF_VIEW',
                'name'          => 'TechnoFest View Access',
                'description'   => 'Allow account to view TechnoFest'
            ],

            [
                'id'            => 'TF_MODIFY',
                'name'          => 'TechnoFest Modify Access',
                'description'   => 'Allow account to modify TechnoFest'
            ],

            [
                'id'            => 'TR_VIEW',
                'name'          => 'TechnoFest View Access',
                'description'   => 'Allow account to view TechnoRun'
            ],

            [
                'id'            => 'TR_MODIFY',
                'name'          => 'TechnoFest Modify Access',
                'description'   => 'Allow account to modify TechnoRun'
            ],

            [
                'id'            => 'IA_VIEW',
                'name'          => 'iTam Ambassadors View Access',
                'description'   => 'Allow account to view iTam Ambassadors'
            ],

            [
                'id'            => 'IA_MODIFY',
                'name'          => 'iTam Ambassadors Modify Access',
                'description'   => 'Allow account to modify iTam Ambassadors'
            ]
        ];

        $this->db->insert_batch('access', $records);
    }

    public function down()
    {
        $this->db->empty_table('access');
    }
}
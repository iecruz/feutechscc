<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    protected $_user                = null;
    protected $view_directory       = '';
    protected $local_js_directory   = 'assets/js';
    protected $local_css_directory  = 'assets/css';
    
    protected $default_view = 'home';
    
    public $default_js      = [
        'script.js',
        'api.js',
        'add_ons.js'
    ];

    public $default_css     = [
        'style.css',
        'add_ons.css'
    ];

    public $cdn_js          = [];
    public $cdn_css         = [];
    public $include_js      = [];
    public $include_css     = [];
    public $include_font    = [];

    public $require_header  = true;
    public $require_footer  = true;
    public $require_load    = false;

    public $page_title      = 'SCC';

    public function __construct()
    {
        parent::__construct();
    }

    public function load_view($view, $page_data = [])
    {
        $view = $view ? $view : $this->default_view;

        $view_path = $this->view_directory.'/'.$view;

        $this->include_js   = array_merge($this->default_js, $this->include_js);
        $this->include_css  = array_merge($this->default_css, $this->include_css);

        foreach ($this->include_js as $index => $js)
        {
            $this->include_js[$index] = $this->local_js_directory.'/'.$js;
        }

        foreach ($this->include_css as $index => $css)
        {
            $this->include_css[$index] = $this->local_css_directory.'/'.$css;
        }

        $data = [
            'page_title'        => $this->page_title,
            'js_path'           => $this->local_js_directory,
            'css_path'          => $this->local_css_directory,
            'cdn_js'            => $this->cdn_js,
            'cdn_css'           => $this->cdn_css,
            'js'                => $this->include_js,
            'css'               => $this->include_css,
            'font'              => $this->include_font,
            'require_header'    => $this->require_header,
            'require_footer'    => $this->require_footer,
            'require_load'      => $this->require_load
        ];

        $data = array_merge($data, $page_data);

        $this->load->view('partials/header', $data);
        $this->load->view($view_path, $data);
        $this->load->view('partials/footer', $data);
    }

    public function error_404()
    {
        $this->load->view('other/404');
    }

    public function validate_access($access_id)
    {
        foreach($access_id as $id)
        {
            if (in_array($id, $this->session->userdata('admin')['access']))
            {
                return true;
                break;
            }
        }

        return false;
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controls extends MY_Controller
{
    protected $view_directory = 'admin';

    public $page_title      = "Admin";
    public $cdn_css         = [
        'https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css'
    ];
    public $cdn_js          = [
        'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js',
        'https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js'
    ];
    public $require_footer  = false;
    public $require_load    = false;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function index($view = 'home')
    {
        switch($view)
        {
            case 'login':
                return $this->login();
                break;

            case 'logout':
                return $this->logout();
                break;
        }

        if (!$this->session->userdata('admin'))
        {
            $this->session->set_flashdata('redirect_page', $view);
            return redirect('admin/login');
        }
        
        switch($view)
        {
            case 'ambassadors':
                if ($this->validate_access(['IA_VIEW', 'IA_MODIFY', 'SCC']))
                {
                    return $this->ambassadors();
                }
                return redirect();
                break;

            case 'technorun':
                if ($this->validate_access(['TR_VIEW', 'TR_MODIFY', 'SCC']))
                {
                    return $this->technorun();
                }
                return redirect();
                break;

            case 'technofest':
                if ($this->validate_access(['TF_VIEW', 'TF_MODIFY', 'SCC']))
                {
                    return $this->technofest();
                }
                return redirect();
                break;

            default:
                return show_404();
        }
    }

    public function ambassadors()
    {
        $page_data['programs'] = ['CE', 'CpE', 'CS', 'ECE', 'EE', 'IT', 'ME'];


        $page_data['total_count'] = count($this->ia_vote->get_all());

        foreach($page_data['programs'] as $program)
        {
            $page_data['vote_counts'][$program]['M'] = count($this->ia_vote->get_many_by(['vote_male' => $program]));
            $page_data['vote_counts'][$program]['F'] = count($this->ia_vote->get_many_by(['vote_female' => $program]));
        }

        return $this->load_view('ambassadors', $page_data);
    }
    
    public function technorun()
    {
        $this->include_js[] = 'admin_technorun.js';

        $page_data['categories'] = [
            '3k'    => 199.00,
            '5k'    => 299.00,
            '10k'   => 599.00
        ];

        $page_data['status']    = [
            'paid' => true,
            'unpaid' => false
        ];

        $page_data['sizes']     = [
            'Extra Small'   => 'XS',
            'Small'         => 'S',
            'Medium'        => 'M',
            'Large'         => 'L',
            'Extra Large'   => 'XL',
            '2 Extra Large' => '2Xl',
            '3 Extra Large' => '3Xl'
        ];

        $page_data['count_total']['all']    = count($this->tr_participant->get_all());
        $page_data['count_total']['paid']   = count($this->tr_participant->get_many_by(['status' => true]));
        $page_data['count_total']['unpaid'] = count($this->tr_participant->get_many_by(['status' => false]));

        $page_data['pay_total']['paid']     = 0;
        $page_data['pay_total']['unpaid']   = 0;

        foreach($page_data['categories'] as $category => $price)
        {
            $page_data['count'][$category.'_internal']  = count($this->tr_participant->get_many_by(['type' => $category, 'UPPER(school_company) like UPPER(\'%FEU%\')']));
            $page_data['count'][$category.'_external']  = count($this->tr_participant->get_many_by(['type' => $category, 'UPPER(school_company) not like UPPER(\'%FEU%\')']));
            
            foreach($page_data['status'] as $status_key => $status_value) 
            {
                $page_data['pay'][$status_key.'_'.$category]    = count($this->tr_participant->get_many_by(['type' => $category, 'status' => $status_value]));
                $page_data['pay_total'][$status_key]            += $page_data['pay'][$status_key.'_'.$category] * $price;

                if($category == '3k')
                {
                    continue;
                }

                foreach($page_data['sizes'] as $size)
                {
                    $page_data['singlet'][$status_key.'_'.$size.'_'.$category] = count($this->tr_participant->get_many_by(['type' => $category, 'status' => $status_value, 'singlet_size' => $size]));
                }
            }
        }

        return $this->load_view('technorun', $page_data);
    }

    public function technofest()
    {
        $this->include_js[] = 'admin_technofest.js';

        $page_data = [];

        $page_data['career_type'] = ['student', 'professional'];
        $page_data['accommodation'] = ['with_accommodation', 'without_accommodation'];
        $page_data['locale'] = ['local_internal', 'local_external', 'international'];

        $page_data['competition'] = [];

        foreach($this->tf_competition->get_all() as $competition) {
            $page_data['competition'] = $competition['name'];
        }

        foreach($page_data['career_type'] as $career_type) {
            foreach($page_data['accommodation'] as $accommodation) {
                $page_data['count'][$career_type][$accommodation]['local_internal'] = count($this->tf_participant->get_many_by(['country' => 'Philippines', 'type' => $career_type, 'accommodation' => ($accommodation == 'with_accommodation' ? 'live_in' : 'live_out'), 'UPPER(school) LIKE UPPER(\'%FEU%\')']));
                $page_data['count'][$career_type][$accommodation]['local_external'] = count($this->tf_participant->get_many_by(['country' => 'Philippines', 'type' => $career_type, 'accommodation' => ($accommodation == 'with_accommodation' ? 'live_in' : 'live_out'), 'UPPER(school) NOT LIKE UPPER(\'%FEU%\')']));
                $page_data['count'][$career_type][$accommodation]['international'] = count($this->tf_participant->get_many_by(['country != \'Philippines\'', 'type' => $career_type, 'accommodation' => ($accommodation == 'with_accommodation' ? 'live_in' : 'live_out')]));
            }
        }

        return $this->load_view('technofest', $page_data);
    }

    public function login()
    {
        if ($this->session->userdata('admin'))
        {
            return redirect();
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $username = $this->input->post('username');
    
            if ($this->account->authenticate($username, $this->input->post('password'))) 
            {
                $this->_user = $this->account->get_with_access_by(['username' => $username]);
                $this->session->set_userdata('admin', $this->account->get_with_access_by(['username' => $username]));
                return redirect('admin/'.$this->session->flashdata('redirect_page'));
            }
        }
        
        $this->session->set_flashdata('redirect_page', $this->session->flashdata('redirect_page'));
        return $this->load_view('login');
    }


    public function logout()
    {
        $this->_user = null;
        $this->session->unset_userdata('admin');
        return redirect();
    }
}
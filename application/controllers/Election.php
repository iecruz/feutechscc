<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Election extends MY_Controller
{
    protected $view_directory = 'election';

    public $page_title      = "Student Coordinating Council Election 2018";
    public $include_font    = [
        'Monsterrat'
    ];
    public $include_css     = [
        'election.css'
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['candidates'] = [
            [
                'name' => 'Andrei Caoile',
                'course' => 'CpE Representative',
                'link' => 'andrei_caoile'
            ],
            [
                'name' => 'Glenn Cadorna',
                'course' => 'ECE Representative',
                'link' => 'glenn_cadorna'
            ],
            [
                'name' => 'Lance Gonzales',
                'course' => 'IT Representative',
                'link' => 'lance_gonzales'
            ],
            [
                'name' => 'Melissa Lumbre',
                'course' => 'CS Representative',
                'link' => 'melissa_lumbre'
            ],
            [
                'name' => 'Paul Espinoza',
                'course' => 'ME Representative',
                'link' => 'paul_espinoza'
            ],
            [
                'name' => 'Rafael Diaz',
                'course' => 'CE Representative',
                'link' => 'rafael_diaz'
            ]
        ];

        return $this->load_view('index', $data);
    }

    public function profile($candidate_link)
    {
        $data = [];
        switch($candidate_link)
        {
            case 'andrei_caoile':
                $data['name'] = 'Andrei Caoile';
                $data['course'] = 'CE Representative';
                $data['descriptions'] = [
                    [
                        'title' => 'Officer Trainee',
                        'subtitle' => 'Computer Engineering Organization',
                        'year' => '2017 - 2018'
                    ],
                    [
                        'title' => 'Elite Scholar',
                        'subtitle' => 'FEU Institute of Technology',
                        'year' => '2015 - 2018'
                    ],
                    [
                        'title' => '1st Place',
                        'subtitle' => 'Field Programmable Gate Array Competition',
                        'year' => '2015 - 2018'
                    ],
                    [
                        'title' => 'Elite Scholar',
                        'subtitle' => 'FEU Institute of Technology',
                        'year' => '2015 - 2018'
                    ],
                    [
                        'title' => 'Batch 2015',
                        'subtitle' => 'Ramon Magsaysay Cubao - High School'
                    ]
                ];
                break;

            case 'glenn_cadorna':
                $data['name'] = 'Glenn Cadorna';
                $data['course'] = 'ECE Representative';
                $data['descriptions'] = [
                    [
                        'title' => 'Logistics Co-Head',
                        'subtitle' => 'TechnoRun',
                        'year' => '2018'
                    ],
                    [
                        'title' => 'Member',
                        'subtitle' => 'Electronics Engineering Students\' Society',
                        'year' => '2016 - 2018'
                    ],
                    [
                        'title' => 'Cadet Captain - Company Commander',
                        'subtitle' => 'Citizenship Advancement Training<br>Lourdes School of Mandaluyong',
                        'year' => '2016'
                    ],
                    [
                        'title' => 'Private 2nd Class',
                        'subtitle' => 'Philippine Living History<br>Imperial Japanese Army',
                        'year' => '2018'
                    ]
                ];
                break;

            case 'melissa_lumbre':
                $data['name'] = 'Melissa Lumbre';
                $data['course'] = 'CS Representative';
                $data['descriptions'] = [
                    [
                        'title' => 'Senior Sister',
                        'subtitle' => 'Youth for Christ - FEU Institute of Technology',
                        'year' => '2017 - 2018'
                    ],
                    [
                        'title' => 'Secretary',
                        'subtitle' => 'Youth for Christ - FEU Institute of Technology',
                        'year' => '2016 - 2017'
                    ],
                    [
                        'title' => '4th Year Representative',
                        'subtitle' => 'Student Council<br>The Palmridge School - Bacoor Cavite',
                        'year' => '2013 - 2014'
                    ],
                    [
                        'title' => 'Member',
                        'subtitle' => 'Association for Computing Machinery',
                        'year' => '2016 - 2017'
                    ]
                ];
                break;

            case 'paul_espinoza':
                $data['name'] = 'Paul Espinoza';
                $data['course'] = 'ME Representative';
                $data['descriptions'] = [
                    [
                        'title' => 'President',
                        'subtitle' => 'American Society of Mechanical Engineers<br>Mechanical Engineering Chain of Societies',
                        'year' => '2017 - 2018'
                    ],
                    [
                        'title' => 'Sector Youth Head',
                        'subtitle' => 'Youth for Christ Campus Based West B Sector',
                        'year' => '2016 - 2018'
                    ],
                    [
                        'title' => 'Logistics Head',
                        'subtitle' => 'Hosanna: Youth for Christ, West B - Sector Conference 2016',
                        'year' => '2016'
                    ],
                    [
                        'title' => 'Batch 2012',
                        'subtitle' => 'Bataan National High School'
                    ]
                ];
                break;

            case 'rafael_diaz':
                $data['name'] = 'Rafael Diaz';
                $data['course'] = 'CE Representative';
                $data['descriptions'] = [
                    [
                        'title' => 'Treasurer',
                        'subtitle' => 'Association of Civil Engineering Students',
                        'year' => '2018'
                    ],
                    [
                        'title' => 'JPICE EXIMIA - Potentia Title Holder',
                        'subtitle' => 'Junior Philippine Institute of Civil Engineers',
                        'year' => '2018'
                    ],
                    [
                        'title' => 'Secretariat Head',
                        'subtitle' => 'Converse<sup>2</sup>: When Trends Meets Innovation'
                    ],
                    [
                        'title' => 'President',
                        'subtitle' => 'Music Club<br>Bloomington Middle and Grade School',
                        'year' => '2014'
                    ]
                ];
                break;

            case 'lance_gonzales':
                $data['name'] = 'Lance Gonzales';
                $data['course'] = 'IT Representative';
                $data['descriptions'] = [
                    [
                        'title' => 'Logistics Head',
                        'subtitle' => 'TechnoRun',
                        'year' => '2018'
                    ],
                    [
                        'title' => 'Logistics Head',
                        'subtitle' => '1<sup>st</sup> International TechnoFest',
                        'year' => '2018'
                    ],
                    [
                        'title' => 'Member',
                        'subtitle' => 'Alliance of Information Technology',
                        'year' => '2016 - 2018'
                    ],
                    [
                        'title' => 'Batch 2014',
                        'subtitle' => 'St. Mary\'s College of Quezon City - High School'
                    ]
                ];
                break;
        }

        return $this->load_view('profile', $data);
    }
}
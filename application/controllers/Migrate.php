<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrate extends CI_controller 
{
    public function __construct()
    {
        parent :: __construct();
        
        // if (!in_array('ROOT', $this->session->userdata('admin')['access'])) {
        //     redirect();
        // }
    }


    public function index($mode = 'current', $version = null)
    {
        $this->load->library('migration');

        switch($mode) {
            case 'current':
                echo $this->migration->current() ? 'Success' : $this->migration->error_string();
                break;

            case 'latest':
                echo $this->migration->latest() ? 'Success' : $this->migration->error_string();
                break;
                
            case 'version':
                echo $this->migration->version($version) ? 'Success' : $this->migration->error_string();
                break;

            default:
                echo $this->migration->current() ? 'Success' : $this->migration->error_string();
        }
    }
}
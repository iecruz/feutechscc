<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Technorun extends MY_Controller
{
    protected $view_directory = 'technorun';

    public $page_title      = "Technorun";
    public $include_js      = ['technorun.js'];
    public $include_css     = ['technorun.css'];
    public $include_font    = ['Anton', 'Proxima Nova'];
    public $require_load    = false;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function index($view = 'home')
    {
        switch($view)
        {
            // case 'register':
            //     $page_data['category'] = $this->input->post('category') ? $this->input->post('category') : '3k';
            //     break;

            case 'home':
            case 'receipt':
                $page_data = [];
                break;

            default:
                return $this->error_404();
        }
            
        return $this->load_view($view, $page_data);
    }

    public function insert_participant()
    {
        $fields = [
            [
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'required|trim|max_length[32]'
            ],
            [
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'required|trim|max_length[32]'
            ],
            [
                'field' => 'email_address',
                'label' => 'Email Address',
                'rules' => 'required|trim|valid_email'
            ],
            [
                'field' => 'contact_number',
                'label' => 'Contact Number',
                'rules' => 'required|trim|exact_length[10]|numeric'
            ],
            [
                'field' => 'school_company',
                'label' => 'School/Company',
                'rules' => 'trim|max_length[64]'
            ]
        ];

        $this->form_validation->set_rules($fields);
        $this->form_validation->set_error_delimiters('','</br>');

        if($this->form_validation->run() === true)
        {
            $record = [
                'first_name'        => strip_tags($this->input->post('first_name')),
                'last_name'         => strip_tags($this->input->post('last_name')),
                'email_address'     => $this->input->post('email_address'),
                'contact_number'    => $this->input->post('contact_number'),
                'school_company'    => strip_tags($this->input->post('school_company')),
                'type'              => $this->input->post('type'),
                'singlet_size'      => $this->input->post('size'),
                'mode_of_payment'   => $this->input->post('mode_of_payment')
            ];

            $data['response']   = $this->tr_participant->insert($record);
        }
        else
        {
            $data['response']   = false;
            $data['error']      = validation_errors();
        }

        echo json_encode($data);
    }
    
    public function update_participant_status()
    {
        $record = [
            'status'            => $this->input->post('status')
        ];

        $data['response'] = $this->tr_participant->update($this->input->post('id'), $record);

        echo json_encode($data);
    }

    public function update_participant_claimed()
    {
        $record = [
            'singlet_claimed'   => $this->input->post('claimed')
        ];

        $data['response'] = $this->tr_participant->update($this->input->post('id'), $record);

        echo json_encode($data);
    }

    public function update_participant_type()
    {
        $record = [
            'type'              => $this->input->post('type')
        ];

        $data['response'] = $this->tr_participant->update($this->input->post('id'), $record);

        echo json_encode($data);
    }

    public function update_participant_size()
    {
        $record = [
            'singlet_size'      => $this->input->post('size')
        ];

        $data['response'] = $this->tr_participant->update($this->input->post('id'), $record);

        echo json_encode($data);
    }

    public function delete_participant()
    {
            $data['response'] = $this->tr_participant->delete($this->input->post('id'));

        echo json_encode($data);
    }

    public function search_participant()
    {
        if($this->input->get('first_name') != null && $this->input->get('first_name') != null)
        {
            $record = [
                'first_name'    => $this->input->get('first_name'),
                'last_name'     => $this->input->get('last_name')
            ];
            
            echo json_encode($this->tr_participant->search($record));
        }
        else
        {
            echo json_encode();
        }
    }

    public function get_all_participants()
    {
        $data['data'] = $this->tr_participant->get_all();
        echo json_encode($data);
    }

    public function upload_receipt()
    {
        $this->load->library('cloudinarylib');

        $options = [
            'public_id'     => $this->input->post('id')
        ];

        $data['cloudinary'] = \Cloudinary\Uploader::upload($_FILES['receipt']['tmp_name'], $options);

        $record = [
            'receipt_link'   => $data['cloudinary']['url']
        ];

        $data['response']   = $this->tr_participant->update((int)$this->input->post('id'), $record);

        echo json_encode($data);
    }
}
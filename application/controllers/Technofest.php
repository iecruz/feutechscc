<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Technofest extends MY_Controller
{
    protected $view_directory = 'technofest';

    public $page_title      = "International TechnoFest";
    public $include_js      = [
        'technofest.js',
        'smooth_scrolling.js'
    ];
    public $include_css     = ['technofest.css'];
    public $include_font    = ['Source Sans Pro'];
    public $require_load    = false;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function index($view = 'home')
    {
        switch($view)
        {
            case 'home':
                $page_data['competitions']  = $this->tf_competition->with('program')->get_all();
                break;

            case 'competition':
                $page_data['competitions']  = $this->tf_competition->with('program')->get_all();
                break;
                
            case 'register':
                $page_data['locale']        = $this->input->post('locale') != null ? $this->input->post('locale') : 'local';
                $page_data['accommodation'] = $this->input->post('accommodation') != null ? $this->input->post('accommodation') : 'live_out';
                $page_data['competitions']  = $this->tf_competition->with('program')->get_all();
                break;    

            case 'speaker':
            case 'paper':
            case 'scholarship':
                $page_data = [];
                break;

            default:
                return $this->error_404();
        }
            
        return $this->load_view($view, $page_data);
    }

    public function scholarship_submission()
    {
        $config['upload_path']          = './uploads/paper';
        $config['overwrite']            = TRUE;
        $config['allowed_types']        = 'pdf|doc|docx';
        $config['file_ext_tolower']     = TRUE;
        $config['file_name']            = $this->input->post('participant_id');
        $config['encrypt_name']         = TRUE;
        $config['remove_spaces']        = TRUE;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('paper_pdf'))
        {
            $upload_data = $this->upload->data();
            $data['status'] = $this->tf_scholarship_applicant->insert([
                'participant_id'    => $this->input->post('participant_id'),
                'paper_url'         => "uploads/paper/{$upload_data['file_name']}",
                'answer_1'          => $this->input->post('answer_1'),
                'answer_2'          => $this->input->post('answer_2')
            ]);

            echo json_encode($data);
        }
        else
        {
            $error = array('error' => $this->upload->display_errors());
            $data['status'] = FALSE;
            
            echo json_encode($data);
        }
    }

    public function insert_participant()
    {
        $fields = [
            [
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'required|trim|max_length[32]'
            ],
            [
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'required|trim|max_length[32]'
            ],
            [
                'field' => 'contact_number',
                'label' => 'Contact Number',
                'rules' => 'required|trim|numeric'
            ],
            [
                'field' => 'email_address',
                'label' => 'Email Address',
                'rules' => 'required|trim|valid_email|is_unique[technofest_participants.email_address]',
                'errors' => [
                    'is_unique' => 'This %s is already register.'
                ]
            ],
            [
                'field' => 'school',
                'label' => 'Company/School',
                'rules' => 'required|trim|max_length[64]'
            ]
        ];

        if ($this->input->post('locale') == 'international')
        {
            $field[]    = [
                'field' => 'country',
                'label' => 'Country',
                'rules' => 'required|trim|max_length[32]'
            ];
        }

        $this->form_validation->set_rules($fields);
        $this->form_validation->set_error_delimiters('','<br/>');

        if ($this->form_validation->run() === true)
        {
            $record = [
                'first_name'        => strip_tags($this->input->post('first_name')),
                'last_name'         => strip_tags($this->input->post('last_name')),
                'email_address'     => $this->input->post('email_address'),
                'contact_number'    => $this->input->post('contact_number'),
                'school'            => strip_tags($this->input->post('school')),
                'country'           => $this->input->post('locale') == 'international' ? strip_tags($this->input->post('country')) : 'Philippines',
                'type'              => $this->input->post('career'),
                'accommodation'     => $this->input->post('locale') == 'local' ? $this->input->post('accommodation') : 'live_in',
                'food_preference'   => $this->input->post('food_preference'),
                'attendee'          => $this->input->post('attendee') != null,
                'presenter'         => $this->input->post('presenter') != null,
                'competitor'        => $this->input->post('competitor') != null
            ];

            $data['response'] = $this->tf_participant->insert($record);

            if ($record['competitor']) {
                
                $record = [
                    'participant_id' => $data['response'],
                    'competition_id' => $this->input->post('competition')
                ];

                $data['response']   = $this->tf_competitor->insert($record);
            }
        }
        else
        {
            $data['response']       = false;
            $data['error']          = validation_errors();
        }

        echo json_encode($data);
    }

    public function delete_participant()
    {
        if($this->tf_participant->get($this->input->post('id'))['status'] == true) {
            $data['status'] = false;
        }

        foreach($this->tf_competitor->get_many_by(['participant_id' => $this->input->post('id')]) as $competitor) {
            $this->tf_competitor->delete($competitor['id']);
        }

        foreach($this->tf_scholarship_applicant->get_many_by(['participant_id' => $this->input->post('id')]) as $applicant) {
            $this->tf_scholarship_applicant->delete($applicant['id']);
        }

        $this->tf_participant->delete($this->input->post('id'));

        $data['status'] = true;

        echo json_encode($data);
    }

    public function update_participant_status()
    {
        $record = [
            'status' => $this->input->post('status')
        ];

        $data['response'] = $this->tf_participant->update($this->input->post('id'), $record);

        echo json_encode($data);
    }
    
    public function get_all_participants()
    {
        $data['data'] = $this->tf_participant->get_all();
        echo json_encode($data);
    }

    public function get_all_scholarship_applicants()
    {
        $data['data'] = $this->tf_scholarship_applicant->with('participant')->get_all();
        echo json_encode($data);
    }

    public function get_all_competitors()
    {
        $data['data'] = $this->tf_competitor->with('participant')->with('competition')->get_all();
        echo json_encode($data);
    }

    public function get_participant()
    {
        if($this->input->get('email_address')) {
            $data['data'] = $this->tf_participant->get_by(['email_address' => $this->input->get('email_address')]);
        } else if($this->input->get('id')) {
            $data['data'] = $this->tf_participant->get_by(['id' => $this->input->get('id')]);
        }
        echo json_encode($data);
    }

    public function get_scholarship_applicant()
    {
        $data['data'] = $this->tf_scholarship_applicant->get_by(['id' => $this->input->get('id')]);
        echo json_encode($data);
    }
}
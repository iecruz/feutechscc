<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Views extends CI_controller 
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index($page = '404')
    {
        return $this->load->view('other/'.$page);
    }

    public function error_404()
    {
        return $this->load->view('other/404');
    }
}
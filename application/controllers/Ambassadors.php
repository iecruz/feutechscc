<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ambassadors extends MY_Controller
{
    protected $view_directory = 'ambassadors';

    public $page_title      = "iTam Ambassadors";
    public $include_js      = ['module/jquery.countdown.min.js', 'numscroller.js', 'ambassadors.js'];
    public $include_css     = ['ambassadors.css'];
    public $include_font    = ['Alex Brush', 'Cinzel', 'Source Sans Pro'];
    public $require_load    = false;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function index($view = 'home')
    {
        switch($view)
        {
            case 'vote':
                $this->include_js[] = 'ambassadors_fb.js'; 
                $this->include_js[] = 'ambassadors_vote.js'; 
                $page_data['programs']      = $this->program->get_all();
                $page_data['candidates']    = $this->ia_candidate->with('program')->get_all();
                break;

            case 'home':
                $page_data['programs']      = $this->program->get_all();
                $page_data['candidates']    = $this->ia_candidate->with('program')->get_all();
                break;

            default:
                return $this->error_404();
        }

        return $this->load_view($view, $page_data);
    }    

    public function get_candidate()
    {
        $condition = [
            'program_code'  => $this->input->get('program_code'),
            'gender'        => $this->input->get('gender'),
            'year'          => '2017'
        ];

        echo json_encode($this->ia_candidate->with('program')->get_by($condition));
    }

    public function edit_candidates()
    {
        $data = [
            'description'   => $this->input->post('description')
        ];

        $condition = [
            'program_code'  => $this->input->post('program_code'),
            'gender'        => $this->input->post('gender')
        ];

        if($this->ia_candidate->update_by($data, $condition))
            redirect('admin/ambassadors');
    }

    public function post_vote()
    {
        $data = [
            'id'            => $this->input->post('id'),
            'gender'        => $this->input->post('gender'),
            'program_code'  => $this->input->post('program_code')
        ];

        echo json_encode($this->ia_vote->insert_vote($data));
    }

    public function get_vote()
    {
        echo json_encode($this->ia_vote->get($this->input->get('id')));
    }
}
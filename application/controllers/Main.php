<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller
{
    protected $view_directory = 'main';

    public $page_title      = "Student Coordinating Council";
    public $include_js      = [];
    public $include_css     = [];
    public $include_font    = [];
    public $require_load    = false;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function index($view = 'home')
    {
        switch($view)
        {
            case 'concern':
                return $this->concern();
                break;

            default:
                return $this->error_404();
        }
    }

    public function concern()
    {
        return $this->load_view('concern');
    }
}
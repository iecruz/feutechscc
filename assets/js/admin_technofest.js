var table = null;

participantTable = $('#technofestParticipants').DataTable({
    "ajax" : `${baseUrl}api/tf/get_all_participants`,
    "columns" : [
        {
            title: 'ID',
            data: 'id'
        },
        {
            title: 'First Name',
            data: 'first_name'
        },
        {
            title: 'Last Name',
            data: 'last_name'
        },
        {
            title: 'Type',
            data: 'type'
        },
        {
            title: 'Email Address',
            data: 'email_address'
        },
        {
            title: 'Contact Number',
            data: 'contact_number'
        },
        {
            title: 'School',
            data: 'school'
        },
        {
            title: 'Country',
            data: 'country'
        },
        {
            title: 'Accomodation',
            data: 'accommodation',
            render: (data, type, row, meta) => {
                
                if(data == 'live_in') {

                    return `<button type="button" class="btn btn-success" disabled>Yes</button>`;
                } else if(data == 'live_out') {

                    return `<button type="button" class="btn btn-danger" disabled>No</button>`;
                }
            }
        },
        {
            title: 'Food Preference',
            data: 'food_preference'
        },
        {
            title: 'Attendee',
            data: 'attendee',
            render: (data, type, row, meta) => {
                
                if(data == '1' || data == 't') {

                    return `<button type="button" class="btn btn-success" disabled>Yes</button>`;
                } else if(data == '0' || data == 'f') {

                    return `<button type="button" class="btn btn-danger" disabled>No</button>`;
                }
            }
        },
        {
            title: 'Competitor',
            data : 'competitor',
            render: (data, type, row, meta) => {
                
                if(data == '1' || data == 't') {

                    return `<button type="button" class="btn btn-success" disabled>Yes</button>`;
                } else if(data == '0' || data == 'f') {
                    
                    return `<button type="button" class="btn btn-danger" disabled>No</button>`;
                }
            }
        },
        {
            title: 'Presenter',
            data: 'presenter',
            render: (data, type, row, meta) => {
                
                if(data == '1' || data == 't') {

                    return `<button type="button" class="btn btn-success" disabled>Yes</button>`;
                } else if(data == '0' || data == 'f') {

                    return `<button type="button" class="btn btn-danger" disabled>No</button>`;
                }
            }
        },
        {
            title: 'Paid?',
            data: 'status',
            render: (data, type, row, meta) => {
                
                var status = {};
                if(data == '0' || data == 'f') {
                    status.text     = 'Unpaid';
                    status.color    = 'danger';
                } else {
                    status.text     = 'Paid';
                    status.color    = 'success';
                }
                return `<button type="button" class="btn btn-${status.color}" onclick="change_technofest_status(event)" data-key="${row.id}" data-value="${data}">${status.text}</button>`;
            }
        },
        {
            title: 'Delete',
            data: 'id',
            render: (data, type, row, meta) => {
                
                return `<button type="button" class="btn btn-danger delete-btn" onclick="delete_technofest_participant(event)" data-value="${data}" ${row.status == 't' || row.status == '1' ? 'disabled' : ''}>Delete</button>`;
            }
        }
    ]
});

scholarshipApplicantTable = $('#technofestApplicants').DataTable({
    'ajax' : `${baseUrl}api/tf/get_all_scholarship_applicants`,
    'columns' : [
        {
            'title': 'First Name',
            'data': 'participant',
            'render': (data, type, row, meta) => {
                return row.participant.first_name
            }
        },
        {
            'title': 'Last Name',
            'data': 'participant',
            'render': (data, type, row, meta) => {
                return row.participant.last_name
            }
        },
        {
            'title': 'Email Address',
            'data': 'participant',
            'render': (data, type, row, meta) => {
                return row.participant.email_address
            }
        },
        {
            'title': 'Paper File',
            'data': 'paper_url',
            'render': (data, type, row, meta) => {
                return `<a target="_blank" href="${baseUrl + data}" class="btn btn-success">Open File</a>`
            }
        },
        {
            'title': 'Answers',
            'data': 'id',
            'render': (data, type, row, meta) => {
                return `<button class="btn btn-success" data-target="#answerModal" data-toggle="modal" data-value="${data}" onclick="show_answers(event)">Show Answers</button>`
            }
        }
    ]
});

competitorTable = $('#technofestCompetitors').DataTable({
    'ajax' : `${baseUrl}api/tf/get_all_competitors`,
    'columns' : [
        {
            'title': 'ID',
            'data': 'id'
        },
        {
            'title': 'First Name',
            'data': 'participant',
            'render': (data, type, row, meta) => {
                return data.first_name;
            }
        },
        {
            'title': 'Last Name',
            'data': 'participant',
            'render': (data, type, row, meta) => {
                return data.last_name;
            }
        },
        {
            'title': 'School',
            'data': 'participant',
            'render': (data, type, row, meta) => {
                return data.school;
            }
        },
        {
            'title': 'Competition',
            'data': 'competition',
            'render': (data, type, row, meta) => {
                return data.name;
            }
        },
    ]
});

function change_technofest_status(e) {

    var target = e.target;
    var currentStatus = $(target).attr('data-value');
    var details = {};

    details.id = $(target).attr('data-key');

    if(currentStatus == 'f' || currentStatus == '0') {
        
        details.status = $(target).attr('data-value') == 'f' ? 't' : '1';
    } else if(currentStatus == 't' || currentStatus == '1') {
        
        details.status = $(target).attr('data-value') == 't' ? 'f' : '0';
    }

    updateTechnofestStatus(details).done(function(data) {

        if(data['response']) {

            if(currentStatus == 'f' || currentStatus == '0') {
            
                $(target).removeClass('btn-danger');
                $(target).addClass('btn-success');
                $(target).html('Paid');
                $(target).attr('data-value', currentStatus == 'f' ? 't' : '1');
                $(target).closest('tr').find('.delete-btn').prop('disabled', true);
            } else if(currentStatus == 't' || currentStatus == '1') {
                
                $(target).removeClass('btn-success');
                $(target).addClass('btn-danger');
                $(target).html('Unpaid');
                $(target).attr('data-value', currentStatus == 't' ? 'f' : '0');
                $(target).closest('tr').find('.delete-btn').prop('disabled', false);
            }   
        }
    });
}

function show_answers(e) {
    e.preventDefault();

    var data = {id: $(e.target).attr('data-value')};
    searchTechnofestScholarshipApplicant(data).done(function(data) {
        var info = data['data'];
        $('#answer1').html(info['answer_1']);
        $('#answer2').html(info['answer_2']);
    });

    $('#answerModal').modal('show');
}

function delete_technofest_participant(e) {
    e.preventDefault();

    var data = {id: $(e.target).attr('data-value')};
    deleteTechnofestParticipant(data).done(function(data) {
        if(data['status'] && confirm('Are you sure you want to delete this record?')) {
            participantTable.row($(e.target).closest('tr')).remove().draw();
            competitorTable.ajax.reload();
            scholarshipApplicantTable.ajax.reload();
        }
    });
}
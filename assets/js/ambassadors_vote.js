$(function() {
    
    $(window).on('load', function() {

        FB.getLoginStatus(function(response) {
            
            if(response.status != 'connected') {
                
                $('#voteModal').modal('show');
                $('[href="#fbLogout"]').html('Login');
            } else {

                var details = {id:  response.authResponse.userID};
                getVote(details).done(function(response) {
                  
                    $(`.candidate-card[data-gender="M"][data-program="${response['vote_male']}"] .card-img`).removeClass('candidate-img');
                    $(`.candidate-card[data-gender="F"][data-program="${response['vote_female']}"] .card-img`).removeClass('candidate-img');
                });
                $('[href="#fbLogout"]').html('Logout');
            }
        });
    });


    $(document).on('click', '[data-target="#candidateModal"]', function() {
        
        $('#candidateModal #candidateModalImage').attr('src', ``);
        $('#candidateModal #candidateModalName').html(``);
        $('#candidateModal #candidateModalCourse').html(``);

        var details = {
            program_code: $(this).attr('data-program'),
            gender: $(this).attr('data-gender')
        };
        
        var candidate = getCandidate(details).responseJSON;

        $('#candidateModal').modal('show');

        $('#candidateModal #candidateModalImage').attr('src', `${baseUrl}assets/res/ambassadors/candidates/vote/${candidate['program_code'].toUpperCase()}_${candidate['gender']}.jpg`);
        $('#candidateModal #candidateModalName').html(`${candidate['first_name']} ${candidate['last_name']}`);
        $('#candidateModal #candidateModalCourse').html(`${candidate['program']['name']}`);
        $('#candidateModal #candidateModalVote').attr('data-program', `${candidate['program_code']}`);
        $('#candidateModal #candidateModalVote').attr('data-gender', `${candidate['gender']}`);
    });


    $(document).on('click', '#candidateModalVote, .vote-btn', function(e) {
        
        var details = {};

        var $this = $(this);
        
        details.program_code = $(this).attr('data-program');
        details.gender = $(this).attr('data-gender');

        FB.getLoginStatus(function(response) {
            
            if(response.status != 'connected') {
                
                $('#voteModal').modal('show');
                
            } else {
                
                details.id = response.authResponse.userID;
                
                postVote(details).done(function(response) {
                    
                    if(response['query'] == 'insert') {

                        alert(`You have successfully voted ${response['candidate']['first_name']} ${response['candidate']['last_name']}`);
                    } else if(response['query'] == 'update') {

                        alert(`You have successfully changed your vote to ${response['candidate']['first_name']} ${response['candidate']['last_name']}`);
                    }
                });

            }
        });

        $(`.candidate-card[data-gender="${$(this).attr('data-gender')}"] .card-img`).addClass('candidate-img');
        $(`.candidate-card[data-program="${$(this).attr('data-program')}"][data-gender="${$(this).attr('data-gender')}"] .card-img`).removeClass('candidate-img');
    });


    $(document).on('click', '[href="#fbLogout"]', function(e) {

        e.preventDefault();

        FB.getLoginStatus(function(response) {

            if(response.status != 'connected') {
                
                $('#voteModal').modal('show');

            } else {

                FB.logout();
                window.location.reload();
            }
        });
    });
});
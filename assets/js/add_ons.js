// Slide Animtion
$(window).scroll(function() {
    $(".slideanim").each(function(){
        var pos = $(this).offset().top;

        var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
            $(this).addClass("slide");
        }
    });
});


// Shimmer
$.fn.shimmer = function () {
    return this.each(function () {
        $(this).stop().css({ backgroundPosition: '0 -180px' }).animate({ 'background-position': '0 280px' }, 3000);
    });
};



// Sidebar
$(function () {
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
});
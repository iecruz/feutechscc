$.fn.initial = function() {
    $('.frame[data-order="1"]').toggleClass('active');
};


$(function() {
    $(document).initial();

    $(document).on('click', '.next', function() {
        var $currentPage = $('.frame.active');
        var $newPage = $(`.frame[data-order="${Number($currentPage.attr('data-order')) + 1}"]`);

        $newPage.css({'transform': 'translateX(0%)'});
        $currentPage.css({'transform': 'translateX(-100%)'});
        $newPage.toggleClass('active');
        $currentPage.toggleClass('active');
    });
    
    $(document).on('click', '.prev', function() {
        var $currentPage = $('.frame.active');
        var $newPage = $(`.frame[data-order="${Number($currentPage.attr('data-order')) - 1}"]`);

        $newPage.css({'transform': 'translateX(0%)'});
        $currentPage.css({'transform': 'translateX(100%)'});
        $newPage.toggleClass('active');
        $currentPage.toggleClass('active');
    });
});
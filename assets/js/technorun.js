$(function() {

    $('#announcementModal').modal('show');

    if($('#registerForm #typeField').val() == '5k' || $('#registerForm #typeField').val() == '10k') {
        
        $('#sizeChartDownload').attr('href', `https://feutech.edu.ph/scc/assets/res/technorun/download/technorun/size_chart_${$(this).val()}.jpg`);
        $('#sizeChartDownload').attr('download', `technorun_size_chart_${$(this).val()}.jpg`);
        $('#sizeChartDownload').show();
        $('#singletSizeField').show();
    } else {

        $('#sizeChartDownload').hide();
        $('#singletSizeField').hide();
    }


    $(document).on('change', '#registerForm #typeField', function(e) {

        $('.category-img').attr('src', `${baseUrl}assets/res/technorun/banner/banner_${$(this).val().toLowerCase()}.jpg`);
        
        if($(this).val() == '5k' || $(this).val() == '10k') {
            
            console.log($(this).val());
            $('#sizeChartDownload').attr('href', `${baseUrl}assets/res/technorun/download/size_chart_${$(this).val()}.jpg`);
            $('#sizeChartDownload').attr('download', `size_chart_${$(this).val()}.jpg`);
            $('#sizeChartDownload').show();
            $('#singletSizeField').show();
        } else {
            
            $('#sizeChartDownload').hide();
            $('#singletSizeField').hide();
        }
    });


    $(document).on('change', '#registerForm #paymentField', function(e) {

        switch($(this).val()) {

            case 'direct':
                $('#paymentDetail').hide();
                break;
                
            case 'cashier':
                $('#paymentDetail').show();
                $('#paymentDetailModal .modal-body div').addClass('d-none');
                $('#paymentDetailModal #cashierDetail').removeClass('d-none');
                break;
                
            case 'bpi':
                $('#paymentDetail').show();
                $('#paymentDetailModal .modal-body div').addClass('d-none');
                $('#paymentDetailModal #bpiDetail').removeClass('d-none');
                break;
                
        }
    });


    $(document).on('submit', 'form#registerForm', function(e) {

        e.preventDefault();

        var zeroPad = "00000";

        registerTechnorun($(this).serializeArray()).done(function(data) { 

            if(data['response']) {

                alert(`Registration Successful\n\n
                Your Registration ID: ${zeroPad.slice(0, zeroPad.length - data['response'].toString().length) + data['response']}`);
                window.location.href = `${baseUrl}technorun/home`;
            } else {
                
                alert('Registraion Failed\nPlease try again');
                $('#formError').html(data['error'])
            }

        }).fail(function() { 
            
            alert('Registraion Failed\nPlease try again'); 
        });
    });

    // Upload Receipt
    $('#searchForm').on('submit', function(e) {
        
        e.preventDefault();
        var zeroPad = "00000";

        $(this).fadeOut(function() {

            searchTechnorun($(this).serialize()).done(function(data) {

                data.forEach(function(data) {

                    $('#resultTableList').append(`
                        <tr>
                            <th scope="row">${zeroPad.slice(0, zeroPad.length - data.id.toString().length) + data['id']}</th>
                            <td>${data['first_name']}</td>
                            <td>${data['last_name']}</td>
                            <td>${data['school_company'] ? data['school_company'] : 'None'}</td>
                            <td>${data['singlet_size']}</td>
                            <td>${data['type']}</td>
                            <td><button class="select-record btn btn-primary" data-firstname="${data['first_name']}" data-lastname="${data['last_name']}" data-value="${data['id']}">Select</button></td>
                        </tr>
                    `);
                });
            });
            
            $('.step-title').html('Step 2: Select your registration record');
            $('#resultTable').fadeIn();
        });
    });

    $(document).on('click', 'button.select-record', function(e) {

        e.preventDefault();
        var zeroPad = "00000";

        $('#receiptForm #firstNameField').val($(this).attr('data-firstname'));
        $('#receiptForm #lastNameField').val($(this).attr('data-lastname'));
        $('#receiptForm #numberField').val(zeroPad.slice(0, zeroPad.length - $(this).attr('data-value').toString().length) + $(this).attr('data-value'));

        $('#resultTable').fadeOut(function() {

            $('.step-title').html('Step 3: Upload your receipt');
            $('#receiptForm').fadeIn();
        });
    });

    $('#receiptForm').on('submit', function(e) {

        e.preventDefault();

        var formData = new FormData(this);

        $('#load').fadeIn();

        uploadTechnorunReceipt(formData).done(function(data) {
            
            $('#load').fadeOut(function() {

                if(data['response']) {

                    alert('Receipt has been successfully uploaded!');
                    window.location.href = `${baseUrl}technorun`;
                } else {

                    alert('Receipt Upload Failed\nPlease try again');
                }
            });
        });
    });
});
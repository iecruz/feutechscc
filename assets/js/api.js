function registerTechnofest(details) {
    
    return $.ajax({
        type: 'POST',
        url: `${baseUrl}api/tf/register`,
        dataType: 'JSON',
        data: details
    });
}


function registerTechnofestScholarshipApplicant(details) {
    
    return $.ajax({
        type: 'POST',
        url: `${baseUrl}api/tf/register_scholarship`,
        dataType: 'JSON',
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        data: details
    });
}


function deleteTechnofestParticipant(details) {
    
    return $.ajax({
        type: 'POST',
        url: `${baseUrl}api/tf/delete_participant`,
        dataType: 'JSON',
        data: details
    });
}


function updateTechnofestStatus(details) {

    return $.ajax({
        type: 'POST',
        url: `${baseUrl}api/tf/update_status`,
        data: details,
        dataType: 'JSON'
    });
}


function searchTechnofestParticipant(details) {
    
    return $.ajax({
        type: 'GET',
        url: `${baseUrl}api/tf/get_participant`,
        data: details,
        dataType: 'JSON'
    });
}


function searchTechnofestScholarshipApplicant(details) {
    
    return $.ajax({
        type: 'GET',
        url: `${baseUrl}api/tf/get_scholarship_applicant`,
        data: details,
        dataType: 'JSON'
    });
}


function registerTechnorun(details) {
    
    return $.ajax({
        type: 'POST',
        url: `${baseUrl}api/tr/register`,
        dataType: 'JSON',
        data: details
    });
}


function updateTechnorunStatus(details) {

    return $.ajax({
        type: 'POST',
        url: `${baseUrl}api/tr/update_status`,
        data: details,
        dataType: 'JSON'
    });
}


function updateTechnorunClaim(details) {

    return $.ajax({
        type: 'POST',
        url: `${baseUrl}api/tr/update_claim`,
        data: details,
        dataType: 'JSON'
    });
}


function updateTechnorunType(details) {

    return $.ajax({
        type: 'POST',
        url: `${baseUrl}api/tr/update_type`,
        data: details,
        dataType: 'JSON'
    });
}


function updateTechnorunSize(details) {

    return $.ajax({
        type: 'POST',
        url: `${baseUrl}api/tr/update_size`,
        data: details,
        dataType: 'JSON'
    });
}


function deleteTechnorunStatus(details) {

    return $.ajax({
        type: 'POST',
        url: `${baseUrl}api/tr/delete`,
        data: details,
        dataType: 'JSON'
    });
}


function searchTechnorun(details) {

    return $.ajax({
        type: 'GET',
        url: `${baseUrl}api/tr/search`,
        dataType: 'JSON',
        data: details
    });
}


function uploadTechnorunReceipt(details) {

    return $.ajax({
        type: 'POST',
        url: `${baseUrl}api/tr/upload_receipt`,
        data: details,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        dataType: 'JSON'
    });
}


function getCandidate(details) {

    return $.ajax({
        async: false,
        type: 'GET',
        url: `${baseUrl}api/ia/candidate`,
        dataType: 'JSON',
        data: details
    });
};


function postVote(details) {

    return $.ajax({
        type: 'POST',
        url: `${baseUrl}api/ia/vote`,
        dataType: 'JSON',
        data: details
    });
};


function getVote(details) {
    
    return $.ajax({
        async: false,
        type: 'GET',
        url: `${baseUrl}api/ia/vote`,
        dataType: 'JSON',
        data: details
    });
};
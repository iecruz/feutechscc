$(function() {

    $(".countdown").countdown("2018/02/02", function(event) {
    
        $(this).text(
            event.strftime('%D days %H:%M:%S')
        );
    });
});
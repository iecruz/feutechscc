$(function() {

    $(document).on('change', 'input#competitorCheckField', function(e) {
    
        if($(this).prop('checked')) {

            if(!$('#competitionCollapse').hasClass('show')) {
    
                $(this).siblings('button[data-toggle="collapse"]').click();
            }

        } else {
            
            if($('#competitionCollapse').hasClass('show')) {
                
                $(this).siblings('button[data-toggle="collapse"]').click();
            }

        }
    });


    $(document).on('click', 'button.schedule-tab', function(e) {

        var page = $(this).attr('data-target');

        if($('.schedule-panel' + page).css('display') != 'none') { return; }

        $(this).siblings().removeClass('btn-warning');
        $(this).siblings().addClass('btn-dark');
        $(this).removeClass('btn-dark');
        $(this).addClass('btn-warning');

        $('.schedule-panel:not(.schedule-panel' + page + ')').hide(function() {

            $('.schedule-panel' + page).fadeIn(700);
        });
    });

    
    $(document).on('submit', 'form#registerForm', function(e) {

        e.preventDefault();

        registerTechnofest($(this).serializeArray()).done(function(data) { 

            if(data['response']) {

                alert('Registration Successful');
                window.location.href = `${baseUrl}technofest/home`;
            } else {
                
                alert('Registration Failed\nPlease try again');
                $('#formError').html(data['error']);
            }

        }).fail(function() { 
            
            alert('Registration Failed\nPlease try again'); 
        });
    });
});

function search_participant(e) {
    e.preventDefault();
    searchTechnofestParticipant($(e.target).serializeArray()).done(function(data) {
        var info = data['data'];
        if(info && (info['presenter'] == '1' || info['presenter'] == 'true')) {
            $('#participantField').val(info['id']);
            $('#registerName').html(`${info['first_name']} ${info['last_name']}`);
            $('#registerCompany').html(info['school']);
            $('#registerCountry').html(info['country']);
            $('#registerCareer').html(info['type']);
    
            $('#registerCollapse').collapse('hide');
            $('#paperCollapse').collapse('show');
        } else {
            $('#redirectCollapse').collapse('show');
        }
    });
}

function submit_application(e) {
    e.preventDefault();
    var formData = new FormData(e.target);
    if($('#customFile').val() == '') {
        return alert('No file uploaded.');
    }
    registerTechnofestScholarshipApplicant(formData).done(function(data) {
        if(data['status']) {
            alert('Application successfully submitted.');
            window.location.href = `${baseUrl}technofest`;
        } else {
            alert('Application submission failed.');
        }
    }).fail(function(data) {
        alert('Application submission failed.');
    });
}
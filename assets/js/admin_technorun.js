var table = null;

$(function() {

    table = $('#technorunParticipants').DataTable({
        ajax: `${baseUrl}api/tr/get_all_participants`,
        columns: [
            {
                title: 'ID',
                data: 'id'
            },
            {
                title: 'First Name',
                data: 'first_name'
            },
            {
                title: 'Last Name',
                data: 'last_name'
            },
            {
                title: 'Email Address',
                data: 'email_address'
            },
            {
                title: 'Contact Number',
                data: 'contact_number'
            },
            {
                title: 'School/Company',
                data: 'school_company'
            },
            {
                title: 'Type',
                data: 'type',
                render: (data, type, row, meta) => {
                    return `<select name="type" class="custom-select" data-key="${row.id}" onchange="change_technorun_type(event)">
                        <option value="3k" ${data == '3k' ? 'selected' : ''}>3K</option>
                        <option value="5k" ${data == '5k' ? 'selected' : ''}>5K</option>
                        <option value="10k" ${data == '10k' ? 'selected' : ''}>10K</option>
                    </select>`;
                }
            },
            {
                title: 'Singlet Size',
                data: 'singlet_size',
                render: (data, type, row, meta) => {
                    return `<select name="size" class="custom-select" data-key="${row.id}" onchange="change_technorun_size(event)">
                        <option value="XS" ${data == 'XS' ? 'selected' : ''}>Extra Small</option>
                        <option value="S" ${data == 'S' ? 'selected' : ''}>Small</option>
                        <option value="M" ${data == 'M' ? 'selected' : ''}>Medium</option>
                        <option value="L" ${data == 'L' ? 'selected' : ''}>Large</option>
                        <option value="XL" ${data == 'XL' ? 'selected' : ''}>Extra Large</option>
                        <option value="2Xl" ${data == '2Xl' ? 'selected' : ''}>2 Extra Large</option>
                        <option value="3Xl" ${data == '3Xl' ? 'selected' : ''}>3 Extra Large</option>
                    </select>`;
                }
            },
            {
                title: 'Mode of Payment',
                data: 'mode_of_payment'
            },
            {
                title: 'Receipt',
                data: 'receipt_upload_status',
                render: (data, type, row, meta) => {

                    if(data == '0' || data == 'f') { 

                        return 'None';
                    } else {

                        return `<a href="${row.receipt_link}" class="btn btn-primary">Receipt</a>`;
                    }
                }
            },
            {
                title: 'Paid?',
                data: 'status',
                render: (data, type, row, meta) => {
                    
                    var status = {};
                    if(data == '0' || data == 'f') {

                        status.text     = 'Unpaid';
                        status.color    = 'danger';
                    } else {

                        status.text     = 'Paid';
                        status.color    = 'success';
                    }
                    return `<button type="button" class="btn btn-${status.color}" onclick="change_technorun_status(event)" data-key="${row.id}" data-value="${data}">${status.text}</button>`;
                }
            },
            {
                title: 'Singlet Claimed?',
                data: 'singlet_claimed',
                render: (data, type, row, meta) => {
                    
                    var status = {};
                    if(data == '0' || data == 'f') {

                        status.text     = 'No';
                        status.color    = 'danger';
                    } else {

                        status.text     = 'Yes';
                        status.color    = 'success';
                    }
                    return `<button type="button" class="btn btn-${status.color}" onclick="change_technorun_claim(event)" data-key="${row.id}" data-value="${data}">${status.text}</button>`;
                }
            },
            {
                
                title: 'Delete',
                data: 'id',
                render: (data, type, row, meta) => {
                    return `<button type="button" class="btn btn-danger font-weight-bold" onclick="delete_technorun(event)" data-key="${data}">&times;</button>`;
                }
            }
        ]
    });
});

function change_technorun_status(e) {

    var target = e.target;
    var currentStatus = $(target).attr('data-value');
    var details = {};

    details.id = $(target).attr('data-key');

    if(currentStatus == 'f' || currentStatus == '0') {
        
        details.status = $(target).attr('data-value') == 'f' ? 't' : '1';
    } else if(currentStatus == 't' || currentStatus == '1') {
        
        details.status = $(target).attr('data-value') == 't' ? 'f' : '0';
    }
    
    updateTechnorunStatus(details).done(function(data) {
      
        if(data['response']) {

            if(currentStatus == 'f' || currentStatus == '0') {
            
                $(target).removeClass('btn-danger');
                $(target).addClass('btn-success');
                $(target).html('Paid');
                $(target).attr('data-value', currentStatus == 'f' ? 't' : '1');
            } else if(currentStatus == 't' || currentStatus == '1') {
                
                $(target).removeClass('btn-success');
                $(target).addClass('btn-danger');
                $(target).html('Unpaid');
                $(target).attr('data-value', currentStatus == 't' ? 'f' : '0');
            }   
        }
    });
}

function change_technorun_claim(e) {

    var target = e.target;
    var currentStatus = $(target).attr('data-value');
    var details = {};

    details.id = $(target).attr('data-key');

    if(currentStatus == 'f' || currentStatus == '0') {
        
        details.claimed = $(target).attr('data-value') == 'f' ? 't' : '1';
    } else if(currentStatus == 't' || currentStatus == '1') {
        
        details.claimed = $(target).attr('data-value') == 't' ? 'f' : '0';
    }
    
    updateTechnorunClaim(details).done(function(data) {
      
        if(data['response']) {

            if(currentStatus == 'f' || currentStatus == '0') {
            
                $(target).removeClass('btn-danger');
                $(target).addClass('btn-success');
                $(target).html('Yes');
                $(target).attr('data-value', currentStatus == 'f' ? 't' : '1');
            } else if(currentStatus == 't' || currentStatus == '1') {
                
                $(target).removeClass('btn-success');
                $(target).addClass('btn-danger');
                $(target).html('No');
                $(target).attr('data-value', currentStatus == 't' ? 'f' : '0');
            }   
        }
    });
}

function change_technorun_type(e) {
    
    var target = e.target;
    var details = {};

    details.id = $(target).attr('data-key');
    details.type = $(target).val();

    updateTechnorunType(details);
}

function change_technorun_size(e) {
    
    var target = e.target;
    var details = {};

    details.id = $(target).attr('data-key');
    details.size = $(target).val();

    updateTechnorunSize(details);
}

function delete_technorun(e) {

    var target = e.target;
    var details = {};

    details.id = $(target).attr('data-key');

    deleteTechnorunStatus(details).done(function(data) {

        if(data['response'] && confirm('Are you sure you want to delete Entry #' + $(target).attr('data-key') + '?')) {

            table.row($(target).closest('tr')).remove().draw();
        }
    });
}
var baseUrl = `${window.location.origin}/`;

$(window).on('load', function () {
    $('#load').fadeOut(150);
});

$(document).ready(function() {
    if($('.full-page').height() < $(window).height()){
        $('.full-page').height($(window).height());
    }
});


$(window).resize(function() {
    if($('.full-page').height() < $(window).height()){
        $('.full-page').height($(window).height());
    }
});
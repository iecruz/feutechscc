<?php

if(!defined('ENVIRONMENT')) {

    switch(strtolower($_SERVER['HTTP_HOST'])) {
        case 'scc-test.herokuapp.com':
            define('ENVIRONMENT', 'production');
            break;
    
        default:
            define('ENVIRONMENT', 'development');
            break;
    }
}
